package events
{
	import flash.events.Event;
	
	public class GenericEvent extends Event
	{
		public var data:Object;
		
		public static var SELECT_VIDEO_TO_POST:String = "SELECT_VIDEO_TO_POST";
		
		public static var SELECT_IMAGE_TO_POST:String = "SELECT_IMAGE_TO_POST";
		
		public static var EFFECT_SELECTED:String = "EffectSelected";
		
		public static var SEARCHED_IMAGE_CLICKED:String = "SearchedImageClicked";
		
		public static var IMAGELOADEDFROMURL:String = "IMAGELOADEDFROMURL";
		
		public static var STATUS_IDEA_SELECTED:String = "STATUS_IDEA_SELECTED";
		
		public static var EMOTICONS_SELECTED:String = "EMOTICONS_SELECTED";
		
		public static var HEADLINE_FEED_LOADED:String = "HEADLINE_FEED_LOADED";
		
		public static var NEW_KEYWORD_ENTRY_ADDED:String = "NEW_KEYWORD_ENTRY_ADDED";
		
		public function GenericEvent(type:String, data:Object, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.data = data;
		}
	}
}