package TimerObjects
{

	import Models.AppStateVars;
	
	import VOs.VOBaseCode;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.rpc.events.ResultEvent;
	
	public class SystemClockSyncTO extends VOBaseCode
	{
		/* VO Properties */
		[Bindable] public var id:int; // needed to be able to implement VOBaseCode
				   public var system_time_stamp_string:String;
		
		[Bindable] public var fetchIntervalInMilliSeconds:int;
				   private var timer:Timer;
				   private var isWaitingForFetch:Boolean = false;

		public function SystemClockSyncTO()
		{
			super();
			setServicesParameters("system_time_view","","");
		}
		
		public function setUpTimeToFireInSeconds(pFetchIntervalInSeconds:int):void {
			//systemTimeStamp 			= pSystemTimeStamp;
			fetchIntervalInMilliSeconds	= pFetchIntervalInSeconds * 1000;
		}
		
		public function start():void {
			timer = new Timer(fetchIntervalInMilliSeconds);
			timer.addEventListener(TimerEvent.TIMER, fireTimerAction);
			timer.start();
		}
		
		private function fireTimerAction(evt:TimerEvent):void {
			if (isWaitingForFetch)
				return;
			
			isWaitingForFetch = true;
 		}
		
		private function stop():void {
			timer.reset();
		}
	}
}
