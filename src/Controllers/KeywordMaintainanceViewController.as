package Controllers
{
	import Components.DBSocket;
	import Components.KeywordsPopup;
	import Components.QuickDateFormatter;
	
	import Interfaces.iController;
	
	import Models.*;
	
	import VOs.FacebookPageVO;
	import VOs.KeywordMaintainanceVO;
	import VOs.ScheduledPostVO;
	
	import Views.*;
	
	import events.GenericEvent;
	
	import flash.display.DisplayObject;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.ToggleButtonBar;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.FlexGlobals;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import spark.components.BorderContainer;
	
	public class KeywordMaintainanceViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:KeyworkMaintainanceView;
		
	   public var keywordsPopup:KeywordsPopup;
	   
	   public var keywordMaintainanceVo:KeywordMaintainanceVO;
	   
	   [Bindable] public var model:KeywordMaintainanceViewModel = new KeywordMaintainanceViewModel(this);
	      
	   public function KeywordMaintainanceViewController() {
			super();
	   }
	   
	   public function init(event:Event):void {
		   myView = view as KeyworkMaintainanceView;
		   /*myView = view as KeyworkMaintainanceView;
		   var appStateVars:AppStateVars = AppStateVars.getInstance();
		   appStateVars.appDataBase = new DBSocket(this);
		   appStateVars.appDataBase.openDBConnection();
		   appStateVars.appDataBase.getKeywords();*/
	   }
	   
	   public function handleTabChange(event:Event):void {
		   if((event.target as ToggleButtonBar).selectedIndex != 1) {
			   removeMe(null);
			   FlexGlobals.topLevelApplication.dispatchEvent(new DataEvent("TAB_CHANGED",true,false,(event.target as ToggleButtonBar).selectedIndex.toString()));
		   }
	   }
	   
	   public function dataHasBeenLoaded(pData:Array):void {
		   /*model.keywordsDp.removeAll();
		   for each(var obj:Object in pData) {
			   var keywordMain:KeywordMaintainanceVO = new KeywordMaintainanceVO();
			   keywordMain.fan_page_id = obj.fan_page_id;
			   keywordMain.id = obj.id;
			   keywordMain.keywords = obj.keywords;
			   model.keywordsDp.addItem(keywordMain);
		   }
		   model.keywordsDp.refresh();
		   AppStateVars.getInstance().keywordsCollection = model.keywordsDp;
		   AppStateVars.getInstance().keywordsCollection.refresh();*/
	   }
	   
	   public override function removeMe(evt:Event):void {
			super.removeMe(evt);
	   }
	   
	   public function getFanPageNameById(id:String):String {
		   for each(var fanPage:FacebookPageVO in model.appStateVars.facebookCOM.facebookPagesArray) {
			   if(fanPage.id ==  id) {
			   		return fanPage.label
			   }
		   }
		   return "";
	   }
	   
	   public function buttonClicked(evt:Event):void {
		   var target:String = evt.currentTarget.id;
		   switch(target) {
			   case "btnAdd":
				   keywordsPopup = new KeywordsPopup();
				   PopUpManager.addPopUp(keywordsPopup,FlexGlobals.topLevelApplication as DisplayObject,true);
				   PopUpManager.centerPopUp(keywordsPopup);
				   keywordMaintainanceVo = new KeywordMaintainanceVO();
				   keywordsPopup.keywordsCol = AppStateVars.getInstance().keywordsCollection;
				   keywordsPopup.initializePopup(keywordMaintainanceVo,true);
				   keywordsPopup.addEventListener(GenericEvent.NEW_KEYWORD_ENTRY_ADDED,onNewEntry);
				   break;
			   case "btnEdit":
				   if(myView.dgrKeywordMainPosts.selectedIndex >= 0) {
					   keywordsPopup = new KeywordsPopup();
					   PopUpManager.addPopUp(keywordsPopup,FlexGlobals.topLevelApplication as DisplayObject,true);
					   PopUpManager.centerPopUp(keywordsPopup);
					   keywordsPopup.keywordsCol = AppStateVars.getInstance().keywordsCollection;
					   var fanPageId:String = myView.dgrKeywordMainPosts.selectedItem.fan_page_id;
					   keywordsPopup.initializePopup(myView.dgrKeywordMainPosts.selectedItem as KeywordMaintainanceVO,false,getFanPageNameById(fanPageId));
				   }
				   break;
			   case "btnDelete":
				   if(myView.dgrKeywordMainPosts.selectedIndex >= 0) {
					   Alert.show("Do you really want to delete ?","Delete Confirmation",3,myView,onClose);
					   function onClose(event:CloseEvent):void {
						   if(event.detail == 1) {
							   keywordMaintainanceVo = new KeywordMaintainanceVO();
							   keywordMaintainanceVo.addEventListener("TRANSACTION_SUCCESS", deleteSuccessfully);
							   keywordMaintainanceVo.addEventListener("TRANSACTION_FAILED", deleteFailed);
							   keywordMaintainanceVo.fan_page_id = myView.dgrKeywordMainPosts.selectedItem.fan_page_id;
							   keywordMaintainanceVo.deleteVO();
						   }
					   }
				   }
				   break;
		   }
	   }
	   
	   private function deleteSuccessfully(event:Event):void {
		   keywordMaintainanceVo.removeEventListener("TRANSACTION_SUCCESS",function():void{});
		   keywordMaintainanceVo.removeEventListener("TRANSACTION_FAILED",function():void{});
			Alert.show("Deleted Successfully","Information");
			if(myView.dgrKeywordMainPosts.selectedIndex >= 0) 
				AppStateVars.getInstance().keywordsCollection.removeItemAt(myView.dgrKeywordMainPosts.selectedIndex);
			AppStateVars.getInstance().keywordsCollection.refresh();
	   }
	   
	   private function deleteFailed(event:Event):void {
		   keywordMaintainanceVo.removeEventListener("TRANSACTION_SUCCESS",function():void{});
		   keywordMaintainanceVo.removeEventListener("TRANSACTION_FAILED",function():void{});
		   Alert.show("Error Deleting");
	   }
	   
	   private function onNewEntry(event:GenericEvent):void {
		   AppStateVars.getInstance().keywordsCollection.addItem(event.data);
		   AppStateVars.getInstance().keywordsCollection.refresh();
	   }
	   
	   public function customLabelFunction(item:Object):String{
		   return item.name;
	   }
	   
	   public function selectionChanged(evt:Event):void {
		   myView.dgrKeywordMainPosts.dataProvider = null;   
		   model.appStateVars.facebookCOM.postListPageNumber = 0;
		   //Alert.show(evt.currentTarget.inputTxt.text);
	   }
	   
	   public function countHasBeenLoaded(pData:Array):void {
		   // trace("count has been laoded");
		   if (pData.length == 0)
			   return;
		   dispatchEvent(new Event("REFRESH_PAGING"));
	   }
	}
}
	