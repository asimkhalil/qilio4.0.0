package Controllers
{
	import Components.DBSocket;
	
	import Interfaces.iController;
	
	import Models.*;
	
	import VOs.UserFeedVO;
	
	import Views.*;
	
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	
	import spark.components.BorderContainer;
	
	public class FeedViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:FeedView;
 	   
	   [Bindable] public var model:FeedViewModel = new FeedViewModel(this);
	   [Bindable] public var userFeed:UserFeedVO;
	   
	   public function countHasBeenLoaded(object:Array):void{};
	   
	   public function FeedViewController() {
			super();
	   }
	   
	   public function init(event:Event):void {
		   myView = view as FeedView;
		   
		   if (model.mainObject.fan_page_id != "") {
			   myView.cmbFanPage.selectedIndex = getObjectIndexFromArray(model.appStateVars.facebookCOM.facebookPagesArray, "id", model.mainObject.fan_page_id);
			   myView.btnDelete.visible = true;
		   } else {
			   myView.btnDelete.visible = false;
		   }
	   }
	     
	   private function feedWasSavedSuccess(evt:Event):void {
		   trace("received confirmation on save, removing ");
		   this.dispatchEvent(new Event("PROCESSED_FEED"));
		   removeMe(null);   
	   }
	   
	   private function feedSaveFailed(evt:Event):void {
		   Alert.show("There was an error performing this operation");
	   }
	   
	   
	   public function buttonClicked(evt:Event):void {
		   		   
			var target:String = evt.currentTarget.id;
			
			this.model.mainObject.title = myView.txbTitle.text;
			this.model.mainObject.link = myView.txbUrl.text;
			this.model.mainObject.fan_page_id = myView.cmbFanPage.selectedItem.id;
			this.model.mainObject.addEventListener("TRANSACTION_SUCCESS", feedWasSavedSuccess);
			this.model.mainObject.addEventListener("TRANSACTION_FAILED", feedSaveFailed);
							
			switch (target) {
				
				case "btnSave":
					if (model.mainObject.id == 0) {
						model.mainObject.saveVO();						 
					} else {
						model.mainObject.updateVO();						 
					}
					break; 
				case "btnDelete":
					deleteEntityRequest();
					 
					break; 
				default:
					trace("ERROR: no case for switch on MainViewController-buttonClicked for target: " + target);
					break;
			}
	   }
	   
	   public function dataHasBeenLoaded(pData:Array):void {
		  
		   // myView.dgrStatusIdeas.dataProvider = pData;
		   //myView.pgrBar.visible = false;
	   }
	   
	   /* 1) The user requested to delete an item, she gets a chance to say yes or no here */
	   public function deleteEntityRequest():void{
		   var alert:Alert = Alert.show("Are you sure you want to delete this feed?", "Delete", Alert.YES | Alert.NO, view, deleteEntityRequestResponse);
	   }
	   
	   /* 2) The user answer to the "Delete" question goes here, if YES then the comment is deleted for good. */
	   public function deleteEntityRequestResponse(event:CloseEvent):void {
		   if (event.detail == Alert.YES) {
			   deleteEntity();
		   }
	   }
	   
	   /* 3) The real deleting call goes here */
	   public function deleteEntity():void{
		   model.mainObject.deleteVO();
	   }
	}
}
	