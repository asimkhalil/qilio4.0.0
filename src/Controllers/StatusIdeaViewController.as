package Controllers
{
	import Components.DBSocket;
	
	import Interfaces.iController;
	
	import Models.*;
	
	import VOs.StatusIdeaVO;
	
	import Views.*;
	
	import events.GenericEvent;
	
	import mx.controls.Alert;
	
	import spark.components.BorderContainer;
	
	public class StatusIdeaViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:StatusIdeaView;
 	   
	   [Bindable] public var model:StatusIdeaViewModel = new StatusIdeaViewModel(this);
	   [Bindable] public var selectedStatusIdea:StatusIdeaVO;
	   
	   public function countHasBeenLoaded(object:Array):void{};
	   
	   public function StatusIdeaViewController() {
			super();
	   }
	   
	   public function init(event:Event):void {
		   myView = view as StatusIdeaView;
		   myView.btnSettings0.setFocus();
		   
		   myView.addEventListener(GenericEvent.STATUS_IDEA_SELECTED,onStatusIdeaSelected);
		   
		   model.appStateVars.appDataBase = new DBSocket(this);
		   model.appStateVars.appDataBase.openDBConnection();
		   model.appStateVars.appDataBase.getStatusCategories();
		   
		  /* model.appStateVars.appDataBase.enternewIdeas();*/
	   }
	   
	   public function statusCategoriesDataLoaded(pData:Array):void {
		   myView.cmbCategory.dataProvider = pData;   
			
		   model.appStateVars.appDataBase.getStatusIdeasCount(myView.cmbCategory.selectedLabel);
		  
	   }
	   
	   private function onStatusIdeaSelected(event:GenericEvent):void {
		   selectItem(event.data);
	   }
	   
	   public function searchByKeyword():void {
		   model.pageNum = 1;
		   model.totalRecords = 0;
		   dispatchEvent(new Event("REFRESH_PAGING"));
		   myView.pgrBar.visible = true;
		   model.appStateVars.appDataBase.getStatusIdeasCount(myView.cmbCategory.selectedLabel,myView.txt_keyword.text);
	   }
	   
	   public function statusIdeasCountDataLoaded(pStatusIdeasCount:int):void {
		    model.totalRecords = pStatusIdeasCount;   
			dispatchEvent(new Event("REFRESH_PAGING"));
			myView.pgrBar.visible = true;
			var params:Array = model.addToSearchStatement(params, "status_category", myView.cmbCategory.selectedLabel);
			model.appStateVars.appDataBase.getEntities("status_ideas", params, "status_idea", model.CUSTOM_PAGE_SIZE, model.pageNum, false, myView.txt_keyword.text);
	   }
	   
	   public function changeToPage(newPage:int):void {
		   model.pageNum = newPage; 
		   var params:Array = model.addToSearchStatement(params, "status_category", myView.cmbCategory.selectedLabel);

		   model.appStateVars.appDataBase.getEntities("status_ideas", params, "status_idea", model.CUSTOM_PAGE_SIZE, model.pageNum, false, myView.txt_keyword.text);
	   }
	   
	   public function selectionChanged(evt:Event):void {
		   myView.txt_keyword.text = "";
		   var target:String = evt.target.id;
		   
		   switch (target) {
			   case "cmbCategory":
				   model.pageNum = 1;
				   model.totalRecords = 0;
				   dispatchEvent(new Event("REFRESH_PAGING"));
				   myView.pgrBar.visible = true;
				   model.appStateVars.appDataBase.getStatusIdeasCount(myView.cmbCategory.selectedLabel);
				   break;
			   default:
				   trace("no case for switch on PostViewController, target = " + target);
				   break;
		   }
	   }
	   
	   public function selectItem(selectedObject:Object):void {
		 	this.selectedStatusIdea = new StatusIdeaVO();
			this.selectedStatusIdea.loadDataFromResult(selectedObject, false);
		   	this.dispatchEvent(new Event("STATUS_IDEA_SELECTED"));
			this.removeMe(null);
	   }
	   
	   public function buttonClicked(evt:Event):void {
			var target:String = evt.currentTarget.id;
			
			switch (target) {
				
				case "btnSettings":
					Alert.show("Settings button");
					break; 
				default:
					trace("ERROR: no case for switch on MainViewController-buttonClicked for target: " + target);
					break;
			}
		   
	   }
	   
	   public function dataHasBeenLoaded(pData:Array):void {
	   		myView.dgrStatusIdeas.dataProvider = pData;
			myView.pgrBar.visible = false;
	   }
	   
	 
	}
}
	