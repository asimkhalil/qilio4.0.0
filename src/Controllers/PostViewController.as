
package Controllers
{
	import Components.CustomImageControl;
	import Components.CustomTextControl;
	import Components.DBSocket;
	import Components.GoogleSearchCenter;
	import Components.HashTagsPopup;
	import Components.HeadlineNewsListRenderer;
	import Components.LinkImageSelector;
	import Components.LoadImageFromURL;
	import Components.PostConfirmationAlert;
	import Components.QuickDateFormatter;
	import Components.SelectTargetingPopup;
	import Components.TextLayer;
	import Components.VideoFeedPage;
	
	import Interfaces.iController;
	
	import Models.AppModelLocator;
	import Models.AppStateVars;
	import Models.PostViewModel;
	
	import VOs.Change;
	import VOs.CropImageVO;
	import VOs.FacebookPageVO;
	import VOs.KeywordMaintainanceVO;
	import VOs.PageWithTargetVO;
	import VOs.ScheduledPostVO;
	import VOs.StatusIdeaVO;
	import VOs.UserFeedVO;
	
	import Views.ContentIdeaView;
	import Views.CropImageView;
	import Views.EmoticonSelectView;
	import Views.FeedItemSelectView;
	import Views.FeedView;
	import Views.HolidayCalendarPopupView;
	import Views.HolidaysMonthlyPopup;
	import Views.IconSelectView;
	import Views.ImageBackground;
	import Views.ImageComponent;
	import Views.ImageSearchView;
	import Views.PostView;
	import Views.SelectPagesToPostPopup;
	import Views.StatusIdeaView;
	import Views.ToolBox;
	import Views.VideoSearchView;
	
	import com.adobe.images.PNGEncoder;
	import com.pageone.imagemod.TransformTool1;
	import com.roguedevelopment.objecthandles.Flex4ChildManager;
	import com.roguedevelopment.objecthandles.ObjectChangedEvent;
	import com.roguedevelopment.objecthandles.ObjectHandles;
	import com.roguedevelopment.objecthandles.SelectionEvent;
	import com.vstyran.transform.connectors.UIScaleConnector;
	import com.vstyran.transform.model.Bounds;
	import com.vstyran.transform.view.TransformTool;
	
	import events.GenericEvent;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.net.sendToURL;
	import flash.utils.ByteArray;
	import flash.utils.getQualifiedClassName;
	import flash.utils.setTimeout;
	import flash.xml.XMLDocument;
	import flash.xml.XMLNode;
	import flash.xml.XMLNodeType;
	
	import flashx.textLayout.conversion.TextConverter;
	
	import flex.utils.MatrixUtil;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.TextInput;
	import mx.controls.ToggleButtonBar;
	import mx.controls.Tree;
	import mx.core.FlexGlobals;
	import mx.core.IVisualElement;
	import mx.core.ScrollPolicy;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.events.ColorPickerEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.formatters.DateFormatter;
	import mx.graphics.codec.JPEGEncoder;
	import mx.graphics.codec.PNGEncoder;
	import mx.managers.CursorManagerPriority;
	import mx.managers.PopUpManager;
	import mx.utils.StringUtil;
	import mx.utils.UIDUtil;
	
	import spark.components.BorderContainer;
	import spark.components.Group;
	import spark.components.NavigatorContent;
	import spark.components.TextArea;
	import spark.components.TextInput;
	

	
	public class PostViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:PostView;
 	   
	   [Bindable] public var model:PostViewModel = new PostViewModel(this);
	   
	   [Bindable] public var hotTrendsDp:ArrayCollection = new ArrayCollection();
	   
	   public var tempStatusIdeaView:StatusIdeaView; 
	   public var tempContentIdeaView:ContentIdeaView; 
	   public var tempImageBackground:ImageBackground;
	   public var tempImageIdeaView:ImageSearchView; 
	   public var tempVideoIdeaView:VideoSearchView; 
	   public var tempFeedView:FeedView;
	   public function countHasBeenLoaded(object:Array):void{};
	   public var fileReference:FileReference = new FileReference();
	   public var sourceBMP:BitmapData;
	   public var sourceBMPName:String;
	   private var imageBackgroundLoader:Loader;
	   private var imageBackgroundRequest:URLRequest;
	   private var feedsArray:Array;
	   private var tempUserFeed:UserFeedVO;
	   private var tempImage:CustomImageControl = new CustomImageControl();
	   [Bindable] public var searchResults:Array = new Array();
	   
	   [Bindable] public var currentLoadedHeadlines:Array = new Array();
	   
	   public var imgFeedsCollection:ArrayCollection = new ArrayCollection();
	   
	   public var objectHandles:ObjectHandles;
	   
	   
	   /* cropping code ***************************/
	   // Index of last image selected using the selectImage ComboBox
	   
	   private var lastSelectedImage:uint = 0;
	   
	   // Current values for the cropping rectangle, handle size, and aspect ratio constraint settings
	   
	   private var currentCropbox:Rectangle;
	   private var currentHandleSize:uint = 10;
	   private var currentCropConstraint:Boolean = false;
	   
	   public var lastOpenFeed:Object;
	   
	   [Bindable]
	   public var imageBackgroundFill:uint = 0xFFFFFF; 
	   
	   private var img1Cropbox:Rectangle;
	   private var img1HandleSize:uint = 10;
	   private var img1CropConstraint:Boolean = false;
	   
	   private var img2Cropbox:Rectangle;            
	   private var img2HandleSize:uint = 10;
	   private var img2CropConstraint:Boolean = false;            
	   /* cropping code end **************************/
	   
	   /* video feeds code start ************************/
	   [Bindable]
	   public var videoStartIndex:int = 1;
	   
	   [Bindable]
	   public var videoCustomStartIndex:int = 1;
	   /*private var videoEndIndex:int = 0;*/
	   [Bindable]
	   public var videoItemsPerPage:int = 6;
	   [Bindable]
	   public var feedDataProvider:ArrayCollection = new ArrayCollection();
	   [Bindable]
	   public var feedTotalResults:int = 0;
	   [Bindable]
	   public var feedCustomTotalResults:int = 0;
	   [Bindable]
	   public var showFeedLoading:Boolean = false;
	   private var videoFeedLoaded:Boolean = false;
	   
	   private var imageFeedLoaded:Boolean = false;
	   
	   private var selectedChannelIndex:int = 0;
	   
	   [Bindable]
	   public var channelsCol:ArrayCollection = new ArrayCollection();
	   
	   private var _selectedVideoObject:Object;
	   
	   public var currentlySelectedImageName:String = "";
	   
	   [Bindable]
	   public var currentHeadlineIndex:int = 0;
	   
	   public var imageFilters:Array = new Array();
	   
	   private var _brightnessAmount:Number;
	   
	   private var _contrastAmount:Number;
	   
	   private var _saturationAmount:Number;
	   
	   [Bindable]
	   public var keywords:Array = new Array();
	   
	   [Bindable] public var showGridLines:Boolean = false;
	   
	   [Bindable] public var showTextLayer:Boolean = false;
	   
	   [Bindable] public var textLayer:TextLayer;
	   
	   [Bindable] public var googleSearchCOM:GoogleSearchCenter = new GoogleSearchCenter();
	   
	   public var exportString:String;
	   
	   private var _undoRedoStack:ArrayCollection = new ArrayCollection();

	   private var _redoBuffer:int = -1;
	   
	   private var _topOfStack:int = -1;
	   
	   // icons for tree control
	   /*[Embed("../Media/tabs.gif")]
	   private var treeIconA:Class;
	   
	   [Embed("../Media/rss.gif")]
	   private var treeIconB:Class;*/
	   
	   private var youtubeItems:ArrayCollection;
	   
	   public function PostViewController() {
			super();
	   }
	   
	   public function headlineSelectedHandler(event:DataEvent):void {
		   myView.li.tUrl.text = extractTargetUrl(event.data);
		   myView.li.attach();
	   }
	   
	   public function set selectedVideoObject(video:Object):void 
	   {
		   _selectedVideoObject = video;
		   myView.li.clearAll();
		   myView.li.url = _selectedVideoObject.videoURI;
		   myView.li.tUrl.text = _selectedVideoObject.videoURI;
		   myView.li.attach();
	   }
	   
	   public function get selectedVideoObject():Object {
		   return _selectedVideoObject;
	   }
	   
	   // --------------------------------------------------------------------------------------------------
	   // imageReady - Called when the ImageCropper component has loaded and initialized an image
	   // --------------------------------------------------------------------------------------------------
	   
	   public function handleTabChange(event:Event):void {
		   if((event.target as ToggleButtonBar).selectedIndex > 0) {
			   removeMe(null);
			   FlexGlobals.topLevelApplication.dispatchEvent(new DataEvent("TAB_CHANGED",true,false,(event.target as ToggleButtonBar).selectedIndex.toString()));
		   }
	   }
	   
	   public function imageReady():void {
		   
		   // Enable the controls (including the imageCropper). Note that the imageCropper must be enabled before changing property values or calling setCropRect().
		   
		   enableControls(true, true);
		   
		   // Restore the handle size that was previously saved for this image
		   
		   myView.imageCropper.handleSize = currentHandleSize;
		 //  myView.handleSize.value = currentHandleSize;
		   
		   // Restore "Constrain Crop Rectangle to Aspect Ratio" to the setting that was previously saved for the image
		   
		   myView.imageCropper.constrainToAspectRatio = currentCropConstraint;
		//   myView.constrainToAspectRatio.selected = currentCropConstraint;
		   
		   // If this image was not previously selected then set the cropping rectangle to include the entire image. Otherwise,
		   // restore the cropping rectangle to its previous value. Note that the cropping rectangle is relative to the component
		   // and not to the image because the componentRelative parameter in the call the to setCropRect is set to true.
		   
		   if (!currentCropbox) myView.imageCropper.setCropRect();
		   else myView.imageCropper.setCropRect(currentCropbox.width, currentCropbox.height, currentCropbox.x, currentCropbox.y, true);
		   
		   // Get the cropped image 
		   
		   doCrop();                
	   }
	   
	   public function imageEffectsHandler(evt:GenericEvent):void {
		   
		   switch(evt.data.effect)
		   {
			   case -2:
			   {
				   imageFilters = [];
				   break;
			   }
				   
			  
				   
			   case 0:
			   {
				   imageFilters = [MatrixUtil.setRGB(1,1,1),MatrixUtil.noise(true),MatrixUtil.setBrightness(evt.data.brightness),MatrixUtil.setContrast(evt.data.contrast),MatrixUtil.setSaturation(evt.data.saturation),MatrixUtil.adjustHue(evt.data.hue),MatrixUtil.applyBlur(evt.data.blur)]; 
				   break;
			   }
				
			   case 1:
			   {
				   imageFilters = [MatrixUtil.setRGB(evt.data.red,evt.data.green,evt.data.blue),MatrixUtil.setBrightness(evt.data.brightness),MatrixUtil.setContrast(evt.data.contrast),MatrixUtil.setSaturation(evt.data.saturation),MatrixUtil.adjustHue(evt.data.hue),MatrixUtil.applyBlur(evt.data.blur)]; 
				   break;
			   }   
				   
			   case 2:
			   {
				   imageFilters = [MatrixUtil.setRGB(1,1,1),MatrixUtil.setGreyScale(true),MatrixUtil.setBrightness(evt.data.brightness),MatrixUtil.setContrast(evt.data.contrast),MatrixUtil.setSaturation(evt.data.saturation),MatrixUtil.adjustHue(evt.data.hue),MatrixUtil.applyBlur(evt.data.blur)]; 
				   break;
			   }
			   case 3:
			   {
				   imageFilters = [MatrixUtil.setRGB(1,1,0.78),MatrixUtil.setBrightness(evt.data.brightness),MatrixUtil.setContrast(evt.data.contrast),MatrixUtil.setSaturation(evt.data.saturation),MatrixUtil.adjustHue(evt.data.hue),MatrixUtil.applyBlur(evt.data.blur)];
				   break;
			   }
			   case 4:
			   {
				   imageFilters = [MatrixUtil.setRGB(evt.data.red,evt.data.green,evt.data.blue),MatrixUtil.setBrightness(evt.data.brightness),MatrixUtil.setContrast(evt.data.contrast),MatrixUtil.setSaturation(evt.data.saturation),MatrixUtil.adjustHue(evt.data.hue),MatrixUtil.applyBlur(evt.data.blur)];
				   break;
			   }
				   
			   case 5:
			   {
				   imageFilters = [MatrixUtil.setRGB(1,1,1),MatrixUtil.sharpen(true),MatrixUtil.setBrightness(evt.data.brightness),MatrixUtil.setContrast(evt.data.contrast),MatrixUtil.setSaturation(evt.data.saturation),MatrixUtil.adjustHue(evt.data.hue),MatrixUtil.applyBlur(evt.data.blur)];    
				   break;
			   }
				   
			   case 6:
			   {
				   imageFilters = [MatrixUtil.setRGB(evt.data.red,evt.data.green,evt.data.blue),MatrixUtil.setBrightness(evt.data.brightness),MatrixUtil.setContrast(evt.data.contrast),MatrixUtil.setSaturation(evt.data.saturation),MatrixUtil.adjustHue(evt.data.hue),MatrixUtil.applyBlur(evt.data.blur)];
				   break;
			   }
			   case 7:
			   {
				   imageFilters = [MatrixUtil.setRGB(evt.data.red,evt.data.green,evt.data.blue),MatrixUtil.setBrightness(evt.data.brightness),MatrixUtil.setContrast(evt.data.contrast),MatrixUtil.setSaturation(evt.data.saturation),MatrixUtil.adjustHue(evt.data.hue),MatrixUtil.applyBlur(evt.data.blur)];
				   break;
			   }
			   case 8:
			   {
				   imageFilters = [MatrixUtil.setRGB(evt.data.red,evt.data.green,evt.data.blue),MatrixUtil.setBrightness(evt.data.brightness),MatrixUtil.setContrast(evt.data.contrast),MatrixUtil.setSaturation(evt.data.saturation),MatrixUtil.adjustHue(evt.data.hue),MatrixUtil.applyBlur(evt.data.blur)];
				   break;
			   }
			   default: 
			   {
				   imageFilters = [MatrixUtil.setRGB(evt.data.red,evt.data.green,evt.data.blue),MatrixUtil.setBrightness(evt.data.brightness),MatrixUtil.setContrast(evt.data.contrast),MatrixUtil.setSaturation(evt.data.saturation),MatrixUtil.adjustHue(evt.data.hue),MatrixUtil.applyBlur(evt.data.blur)];
			   }
		   }
		   
		  
		   if(objectHandles.selectionManager.currentlySelected.length > 0){
		   	var obj:DisplayObject = objectHandles.selectionManager.currentlySelected[0] as DisplayObject;
			var change:Change = new Change();
			change.type = "ImageEffects";
			change.oldFilters = obj.filters;
			obj.filters = imageFilters;
			change.newFilters = obj.filters;
			change.changedObject = obj;
			pushtoStack(change);
		   }
		   
	   }
	   
	   public function showControlsHnd(event:Event):void {
			   
			myView.connector.target = event.target as CustomTextControl;
			AppModelLocator.currentlySelectedCustomTextName = (event.target as CustomTextControl).name;
			myView.transformerTool.visible = true;		   
		    myView.imageEffectsPanel.accordion.selectedText = event.target as CustomTextControl;
	   }
	   
	   // --------------------------------------------------------------------------------------------------
	   // doCrop - Get the cropped image from the ImageCropper component
	   // --------------------------------------------------------------------------------------------------
	   
	   public function doCrop():void {
		   
		   // Get the cropped BitmapData
		 
		   var newImage:BitmapData = myView.imageCropper.croppedBitmapData;
		   
		   // Set the width and height of the croppedImage Image based on the dimensions of the cropped image
		   
		    // Create a new Bitmap from the BitmapData and assign it to the croppedImage Image
		   
		   var croppedImage:ImageComponent = new ImageComponent();
		   
		   myView.imgFinalImage.addChild(croppedImage);
		   
		   croppedImage.img.source = new Bitmap(newImage);
		   
		   croppedImage.img.maintainAspectRatio = false;
		   
		   croppedImage.img.scaleContent = true;
		   
		   croppedImage.width = myView.imgFinalImage.width >= newImage.width ? newImage.width : myView.imgFinalImage.width;
		   croppedImage.height = myView.imgFinalImage.height >= newImage.height ? newImage.height : myView.imgFinalImage.height;  
		   croppedImage.addEventListener(MouseEvent.MOUSE_DOWN,objectSelected);
		  
		   // Display the cropping rectangle in relative to the ImageCropper component and relative to the image
		   objectHandles.registerComponent( croppedImage, croppedImage );
		   
		   croppedImage.img.smoothBitmapContent = true;
		   
		   moveTextToTop();
	   }    
	   
	   private var change:Change;
	   private var isMoving:Boolean = false;
	   private function objectSelected(event:MouseEvent):void {
			moveTextToTop();
	   }
	   
	   public function objectMovementStarted(event:Event):void{
	   		if(!isMoving){
				isMoving = true;
				change = new Change();
				change.type = "imageMoved";
				change.changedObject = objectHandles.selectionManager.currentlySelected[0] as DisplayObject;
				change.oldH = change.changedObject.height;
				change.oldW = change.changedObject.width;
				change.oldX = change.changedObject.x;
				change.oldY = change.changedObject.y;
				change.oldRotation = change.changedObject.rotation;
			}
	   }
	   
	   public function moveTextToTop():void {
		   if(textLayer) {
		   		myView.imgFinalImage.setChildIndex(textLayer,myView.imgFinalImage.numChildren-1);
		   }
		   for(var i:int=0;i<myView.imgFinalImage.numChildren;i++) {
			   var child:* = myView.imgFinalImage.getChildAt(i) as CustomTextControl; 
			   if(child is CustomTextControl) {
				   myView.imgFinalImage.setChildIndex(child,myView.imgFinalImage.numChildren-1);
			   }
		   }
		   myView.imgFinalImage.setChildIndex(myView.transformerTool,myView.imgFinalImage.numChildren-1);
	   }
	   
	   public function bringImageForward():void {
		   if(objectHandles.selectionManager.currentlySelected.length > 0) {
			   var selectedImageIndex:int = myView.imgFinalImage.getChildIndex(objectHandles.selectionManager.currentlySelected[0]);
			   if(selectedImageIndex < (myView.imgFinalImage.numChildren - 1)) {
				   for(var i:int=(selectedImageIndex+1);i<=(myView.imgFinalImage.numChildren - 1);i++) {
					   if(myView.imgFinalImage.getChildAt(i) is ImageComponent) {
						   myView.imgFinalImage.swapChildren(myView.imgFinalImage.getChildAt(selectedImageIndex),myView.imgFinalImage.getChildAt(i));
						   return;
					   }   
				   }
			   }
		   }
	   }
	   
	   public function moveImageBackward():void {
		   if(objectHandles.selectionManager.currentlySelected.length > 0) {
			   var selectedImageIndex:int = myView.imgFinalImage.getChildIndex(objectHandles.selectionManager.currentlySelected[0]);
			   if(selectedImageIndex > 0) {
				   for(var i:int=selectedImageIndex-1;i>=0;i--) {
					   if(myView.imgFinalImage.getChildAt(i) is ImageComponent) {
						   myView.imgFinalImage.swapChildren(myView.imgFinalImage.getChildAt(selectedImageIndex),myView.imgFinalImage.getChildAt(i));
						   return;
					   }   
				   }
			   }
		   }
	   }
	   
	   public function deleteSelectedImage(name:String):void 
	   {
		   
		   var selectedImage:Image = myView.imgFinalImage.getChildByName(name) as Image;
		   var change:Change = new Change();
		   if(objectHandles.selectionManager.currentlySelected.length > 0) { 
			   change.type = "ImageDeleted";
			   change.changedObject = objectHandles.selectionManager.currentlySelected[0] as UIComponent;
		   		myView.imgFinalImage.removeChild(objectHandles.selectionManager.currentlySelected[0] as UIComponent);
				if(objectHandles.selectionManager.currentlySelected[0] == textLayer){
					textLayer = null;
				}
				pushtoStack(change);
		   }
		   
		   objectHandles.selectionManager.clearSelection();
		   try{
			   if(myView.transformerTool.connector) {
				   if(myView.transformerTool.connector.targets)
					   if(myView.transformerTool.connector.targets.length > 0 && myView.transformerTool.visible) {
						   var customTextControlSelected:CustomTextControl = myView.transformerTool.connector.targets[0] as CustomTextControl;
						   if(customTextControlSelected) {
							   change.type = "TextDeleted";
							   change.changedObject = customTextControlSelected;
							   myView.imgFinalImage.removeChild(customTextControlSelected);
							   myView.transformerTool.visible = false;
							   pushtoStack(change);
						   }
					   }
			   }
		   }catch(e:Error){}
	   }
	   
	   public function refresh():void{
		   imageBackgroundFill = 0xFFFFFF;
		   for(var i:int = 0 ; i < myView.imgFinalImage.numChildren ; i++){
		   	if(myView.imgFinalImage.getChildAt(i) as DisplayObject != myView.transformerTool  && myView.imgFinalImage.getChildAt(i) as DisplayObject != myView.imgBackground
				&& myView.imgFinalImage.getChildAt(i).name != "gridlines")
			{
				trace(myView.imgFinalImage.getChildAt(i));
				myView.imgFinalImage.removeChildAt(i);
				i--;
		   	}
		   }
		   myView.transformerTool.visible = false;
		   myView.imgBackground.source = null;
		   objectHandles.selectionManager.clearSelection();
		   _undoRedoStack = new ArrayCollection();
		  _redoBuffer = -1;
		  _topOfStack = -1;
		   
	   }
	   
	   // --------------------------------------------------------------------------------------------------
	   // enableControls - Enables or disables the controls
	   // --------------------------------------------------------------------------------------------------
	   
	   public function enableControls(enable:Boolean, includeEnabled:Boolean = false):void {
		   
	   }
	   
	   public function test(evt:Event):void {
		trace("w=" + myView.imgFinalImage.width.toString() + ", h= " +   myView.imgFinalImage.height.toString()) 
	   }
	   
	   public function initBack(event:Event):void {
		   myView = view as PostView;
	   }
	   
	   [Bindable]
	   public var noOfkeywords:int = 0;
	   
	   public function getFanPageKeywords(fanPageSelected:FacebookPageVO):Array {
		   for each(var keywordsMainVo:KeywordMaintainanceVO in AppStateVars.getInstance().keywordsCollection) {
			   if(fanPageSelected.id == keywordsMainVo.fan_page_id) {
				   return keywordsMainVo.keywords.split(",");
			   }
		   }
		   return [];
	   }
	   
	   public function getFanPageHashTags(fanPageSelected:FacebookPageVO):String {
		   for each(var keywordsMainVo:KeywordMaintainanceVO in AppStateVars.getInstance().keywordsCollection) {
			   if(fanPageSelected.id == keywordsMainVo.fan_page_id) {
				   return keywordsMainVo.hash_tags;
			   }
		   }
		   return "";
	   }
	   
	   public function loadPreviousHeadlines():void {
		   if(currentHeadlineIndex > 0) {
			   currentHeadlineIndex--;
			   var feedPageStartIndex:int = currentHeadlineIndex*10;
			   currentLoadedHeadlines = new Array();
			   if(searchResults[feedPageStartIndex]) {
				   for(var i:int = feedPageStartIndex;i<feedPageStartIndex+10;i++) {
					   currentLoadedHeadlines.push(searchResults[i]);
				   }
			   } else {
				   loadHeadlineFeeds(feedPageStartIndex/10);
			   }
		   }
	   }
	   
	   public function loadMoreHeadlines():void {
		   if(keywords.length > 0) {
			   if((currentHeadlineIndex+1)/9 != keywords.length) {
					  currentHeadlineIndex++;
					  var feedPageStartIndex:int = currentHeadlineIndex*10;
					  currentLoadedHeadlines = new Array();
					  if(searchResults[feedPageStartIndex]) {
						  for(var i:int = feedPageStartIndex;i<feedPageStartIndex+10;i++) {
							  currentLoadedHeadlines.push(searchResults[i]);
						  }
					  } else {
						  loadHeadlineFeeds(feedPageStartIndex/20);
					  }
			   }
		   } else {
			   if(currentHeadlineIndex < 1) {
				   currentHeadlineIndex++;
				   var feedPageStartIndex:int = currentHeadlineIndex*10;
				   currentLoadedHeadlines = new Array();
				   if(searchResults[feedPageStartIndex]) {
					   for(var i:int = feedPageStartIndex;i<feedPageStartIndex+10;i++) {
						   currentLoadedHeadlines.push(searchResults[i]);
					   }
				   } else {
					   loadHeadlineFeeds(feedPageStartIndex/20);
				   }
			   }
		   }
	   }
	   
	   private function loadHeadlineFeeds(keywordIndex:int):void {
		   if(keywords.length > 0) {
			   googleSearchCOM.addEventListener("GOOGLE_HEADLINE_FEED_READY", finishedSearch);
			   googleSearchCOM.getHeadlineFeeds(keywords[keywordIndex]);
		   } else {
			   googleSearchCOM.addEventListener("GOOGLE_HEADLINE_FEED_READY", finishedSearch);
			   googleSearchCOM.getHeadlineFeeds("");	
		   }
	   }
	   
	   private function loadYouTubeChannels():void{
		   
		   youtubeItems = new ArrayCollection();
		   youtubeItems.addItem("UCRrW0ddrbFnJCbyZqHHv4KQ");
		   youtubeItems.addItem("UCtZNqEGzfQ6jO4SXWz51bRw");
		   youtubeItems.addItem("UCpko_-a4wgz2u_DgDgd9fqA");
		   youtubeItems.addItem("UCK7tptUDHh-RYDsdxO1-5QQ");
		   youtubeItems.addItem("UCF9imwPMSGz4Vq1NiTWCC7g");
		   youtubeItems.addItem("UCxAICW_LdkfFYwTqTHHE0vg");
		   youtubeItems.addItem("UC6nSFpj9HTCZ5t-N3Rm3-HA");
		   youtubeItems.addItem("UCBtMmmdprSADsHXhwZcDpEw");
		   
		   youtubeItems.refresh();
		   
		   youtubeItems = new ArrayCollection(randomizeArray(youtubeItems.source));
		   
		   youtubeItems.refresh();
	   }
	   
	   private function randomizeArray(array:Array):Array{
		   var newArray:Array = new Array();
		   while(array.length > 0){
			   newArray.push(array.splice(Math.floor(Math.random()*array.length), 1));
		   }
		   return newArray;
	   }
	   
	   public function init(event:Event):void {
		  myView = view as PostView;
		  myView.pgrContentProgress.visible = false;
		  myView.pgrFeedLoad.visible = false; 
		  loadHotTrends();
		  loadYouTubeChannels();
		  keywords = getFanPageKeywords(AppStateVars.getInstance().facebookCOM.facebookPagesArray[0] as FacebookPageVO);
		  noOfkeywords = keywords.length;
		  loadHeadlineFeeds(0);
		  myView.callLater(fetchTreeData);
		  myView.callLater(setDefaultFanPage);
	   }
	   
	   
	   private function setDefaultFanPage():void {
		   if(model.appStateVars.defaultFanPageId != "") {
			   AppStateVars.getInstance().selectedFanPageId = model.appStateVars.defaultFanPageId; 
			   FlexGlobals.topLevelApplication.ImportEngine.controller.scheduledPosts.getSelectedIndexForPage();
			   if(!ApplicationModel.isLite) {
			   		FlexGlobals.topLevelApplication.ImportEngine.controller.analytics.getSelectedIndexForPage();
			   }
			   var defaultPageSet:Boolean = false;
			   for(var i:int=0;i<model.appStateVars.facebookCOM.facebookPagesArray.length;i++) {
				   if(model.appStateVars.facebookCOM.facebookPagesArray[i].id == model.appStateVars.defaultFanPageId) {
					   defaultPageSet = true;
					   AppStateVars.getInstance().fanPageSelectedIndex = i;
					   myView.cmbFanPage.selectedIndex = i;
					   var urlLoader:URLLoader = new URLLoader();
					   urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
					   urlLoader.addEventListener(Event.COMPLETE,function(event:Event):void {
						   myView.image_fanPageThumb.source = event.target.data;
					   });
					   urlLoader.load(new URLRequest('https://graph.facebook.com/'+model.appStateVars.defaultFanPageId+'/picture'));
					   myView.lbl_pageName.text = myView.cmbFanPage.selectedItem.name;
					   model.appStateVars.facebookCOM.getPostsSummary(myView.cmbFanPage.selectedItem.data,(myView.cmbFanPage.selectedIndex==0));
					   break;
				   }
			   }
			   
			   if(!defaultPageSet) {
				   myView.cmbFanPage.selectedIndex = 0;
				   var urlLoader:URLLoader = new URLLoader();
				   urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
				   urlLoader.addEventListener(Event.COMPLETE,function(event:Event):void {
					   myView.image_fanPageThumb.source = event.target.data;
				   });
				   urlLoader.load(new URLRequest('https://graph.facebook.com/'+myView.cmbFanPage.selectedItem.id+'/picture'));
				   myView.lbl_pageName.text = myView.cmbFanPage.selectedItem.name;
			   }
			   
		   } else {
			   myView.cmbFanPage.selectedIndex = 0;
			   model.appStateVars.facebookCOM.getPostsSummary(myView.cmbFanPage.selectedItem.data,(myView.cmbFanPage.selectedIndex==0));
			   var urlLoader:URLLoader = new URLLoader();
			   urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
			   urlLoader.addEventListener(Event.COMPLETE,function(event:Event):void {
				   myView.image_fanPageThumb.source = event.target.data;
			   });
			   urlLoader.load(new URLRequest('https://graph.facebook.com/'+myView.cmbFanPage.selectedItem.id+'/picture'));
			   myView.lbl_pageName.text = myView.cmbFanPage.selectedItem.name;
		   }
	   }
	   
	   public function trendClickedHandler(event:DataEvent):void {
		   var trend:String = event.data;
		   tempContentIdeaView = showPopUpView("ContentIdeaView", new Object(),"",true, true, "Search Content Idea") as ContentIdeaView;
		   tempContentIdeaView.controller.addEventListener("CONTENT_IDEA_SELECTED", contentIdeaSelected);
		   tempContentIdeaView.controller.perfromSearch(trend);
	   }
	   
	   public function onCloseRssFeedPopup(event:Event):void {
		   tempContentIdeaView = null;
		   tempUserFeed = null;
	   }
	   
	   public function postTemplateClickedHandler(event:DataEvent):void {
		   var postTemplate:String = event.data;
		   myView.txbPostText.text = postTemplate;
	   }
	   
	   public var selectPagesToPost:SelectPagesToPostPopup;
	   
	   public function selectFanPagesToPostHandler(event:Event):void {
		  selectPagesToPost = new SelectPagesToPostPopup();
		  PopUpManager.addPopUp(selectPagesToPost,FlexGlobals.topLevelApplication as DisplayObject,true);
		  PopUpManager.centerPopUp(selectPagesToPost);
		  selectPagesToPost.addEventListener("FAN_PAGES_SELECTED_TO_POST",function(event:Event):void{
			  preparePagesWithTargets();
			  PopUpManager.removePopUp(selectPagesToPost);
		  });
		  if(!alreadyExists()){
		 	 AppStateVars.getInstance().selectedMultiFanPagesToPost.addItem(myView.cmbFanPage.selectedItem.id);
		  }
		  var pagesArray:ArrayCollection = new ArrayCollection();
		  for each(var page:FacebookPageVO in model.appStateVars.facebookCOM.facebookPagesArray) {
			  pagesArray.addItem({id:page.id,name:page.name});
		  }
		  pagesArray.refresh();
		  selectPagesToPost.dp = pagesArray;
	   }
	   
	   public var selectTargetingPopup:SelectTargetingPopup;
	   
	   public function selectTargetingToPostHandler(event:Event):void {
		   selectTargetingPopup = new SelectTargetingPopup();
		   PopUpManager.addPopUp(selectTargetingPopup,FlexGlobals.topLevelApplication as DisplayObject,true);
		   PopUpManager.centerPopUp(selectTargetingPopup);
		   selectTargetingPopup.addEventListener(CloseEvent.CLOSE,function(event:CloseEvent):void {
			   selectTargetingPopup.selectedItems;
			   
			   ApplicationModel.targeting = new ArrayCollection(selectTargetingPopup.selectedItems.source);
			   
			   preparePagesWithTargets();
			   
			   PopUpManager.removePopUp(selectTargetingPopup);
			   
		   });
	   }
	   
	   private function preparePagesWithTargets():void {
		   ApplicationModel.pagesWithTargets = new ArrayCollection();
		   if(ApplicationModel.targeting.length > 0 && AppStateVars.getInstance().selectedMultiFanPagesToPost.length == 0) {
			   for each(var target:Object in ApplicationModel.targeting) {
				   var pwt:PageWithTargetVO = new PageWithTargetVO();
				   pwt.pageId = myView.cmbFanPage.selectedItem.data;
				   pwt.target = target.data;
				   pwt.dateTime = target.dateTime; 
				   ApplicationModel.pagesWithTargets.addItem(pwt);
			   } 
			   
		   } else if(ApplicationModel.targeting.length == 0 && AppStateVars.getInstance().selectedMultiFanPagesToPost.length > 0) {
			   for each(var pageId:String in AppStateVars.getInstance().selectedMultiFanPagesToPost) {
				   var pwt:PageWithTargetVO = new PageWithTargetVO();
				   pwt.pageId = pageId;
				   pwt.target = "";
				   pwt.dateTime = null;
				   ApplicationModel.pagesWithTargets.addItem(pwt);
			   }
			   
		   } else if(ApplicationModel.targeting.length > 0 && AppStateVars.getInstance().selectedMultiFanPagesToPost.length > 0) {
			   for each(var pageId:String in AppStateVars.getInstance().selectedMultiFanPagesToPost) {
				   for each(var target:Object in ApplicationModel.targeting) {
					   var pwt:PageWithTargetVO = new PageWithTargetVO();
					   pwt.pageId = pageId;
					   pwt.target = target.data;
					   pwt.dateTime = target.dateTime;
					   ApplicationModel.pagesWithTargets.addItem(pwt);
				   }
			   }
		   }
	   }
	   
	   private function alreadyExists():Boolean {
		   for(var i:int=0;i<AppStateVars.getInstance().selectedMultiFanPagesToPost.length;i++) {
			   if(myView.cmbFanPage.selectedItem.id.toString() == AppStateVars.getInstance().selectedMultiFanPagesToPost.getItemAt(i).toString()) {
				   return true;
			   }
		   }
		   return false;
	   }
	   
	   public function customLabelFunction(item:Object):String{
		   return item.name;
	   }
	   
	   public function fetchTreeData():void {
		   model.appStateVars.appDataBase = new DBSocket(this);
		   model.appStateVars.appDataBase.openDBConnection();
		   model.appStateVars.appDataBase.getUserFeeds();
		   
		   /*if(model.appStateVars.defaultFanPageId != "") {
			  AppStateVars.getInstance().selectedFanPageId = model.appStateVars.defaultFanPageId; 
			  FlexGlobals.topLevelApplication.ImportEngine.controller.scheduledPosts.getSelectedIndexForPage();
			  FlexGlobals.topLevelApplication.ImportEngine.controller.analytics.getSelectedIndexForPage();
			   var defaultPageSet:Boolean = false;
			   for(var i:int=0;i<model.appStateVars.facebookCOM.facebookPagesArray.length;i++) {
				   if(model.appStateVars.facebookCOM.facebookPagesArray[i].id == model.appStateVars.defaultFanPageId) {
					   defaultPageSet = true;
					   AppStateVars.getInstance().fanPageSelectedIndex = i;
					   myView.cmbFanPage.selectedIndex = i;
					   var urlLoader:URLLoader = new URLLoader();
					   urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
					   urlLoader.addEventListener(Event.COMPLETE,function(event:Event):void {
						   myView.image_fanPageThumb.source = event.target.data;
					   });
					   urlLoader.load(new URLRequest('https://graph.facebook.com/'+model.appStateVars.defaultFanPageId+'/picture'));
					   myView.lbl_pageName.text = myView.cmbFanPage.selectedItem.name;
					   model.appStateVars.facebookCOM.getPostsSummary(myView.cmbFanPage.selectedItem.data,(myView.cmbFanPage.selectedIndex==0));
					   break;
				   }
			   }
			   
			   if(!defaultPageSet) {
				   myView.cmbFanPage.selectedIndex = 0;
				   var urlLoader:URLLoader = new URLLoader();
				   urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
				   urlLoader.addEventListener(Event.COMPLETE,function(event:Event):void {
					   myView.image_fanPageThumb.source = event.target.data;
				   });
				   urlLoader.load(new URLRequest('https://graph.facebook.com/'+myView.cmbFanPage.selectedItem.id+'/picture'));
				   myView.lbl_pageName.text = myView.cmbFanPage.selectedItem.name;
			   }
			   
		   } else {
			   myView.cmbFanPage.selectedIndex = 0;
			   model.appStateVars.facebookCOM.getPostsSummary(myView.cmbFanPage.selectedItem.data,(myView.cmbFanPage.selectedIndex==0));
			   var urlLoader:URLLoader = new URLLoader();
			   urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
			   urlLoader.addEventListener(Event.COMPLETE,function(event:Event):void {
				   myView.image_fanPageThumb.source = event.target.data;
			   });
			   urlLoader.load(new URLRequest('https://graph.facebook.com/'+myView.cmbFanPage.selectedItem.id+'/picture'));
			   myView.lbl_pageName.text = myView.cmbFanPage.selectedItem.name;
		   }*/
		   
	   }	  	  
	  
	   
	   
	   public function loadHotTrends():void{
		   showFeedLoading = true;
		   var urlLoader:URLLoader = new URLLoader();
		   urlLoader.addEventListener(Event.COMPLETE,feedLoadComplete);
		   urlLoader.load(new URLRequest("http://www.google.com/trends/hottrends/atom/feed?pn=p1"));
		   var index:int = 0;
		   function feedLoadComplete(event:Event):void 
		   {
			   
			   var hotTrendsCollection:ArrayCollection = new ArrayCollection();
			   var emrConfig:XMLListCollection = new XMLListCollection(XML(event.target.data).children());
			  
			   for each(var c:XML in emrConfig.child("item")){
				   if(index < 20){
					   hotTrendsCollection.addItem({trendName:c.title});
					   index++;
				   }
			   }
			   
			   myView.trends.dataProvider = hotTrendsCollection;
			   
			   
			   showFeedLoading = false;
		   }
	   }
	   
	   public function hotTresdsClicked():void {
	   }
	   
	   public function finishedSearch(evt:Event):void {
		   currentLoadedHeadlines = new Array();
		   if((currentHeadlineIndex)%2 == 0) {
			   for(var i:int=0;i<10;i++) { 
				   searchResults.push(googleSearchCOM.headlineResults[i]);
				   currentLoadedHeadlines.push(googleSearchCOM.headlineResults[i]);
			   }   
		   } else {
			   for(var i:int=10;i<20;i++) { 
				   searchResults.push(googleSearchCOM.headlineResults[i]);
				   currentLoadedHeadlines.push(googleSearchCOM.headlineResults[i]);
			   }   
		   }
		   
	   }
	   private var youtubeIndex:int = 0 ;
	   
	   private var videoFeedPage:ArrayCollection = new ArrayCollection();
	   
	   public function loadYoutubeFeed():void 
	   {
		   /*var isMove:Boolean = false;
		   var diff:Number = myView.vgroup_videoFeedPages.contentHeight - (myView.scrolerY.viewport.verticalScrollPosition+540);
		   if(diff <= 10){
			   isMove = true;
		   }*/
		   
		   /*if((myView.scrolerY.viewport.verticalScrollPosition == 0 && myView.vgroup_videoFeedPages.contentHeight==0) || isMove || myView.vgroup_videoFeedPages.contentHeight == 537){*/
		   
		   if(youtubeIndex < youtubeItems.length){
			   var urlLoader:URLLoader = new URLLoader();
			   urlLoader.addEventListener(Event.COMPLETE,feedLoadComplete1);
			   
				   showFeedLoading = true;
				   var chanel:String = youtubeItems.getItemAt(youtubeIndex)[0];
				   /*urlLoader.load(new URLRequest("https://gdata.youtube.com/feeds/api/users/"+chanel+"/uploads?start-index="+videoStartIndex+"&max-results="+videoItemsPerPage));*/
				   urlLoader.load(new URLRequest("https://www.youtube.com/feeds/videos.xml?channel_id="+chanel));
			   
			   function feedLoadComplete1(event:Event):void 
			   {
				   var xmlFeed:XMLDocument = new XMLDocument(event.target.data);
				   
				   var feedNode:XMLNode;
				   for each(var rssN:XMLNode in xmlFeed.childNodes) {
					   if(rssN.nodeName == "feed") {
						   feedNode = rssN;
					   }
				   }
				   
				   var feedEntries:Array = feedNode.childNodes;
				   for each(var feed:XMLNode in feedEntries) 
				   {
					   if(feed.nodeName == "entry") 
					   {
						   videoFeedPage.addItem(feed);
						   videoStartIndex++;
					   }
					   /*else if(feed.nodeName == "openSearch:totalResults") 
					   {
						   feedTotalResults = int(feed.firstChild.nodeValue);
						   if(feedTotalResults == 0){
							   youtubeIndex++;
							   videoStartIndex = 2;
							   loadYoutubeFeed();
							   return;
						   }
					   }*/
				   }
				   /*if((videoStartIndex) > 10){
					   youtubeIndex++;
					   videoStartIndex = 2;
				   }*/
				   videoFeedPage.refresh();
				   
				   youtubeIndex++;
				   loadYoutubeFeed();
			   }
			   }
			   else {
				   myView.latestVideosContainer.feedPage = videoFeedPage;
				   showFeedLoading = false;
			   }
		   
	   }
	   
	   public function reloadCustomYoutubeFeeds():void 
	   {
		   if(myView.vgroup_custom_videoFeedPages) {
			   selectedChannelIndex = 0;
			   videoCustomStartIndex = 1
			   myView.vgroup_custom_videoFeedPages.removeAllElements();
			   videoCustomFeedPage = new ArrayCollection();
			   loadYoutubeCustomFeed();
		   }
	   }
	   
	   private var videoCustomFeedPage:ArrayCollection = new ArrayCollection();
	   
	   public function loadYoutubeCustomFeed():void 
	   {
		   /*var isMove:Boolean = false;
		   var diff:Number = myView.vgroup_custom_videoFeedPages.contentHeight - (myView.scrolerC.viewport.verticalScrollPosition+540);
		   if(diff <= 10){
			   isMove = true;
		   }*/
			   
		   /*if((myView.scrolerC.viewport.verticalScrollPosition == 0 && myView.vgroup_custom_videoFeedPages.contentHeight==0) || isMove || myView.vgroup_custom_videoFeedPages.contentHeight == 537){*/
			   var channelsFetched:Array = AppModelLocator.channelsCol.split("\n");
			   
			   channelsCol.removeAll();
			   
			   channelsCol.refresh();
			   
			   for(var i:int=0;i<channelsFetched.length;i++) 
			   {
				   var channelName:String = StringUtil.trim(channelsFetched[i].toString());
				   
				   if(channelName != "") 
				   {
					   channelsCol.addItem(StringUtil.trim(channelsFetched[i].toString()));
				   }
			   }
			   
			   channelsCol.refresh();
			   
			   if(selectedChannelIndex < channelsCol.length){
				   var channel:String = channelsCol.getItemAt(selectedChannelIndex) as String;
				   
				   showFeedLoading = true;
				   var urlLoader:URLLoader = new URLLoader();
				   urlLoader.addEventListener(Event.COMPLETE,feedLoadComplete);
				   //urlLoader.load(new URLRequest("https://gdata.youtube.com/feeds/api/users/"+channel+"/uploads?start-index="+videoCustomStartIndex+"&max-results="+videoItemsPerPage));
				   urlLoader.load(new URLRequest("https://www.youtube.com/feeds/videos.xml?channel_id="+channel));
				   function feedLoadComplete(event:Event):void 
				   {
					   var xmlFeed:XMLDocument = new XMLDocument(event.target.data);
					   
					   var feedNode:XMLNode;
					   for each(var rssN:XMLNode in xmlFeed.childNodes) {
						   if(rssN.nodeName == "feed") {
							   feedNode = rssN;
						   }
					   }
					   
					   var feedEntries:Array = feedNode.childNodes;
					   
					   for each(var feed:XMLNode in feedEntries) 
					   {
						   if(feed.nodeName == "entry") 
						   {
							   videoCustomFeedPage.addItem(feed);
							   /*videoCustomStartIndex++;*/
						   }
						   /*else if(feed.nodeName == "openSearch:totalResults") 
						   {
							   feedCustomTotalResults = Number(feed.firstChild.nodeValue); 
							   if(videoCustomStartIndex >= feedCustomTotalResults ){
								   selectedChannelIndex++;
								   feedCustomTotalResults = 0;
								   videoCustomStartIndex = 1;
						   videoCustomFeedPage = new ArrayCollection();
								   loadYoutubeCustomFeed();
								   return;
							   }else{
								   feedCustomTotalResults = int(feed.firstChild.nodeValue);
							   }
						   }*/
						   
					   }
					   videoCustomFeedPage.refresh();
					   selectedChannelIndex++;
					   loadYoutubeCustomFeed();
				   }
			   }
			   else {
				   myView.customVideoContainer.feedPage = videoCustomFeedPage;
				   //videoFeedPageComponent.addEventListener(FlexEvent.CREATION_COMPLETE,function(event:FlexEvent):void{myView.scrolerC.viewport.verticalScrollPosition += 540;});
				   showFeedLoading = false;
			   }
			   
			   /*}else{
		   	myView.scrolerC.viewport.verticalScrollPosition+= 540;
		   }*/
		   
	   }
	   
	   public function imageObjectSelected(evt:DataEvent):void {
		   removeTitleWindow();
		   var newImage:String = evt.data;
		   
		   // Set the width and height of the croppedImage Image based on the dimensions of the cropped image
		   
		   // Create a new Bitmap from the BitmapData and assign it to the croppedImage Image
		   
		   var croppedImage:ImageComponent = new ImageComponent();
		   myView.imgFinalImage.addChild(croppedImage);
		   croppedImage.name = UIDUtil.createUID();
		   croppedImage.img.source = newImage;
		   croppedImage.width = 150;
		   croppedImage.height = 150;
		   croppedImage.x = (myView.imgFinalImage.width/2)-(croppedImage.width/2);
		   croppedImage.y = (myView.imgFinalImage.height/2)-(croppedImage.height/2);
		   croppedImage.img.smoothBitmapContent = true;
		   croppedImage.img.scaleContent = true;
		   croppedImage.img.maintainAspectRatio = false;
		   objectHandles.registerComponent(croppedImage,croppedImage);
		   croppedImage.addEventListener(MouseEvent.MOUSE_DOWN,objectSelected);
		   
		   var change:Change = new Change();
		   change.type = "addImage";
		   change.changedObject = croppedImage;
		   change.oldH = change.changedObject.height;
		   change.oldW = change.changedObject.width;
		   change.oldX = change.changedObject.x;
		   change.oldY = change.changedObject.y;
		   change.oldRotation = change.changedObject.rotation;
		   pushtoStack(change);
		   
		   moveTextToTop();
	   }
	   
	   public function setBackGround(evt:DataEvent):void {
		   myView.imgBackground.source = evt.data;
		   removeTitleWindow();
	   }
	   
	   public function selectionChanged(evt:Event=null,targetComp:*=null):void {
		   var target:String = evt?evt.target.id:targetComp.id;
		   
		   switch (target) {
			   case "cmbBackgrounds":
				   var newImage:String = myView.cmbBackgrounds.selectedItem.data;
				  
				   if (newImage == "") {
					   myView.imgBackground.source = null;
					   return;
				   }
				   giveNewBackgroundToImage("../Media/imageBackgrounds/" + newImage);
				   break;
			   case "cmbPostType":
				   var selectedState:String = evt?evt.target.selectedItem.state:targetComp.selectedItem.state;
				  myView.currentState = selectedState;
				  if(selectedState == "VideoState") 
				  {
					  if(!videoFeedLoaded) 
					  {
						  videoFeedPage = new ArrayCollection();
						  loadYoutubeFeed();
						  videoCustomFeedPage = new ArrayCollection();
						  loadYoutubeCustomFeed();
						  videoFeedLoaded = true
					  }
				  }else if(selectedState == "ImageState"){
				  	if(!imageFeedLoaded){
						imageFeedLoaded = true;
					}
				  }
				  
				   break;
			  
			   default:
				   model.appStateVars.facebookCOM.getPostsSummary(myView.cmbFanPage.selectedItem.data,(myView.cmbFanPage.selectedIndex==0));
				   refreshFanPageSelection();
				   trace("no case for switch on PostViewController, target = " + target);
				   /*FlexGlobals.topLevelApplication.ImportEngine.controller.scheduledPosts.getSelectedIndexForPage();*/
				   break;
		   }
	   }
	   
	   public function refreshFanPageSelection():void {
		   currentHeadlineIndex = 0;
		   keywords = getFanPageKeywords(myView.cmbFanPage.selectedItem as FacebookPageVO);
		   noOfkeywords = keywords.length;
		   loadHeadlineFeeds(0);
		   
		   var urlLoader:URLLoader = new URLLoader();
		   urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
		   urlLoader.addEventListener(Event.COMPLETE,function(event:Event):void {
			   myView.image_fanPageThumb.source = event.target.data;
		   });
		   urlLoader.load(new URLRequest('https://graph.facebook.com/'+myView.cmbFanPage.selectedItem.id+'/picture'));
		   myView.lbl_pageName.text = myView.cmbFanPage.selectedItem.name;
		   AppStateVars.getInstance().fanPageSelectedIndex = myView.cmbFanPage.selectedIndex;
		   
		   if(model.appStateVars.channelsCollectionDirty) 
		   {
			   model.appStateVars.channelsCollectionDirty = false;
			   reloadCustomYoutubeFeeds();
		   }
	   }
	   
	   public function reloadData():void {
		   currentHeadlineIndex = 0;
		   /* keywords = getFanPageKeywords(myView.cmbFanPage.selectedItem as FacebookPageVO);
		   noOfkeywords = keywords.length;
		   loadHeadlineFeeds(0);*/
		   loadHotTrends();
		   loadYouTubeChannels();
		   loadHeadlineFeeds(0);
		   keywords = getFanPageKeywords(AppStateVars.getInstance().facebookCOM.facebookPagesArray[0] as FacebookPageVO);
		   noOfkeywords = keywords.length;
		   myView.callLater(fetchTreeData);
		   if(myView.imgFeedView){
			   myView.imgFeedView.controller.model.performSearch(true);
		   }
		   if(myView.vgroup_videoFeedPages){
			   //loadYoutubeFeed();
			   //loadYoutubeCustomFeed();
		   }
		   
		   var urlLoader:URLLoader = new URLLoader();
		   urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
		   urlLoader.addEventListener(Event.COMPLETE,function(event:Event):void {
			   myView.image_fanPageThumb.source = event.target.data;
		   });
		   urlLoader.load(new URLRequest('https://graph.facebook.com/'+myView.cmbFanPage.selectedItem.id+'/picture'));
		   myView.lbl_pageName.text = myView.cmbFanPage.selectedItem.name;
		   AppStateVars.getInstance().fanPageSelectedIndex = myView.cmbFanPage.selectedIndex;
		   
		   if(model.appStateVars.channelsCollectionDirty) 
		   {
			   model.appStateVars.channelsCollectionDirty = false;
			   reloadCustomYoutubeFeeds();
		   }
	   }
	   
	   
	   public function giveNewBackgroundToImage(pImage:String):void {
		   imageBackgroundLoader = new Loader();
		   imageBackgroundLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, newBackGroundImageLoaded);
		   imageBackgroundRequest = new URLRequest(pImage);
		   imageBackgroundLoader.load(imageBackgroundRequest);   
	   }
	   
	   private function newBackGroundImageLoaded(e:Event):void
	   {
		   var info:LoaderInfo = e.target as LoaderInfo;
		   var loader:Loader = info.loader;
		   var change:Change = new Change();
		   change.type = "backgoundImage";
		   change.oldSource = myView.imgBackground.source;
		   myView.imgBackground.source = loader.content;
		   change.newSource = myView.imgBackground.source;
		   pushtoStack(change);
	   }
	   
	   private function wallPostFailed(evt:Event):void {
		   model.appStateVars.facebookCOM.removeEventListener("WALL_POST_SEND_FAILED", wallPostFailed);
		   isViewDisabled(false);
	   }
	   
	   private function selectUserImage():void {
		   var arr:Array = [];
		  
		   arr.push(new FileFilter("Images", ".gif;*.jpeg;*.jpg;*.png"));
		   fileReference.addEventListener(Event.SELECT, imageSelected);
		   fileReference.addEventListener(Event.COMPLETE, imageCompleted);
		   fileReference.browse(arr);
	   }
	   
	   private function imageSelected(evt:Event):void {
		   fileReference.load();
	   }	   
	   
	   private function imageCompleted(event:Event):void {
		   var loader:Loader = new Loader();
		   loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loader_complete);
		   loader.loadBytes(fileReference.data);
	   }
	   
	   public function croppedImageReady(resultImage:BitmapData):void {
		   var newImage:Bitmap = new Bitmap(resultImage);
		   /*myView.croppedImage.source = new Bitmap(resultImage);
		   
		   myView.imageCropper.sourceImage = resultImage;*/
		   
		   // Set the width and height of the croppedImage Image based on the dimensions of the cropped image
		   
		   // Create a new Bitmap from the BitmapData and assign it to the croppedImage Image
		   
		   var encoder:JPEGEncoder = new JPEGEncoder();
		   var imageBytes:ByteArray = encoder.encode(resultImage);
		   
		   var croppedImage:ImageComponent = new ImageComponent();
		   myView.imgFinalImage.addChild(croppedImage);
		   croppedImage.name = UIDUtil.createUID();
		   croppedImage.img.source = imageBytes;
		   croppedImage.width = myView.imgFinalImage.width >= newImage.width ? newImage.width : myView.imgFinalImage.width;
		   croppedImage.width = croppedImage.width/2;
		   croppedImage.height = myView.imgFinalImage.height >= newImage.height ? newImage.height : myView.imgFinalImage.height;
		   croppedImage.height = croppedImage.height/2;
		   croppedImage.img.smoothBitmapContent = true;
		   croppedImage.img.scaleContent = true;
		   croppedImage.img.maintainAspectRatio = false;
		   objectHandles.registerComponent(croppedImage,croppedImage);
		   croppedImage.addEventListener(MouseEvent.MOUSE_DOWN,objectSelected);
		   
		   var change:Change = new Change();
		   change.type = "addImage";
		   change.changedObject = croppedImage;
		   change.oldH = change.changedObject.height;
		   change.oldW = change.changedObject.width;
		   change.oldX = change.changedObject.x;
		   change.oldY = change.changedObject.y;
		   change.oldRotation = change.changedObject.rotation;
		   pushtoStack(change);
		  
		   moveTextToTop();
	   }
	   
	   public function noCropedImage(resultImage:ByteArray):void {
		   
		   var croppedImage:ImageComponent = new ImageComponent();
		   myView.imgFinalImage.addChild(croppedImage);
		   croppedImage.name = UIDUtil.createUID();
		   croppedImage.img.source = fileReference.data;
		   croppedImage.width = 60;
		   croppedImage.height = 60;
		   croppedImage.img.smoothBitmapContent = true;
		   croppedImage.img.scaleContent = true;
		   croppedImage.img.maintainAspectRatio = false;
		   objectHandles.registerComponent(croppedImage,croppedImage);
		   croppedImage.addEventListener(MouseEvent.MOUSE_DOWN,objectSelected);
		   
		   var change:Change = new Change();
		   change.type = "addImage";
		   change.changedObject = croppedImage;
		   change.oldH = change.changedObject.height;
		   change.oldW = change.changedObject.width;
		   change.oldX = change.changedObject.x;
		   change.oldY = change.changedObject.y;
		   change.oldRotation = change.changedObject.rotation;
		   pushtoStack(change);
		   
		   moveTextToTop();
	   }
	   
	   public function objectMoved(event:ObjectChangedEvent):void{
		   isMoving = false;
		   var d:DisplayObject = objectHandles.selectionManager.currentlySelected[0] as DisplayObject;
		   change.newH = d.height;
		   change.newW = d.width;
		   change.newX = d.x;
		   change.newY = d.y;
		   change.newRotation = d.rotation;
		   pushtoStack(change);
	   }
	   
	   public function loader_complete (event:Event) : void {
		   var tempCropImageView:CropImageView; 
		   
		   sourceBMP = event.currentTarget.loader.content.bitmapData;
		   
		   if(myView.currentState == "ImageState") {
			   tempCropImageView = showPopUpView("CropImageView", null, "", true, true, "Crop Image") as CropImageView;
			   tempCropImageView.controller.delegate = this;
			   tempCropImageView.controller.assignImageToCropControl(sourceBMP);
			   tempCropImageView.controller.setOriginalImage(fileReference.data);
			   
		   } else if(myView.currentState == "VideoState") {
			   myView.imgVideoImage.source = sourceBMP;
			   
		   }
		   
		   sourceBMPName = fileReference.name;
	   }
	   
	  
	   
	   public function addTextToImage():void {
		   var tempImageText:CustomTextControl = new CustomTextControl();
		   var change:Change = new Change();
		   change.type = "addText";
		  
		   myView.imgFinalImage.addChild(tempImageText);
		   tempImageText.addEventListener(MouseEvent.MOUSE_DOWN,objectSelected);
		   tempImageText.height = 55;
		   tempImageText.width = 340;
		   tempImageText.txtEditable.height = 55;
		   tempImageText.txtEditable.width = 340;
		   tempImageText.txtEditable.text = "Enter text here";
		   tempImageText.txtEditable.setStyle("textAlign","center");
		   tempImageText.setStyle("fontSize", 30);
		   tempImageText.txtEditable.setStyle("fontSize", 30);
//		   tempImageText.setStyle("fontFamily", "vastshadowNOCFF");
//		   tempImageText.txtEditable.setStyle("fontFamily", "vastshadow");
		   
		   myView.connector.target = tempImageText;
		   change.changedObject = tempImageText;
		   change.oldH = change.changedObject.height;
		   change.oldW = change.changedObject.width;
		   change.oldX = change.changedObject.x;
		   change.oldY = change.changedObject.y;
		   change.oldRotation = change.changedObject.rotation;
		   pushtoStack(change);
		   moveTextToTop();
		   tempImageText.validateDimensions();
	   }
	   
	   private function removedFromSelection(event:SelectionEvent):void {
		   if(event.targets[0] is CustomTextControl) {
		   }
	   }
	   
	   private function addedToSelection(event:SelectionEvent):void {
		   if(event.targets[0] is CustomTextControl) {
		   }
	   }
	   
	   public function captureImageAndSaveToLocal():void {
		   var wasShowingGridLines:Boolean = showGridLines;
		   showGridLines = false;
		   var bd1:BitmapData = new BitmapData(myView.imgFinalImage.width, myView.imgFinalImage.height);
		   var screenshot:Bitmap;
		   var newFileOFScreenShot:FileReference = new FileReference();
		   var fs:FileStream = new FileStream();
		   
		   myView.imgFinalImage.setStyle("borderVisible", false);
		   bd1.draw(myView.imgFinalImage);
		   myView.imgFinalImage.setStyle("borderVisible", true);
		   screenshot = new Bitmap(bd1);
		   
		   /* encoding, saving and sending part */
		   var pngenc:mx.graphics.codec.PNGEncoder = new mx.graphics.codec.PNGEncoder();
		   //encode the bitmapdata object and keep the encoded ByteArray
		   var imgByteArray:ByteArray = pngenc.encode(bd1);
		   
		   var fl:File = File.documentsDirectory.resolvePath("Qilio");
		   
		   if(!fl.exists) 
		   {
		   		fl.createDirectory();
		   }
		   
		   var d:Date = new Date();
		   
		   var df:DateFormatter = new DateFormatter();
		   df.formatString = "MM-DD-YYYY-JJ-NN-SS";
		   
		   var fileName:String = myView.cmbFanPage.selectedItem.label+"-"+df.format(d)+".png";
		   
		   var fl:File = new File(fl.nativePath+"/"+fileName);
		   fl.addEventListener(Event.SELECT, dirSelected);
		   fl.browseForSave(fileName);
		   function dirSelected(e:Event):void {
			   try{
				   //open file in write mode
				   fs.open(fl,FileMode.WRITE);
				   //write bytes from the byte array
				   fs.writeBytes(imgByteArray);
				   //close the file
				   fs.close();
				   
				   Alert.show("Image Saved","Information");
				   
			   } catch(e:Error){
				   Alert.show(e.message);
			   }
		   }
		   
		   showGridLines = wasShowingGridLines;
	   }
	   
	   public function captureImageAndSaveOrSend(pPageId:String, pPageAccessToken:String, pTarget:String, scheduleTime:Date):void {
		   var wasShowingGridLines:Boolean = showGridLines;
		   showGridLines = false;
		   var bd1:BitmapData = new BitmapData(myView.imgFinalImage.width, myView.imgFinalImage.height);
		   var screenshot:Bitmap;
		   var newFileOFScreenShot:FileReference = new FileReference();
		   var fs:FileStream = new FileStream();
		   var selectedScheduledTime:int = 0;	
		   var scheduledTS:Date;
		   
		   
		   if (myView.dtpDatetime.publishState == myView.dtpDatetime.PUBLISH_STATE_SCHEDULED ) {
			   if(pTarget && scheduleTime) {
				   selectedScheduledTime = QuickDateFormatter.getUnixStampTimeFromDate(scheduleTime);
				   scheduledTS = scheduleTime;
			   } else {
				   selectedScheduledTime = QuickDateFormatter.getUnixStampTimeFromDate(myView.dtpDatetime.selectedDate);
				   scheduledTS = myView.dtpDatetime.selectedDate;
			   }
			   //selectedScheduledTime = QuickDateFormatter.getDateTimeInMySQLFormatFromDate(myView.dtpDatetime.selectedDate);
		   } 
		   
		   myView.imgFinalImage.setStyle("borderVisible", false);
		   bd1.draw(myView.imgFinalImage);
		   myView.imgFinalImage.setStyle("borderVisible", true);
		   screenshot = new Bitmap(bd1);
		   
		  /* encoding, saving and sending part */
		   var pngenc:mx.graphics.codec.PNGEncoder = new mx.graphics.codec.PNGEncoder();
		   //encode the bitmapdata object and keep the encoded ByteArray
		   var imgByteArray:ByteArray = pngenc.encode(bd1);
		   var fl:File = File.applicationStorageDirectory.resolvePath("Media/postImages/snapshot.png");
		   
		   try{
			   //open file in write mode
			   fs.open(fl,FileMode.WRITE);
			   //write bytes from the byte array
			   fs.writeBytes(imgByteArray);
			   //close the file
			   fs.close();
		   } catch(e:Error){
			   Alert.show(e.message);
		   }
		   
		   //myView.enabled = false;
		   model.appStateVars.facebookCOM.postImageToPageWall((myView.cmbFanPage.selectedIndex==0),pPageId, myView.txbPostText.text, sourceBMPName, fl, pPageAccessToken, selectedScheduledTime,scheduledTS,pTarget);
		   showGridLines = wasShowingGridLines;
	   }
	   
	   private function feedViewClosed(evt:Event):void {
		   fetchTreeData();
		   tempUserFeed = null;
	   }
	   
	   private function validateScheduledTime(pDate:Date):Boolean {
		   var date:Date = new Date();
		   var diff:int = QuickDateFormatter.getMinuteDiffInDates(pDate, date);
		   
		   if (diff < 10 && myView.dtpDatetime.publishState == myView.dtpDatetime.PUBLISH_STATE_SCHEDULED) {
			   Alert.show("The scheduled date must be at least 10 minutes ahead of current time.", "Scheduled post error");
			   return false;
		   }
		   
	 	   return true;
	   }
	   
	   public function emoticonWasSelected(emoticon:String):void
	   { 
		   myView.txbPostText.setFocus();
		   myView.txbPostText.insertText(emoticon);
	   }
	   
	   public function hashTagWasSelected(event:DataEvent):void {
		   myView.txbPostText.setFocus();
		   myView.txbPostText.insertText(event.data);
	   }
	   
	   public function iconWasSelected(pTempIcon:CustomImageControl, pIconName:String):void {
		   trace("icon was selected");
		   var iconName:String = pIconName;
		   
		   pTempIcon.hasDelete = true;
		   pTempIcon.hasResize = false;
		   pTempIcon.width = 80;
		   pTempIcon.height = 45;
		   pTempIcon.boundArea = new Rectangle(	myView.imgFinalImage.x,
			   myView.imgFinalImage.y,
			   myView.imgFinalImage.width - 85,
			   myView.imgFinalImage.height - 45);
		   
		   myView.imgFinalImage.addElement(pTempIcon);
		   
		   pTempIcon.imgObject.source = pIconName;
		   
		     
	   }
	   
	   public function imageMouseClick(evt:Event):void {
		   
		   myView.imageCropper.setStyle("border","solid 1px red");			   
		   
	   }

	   public function saveFile():void {
		   var file:File = new File();
		   file.addEventListener(Event.SELECT, dirSelected);
		   file.browseForSave("Select a directory");
		   function dirSelected(e:Event):void {
			   var writeFileStream:FileStream = new FileStream();
			   writeFileStream.open(file, FileMode.WRITE);
			   writeFileStream.writeUTFBytes(exportString);
			   writeFileStream.close();
			   Alert.show("Data Exported Successfully!","Information");
		   }
	   }
	   
	   private var _importedFeeds:ArrayCollection;
	   
	   public function importFile():void {
		   var file:File = new File();
		   file.addEventListener(Event.SELECT,fileSelected);
		   file.browse();
		   
		   function fileSelected(event:Event):void {
			   file.addEventListener(Event.COMPLETE,fileLoadComplete);
			   file.load();
		   }
		   
		   function fileLoadComplete(event:Event):void {
			   myView.pgrContentProgress.visible = true;
				var fileContents:String = file.data.readUTFBytes(file.data.length);
				var feeds:XMLDocument = new XMLDocument(fileContents);

				_importedFeeds = new ArrayCollection();
				
				var feedsPerPage:ArrayCollection;
				
				for each(var cat:XMLNode in (feeds.childNodes[0] as XMLNode).childNodes) {
					if(cat.nodeType == XMLNodeType.ELEMENT_NODE) 
					{
						var fanPageName:String = cat.attributes["name"];
						
						for each(var feedEntry:XMLNode in cat.childNodes) 
						{
							if(feedEntry.nodeType == XMLNodeType.ELEMENT_NODE) 
							{
								var link:String = feedEntry.attributes["link"];
								
								//var fan_page_id:String = feedEntry.attributes["fan_page_id"];
								
								for each(var feedEntryName:XMLNode in feedEntry.childNodes) 
								{
									if(feedEntryName.nodeType == XMLNodeType.TEXT_NODE) 
									{
										var feedEntryText:String = feedEntryName.nodeValue;
									}
								}
								if(!feedAlreadyExists(feedEntryText,link)) 
								{
									_importedFeeds.addItem({pageName:fanPageName,link:link,text:feedEntryText});
								}
							}
						}
					}
				}
				_importedFeeds.refresh();
				if(_importedFeeds.length > 0) {
					syncFeedsTreeWithDB();
				}
		   }
	   }
	   
	   private function feedAlreadyExists(label:String,link:String):Boolean 
	   {
		   	for(var i:int=0;i<collection.length;i++) 
			{
			 	var feeds:Array = (collection.getItemAt(i) as FacebookPageVO).children;
				
				for(var j:int=0;j<feeds.length;j++) 
				{
					if(feeds[j].link == link && feeds[j].label == label) 
					{
						return true;
					}
				}
			}
			return false;
	   }
	   
	   private function getFanPageId(fanPageName:String):String 
	   {
		   for(var i:int=0;i<collection.length;i++) 
		   {
			   var fanPageVO:FacebookPageVO = (collection.getItemAt(i) as FacebookPageVO);
			   
			   var fanPageNameFromCol:String = fanPageVO.name;
			   
			   fanPageNameFromCol = fanPageNameFromCol.substring(0,fanPageNameFromCol.indexOf("(")-1);
			   
			   if(fanPageNameFromCol == fanPageName) 
			   {
					return fanPageVO.id;   
			   }
		   }
		   return "";
	   }
	   
	   
	   public function syncFeedsTreeWithDB():void {
		   
		   var count:int = 0;
			
		   saveFeed();
		   
		   var userFeedVo:UserFeedVO;
		   
		   function saveFeed():void {
			   var feed:Object = _importedFeeds.getItemAt(count);
			   userFeedVo = new UserFeedVO();
			   userFeedVo.id = 0;
			   userFeedVo.title = feed.text;
			   userFeedVo.link = feed.link;
			   userFeedVo.fan_page_id = getFanPageId(feed.pageName);
			   userFeedVo.addEventListener("TRANSACTION_SUCCESS", feedWasSavedSuccess);
			   userFeedVo.addEventListener("TRANSACTION_FAILED", feedSaveFailed);
			   userFeedVo.saveVO();
		   }
		   
		   function feedWasSavedSuccess(event:Event):void 
		   {
			   count++;
			   userFeedVo.removeEventListener("TRANSACTION_SUCCESS",feedWasSavedSuccess);
			   userFeedVo.removeEventListener("TRANSACTION_FAILED", feedSaveFailed);
			   if(count < _importedFeeds.length) {
				   
				   saveFeed();
			   } 
			   else 
			   {
				   fetchTreeData();
				   tempUserFeed = null;
				   myView.pgrContentProgress.visible = false;
			   }
		   }
		   
		   function feedSaveFailed(event:Event):void 
		   {
			   userFeedVo.removeEventListener("TRANSACTION_SUCCESS",feedWasSavedSuccess);
			   userFeedVo.removeEventListener("TRANSACTION_FAILED", feedSaveFailed);
			   Alert.show("Error Importing Feeds!","Information");
			   myView.pgrContentProgress.visible = false;
		   }
	   }
	   
	   public function buttonClicked(evt:Event):void {
			var m_target:String;
			var m_link:String;
			var m_source:String;
			var m_picture:String;
			var m_pageId:String;
			var m_pageAccessToken:String;
			var m_postText:String;
			var m_videoSource:String;
			var postText:String;
			var link:String;
			var source:String;
			var picture:String;
			var postScheduleTime:int = 0;
			
			m_target = evt.currentTarget.id; 
			
			switch (m_target) {
				
				case "btnHolidayCalendar":
					FlexGlobals.topLevelApplication.disableApp();
					var holidayView:HolidayCalendarPopupView = new HolidayCalendarPopupView();
					holidayView.visible = false;
					PopUpManager.addPopUp(holidayView,FlexGlobals.topLevelApplication as DisplayObject,true);
					break;
				
				case "btnContentEmoticons":
					var tempEmoView:EmoticonSelectView = showPopUpView("EmoticonSelectView", null, "", true, true, "Select Emoticons") as EmoticonSelectView;
					tempEmoView.controller.delegate = this;
					break;
				
				case "btnContentEmoticons1":
					var tempEmoView:EmoticonSelectView = showPopUpView("EmoticonSelectView", null, "", true, true, "Select Emoticons") as EmoticonSelectView;
					tempEmoView.controller.delegate = this;
					break;
				
				case "btnEffects":
					break;
				case "btnCloseCrop":
					myView.brgCrop.visible = false;
					break;
				case "btnImgCropper":
					myView.brgCrop.visible = true;
					break;
				case "txbContentUrl":
					break;
				case "txbVideoURI":
					navigateToURL(new URLRequest(myView.txbVideoURI.label), "_blank");
					break;
				
				case "btnEditFeed":
					var selectedFeed:Object = myView.trFeeds.selectedItem;
					var objectType:String	= objectType = getQualifiedClassName(selectedFeed);
					
					if (!selectedFeed || objectType == "VOs::FacebookPageVO") {
						Alert.show("Please select a feed to edit first","Information");
						return;
					}
					
					tempUserFeed = new UserFeedVO();
					tempUserFeed.loadDataFromResult(selectedFeed);
					
					tempFeedView = showPopUpView("FeedView", tempUserFeed, "", true, true, "Edit Feed") as FeedView;
					tempFeedView.controller.addEventListener("PROCESSED_FEED", feedViewClosed);
					
					break;
				
				case "btnDeleteFeed":
					var selectedFeed:Object = myView.trFeeds.selectedItem;
					var objectType:String	= objectType = getQualifiedClassName(selectedFeed);
					
					if (!selectedFeed || objectType == "VOs::FacebookPageVO") {
						Alert.show("Please select a feed to edit first","Information");
						return;
					}
					
					tempUserFeed = new UserFeedVO();
					tempUserFeed.loadDataFromResult(selectedFeed);
					
					var alert:Alert = Alert.show("Are you sure you want to delete this feed?", "Delete", Alert.YES | Alert.NO, view, deleteEntityRequestResponse);
					
					break;
				
				case "btnViewLinkContent":
					m_pageId 			= myView.cmbFanPage.selectedItem.data;
					m_pageAccessToken 	= FacebookPageVO(myView.cmbFanPage.selectedItem).access_token;
					break;
				case "btnAddFeed":
					tempUserFeed = new UserFeedVO();
					tempFeedView = showPopUpView("FeedView", tempUserFeed, "", true, true, "Add Feed") as FeedView;
					tempFeedView.controller.addEventListener("PROCESSED_FEED", feedViewClosed);
					break;
				case "btnChangeVideoThumbnail":
					selectUserImage();
					break;
				case "btnCloseIconsGrid":
					break;
				case "btnIcons":
					var tempToolbox:ToolBox = showPopUpView("ToolBox", new Object(),"",true, true,"") as ToolBox; //PopUpManager.createPopUp(view.parentApplication, ImageBackground);
					break;
				case "btnAddText":
					addTextToImage();
					break;
				case "btnSelectImage":
					selectUserImage();
					break;
				case "btnPostSchedule":
					if(!viewValidated()) {
						return;	
					}
					
					postSuccessList = [];
					
					if(ApplicationModel.pagesWithTargets.length > 0) {
						var pageWithTarget:PageWithTargetVO = ApplicationModel.pagesWithTargets.getItemAt(0) as PageWithTargetVO;
						recentPostToPageId = pageWithTarget.pageId;
						ApplicationModel.pagesWithTargets.removeItemAt(0);
						postSchedule(pageWithTarget);
					} else {
						recentPostToPageId = myView.cmbFanPage.selectedItem.data;
						postSchedule();
					}
					break;
				
				case "chk_postnow":
					if(!viewValidated()) {
						return;	
					}
					
					postSchedule();
					break;
				
				case "chk_postlater":
					if(!viewValidated()) {
						return;	
					}
					
					postSchedule();
					break;
				
				case "btnClearContent":
					clearViewContent();
					break;
				case "btnStartOver":

					var tempImageBackground:ImageBackground = showPopUpView("ImageBackground", new Object(),"",true, true,"") as ImageBackground; //PopUpManager.createPopUp(view.parentApplication, ImageBackground);
					tempImageBackground.instance = myView;

					break;
				case "btnSearchStatusIdea":
					tempStatusIdeaView = showPopUpView("StatusIdeaView", new Object(),"",true, true, "Search Status Idea") as StatusIdeaView;
					tempStatusIdeaView.controller.addEventListener("STATUS_IDEA_SELECTED", statusIdeaSelected);
					break;
					
				case "btnSearchStatusIdea1":
					tempStatusIdeaView = showPopUpView("StatusIdeaView", new Object(),"",true, true, "Search Status Idea") as StatusIdeaView;
					tempStatusIdeaView.controller.addEventListener("STATUS_IDEA_SELECTED", statusIdeaSelected);
					
					break;
				case "btnSearchContentIdea":
					tempContentIdeaView = showPopUpView("ContentIdeaView", new Object(),"",true, true, "Search Content Idea") as ContentIdeaView;
					tempContentIdeaView.controller.addEventListener("CONTENT_IDEA_SELECTED", contentIdeaSelected);

					break;
				case "btnSearchImages":
					tempImageIdeaView = showPopUpView("ImageSearchView", null, "", true, true, "Search Images") as ImageSearchView;
					tempImageIdeaView.controller.addEventListener("IMAGE_SELECTED", ImageIdeaSelected);
 					break;
				case "btnSettings":
					Alert.show("Settings button");
					break;
				case "btnSearchVideos":
					tempVideoIdeaView = showPopUpView("VideoSearchView", null, "", true, true, "Search Videos") as VideoSearchView;
					tempVideoIdeaView.controller.addEventListener("VIDEO_SELECTED", videoIdeaSelected);
					break;
				case "btnHashTags":
					var hashTagsPopup:HashTagsPopup = new HashTagsPopup();
					PopUpManager.addPopUp(hashTagsPopup,FlexGlobals.topLevelApplication as DisplayObject,false);
					PopUpManager.centerPopUp(hashTagsPopup);
					hashTagsPopup.addEventListener("HASH_TAG_WAS_SELECTED",hashTagWasSelected);
					hashTagsPopup.hashTags = getFanPageHashTags(myView.cmbFanPage.selectedItem as FacebookPageVO);
					break;
				
				case "btnHolidays":
					var holidays:HolidaysMonthlyPopup = new HolidaysMonthlyPopup();
					PopUpManager.addPopUp(holidays,FlexGlobals.topLevelApplication as DisplayObject,false);
					PopUpManager.centerPopUp(holidays);
					break;
				
				case "loadImageFromUrl":
					var loadImagePopup:LoadImageFromURL = new LoadImageFromURL();
					PopUpManager.addPopUp(loadImagePopup,FlexGlobals.topLevelApplication as DisplayObject,false);
					PopUpManager.centerPopUp(loadImagePopup);
					loadImagePopup.addEventListener(GenericEvent.IMAGELOADEDFROMURL,loadImageFromURL);
					break;
				case "btnGridLines":
					showGridLines = !showGridLines;
					break;
				
				case "btn_movebackward":
					moveImageBackward();
					break;
				
				case "btn_bringforward":
					bringImageForward();
					break;
				
				case "btn_textlayer":
					addTextlayer();
					break;
				
				default:
					trace("ERROR: no case for switch on MainViewController-buttonClicked for target: " + m_target);
					break;
			}
		}
	   
	   private function deleteEntityRequestResponse(event:CloseEvent):void {
		   if(event.detail == Alert.YES) {
			   tempUserFeed.addEventListener("TRANSACTION_FAILED",deleteFeedError);
			   tempUserFeed.addEventListener("TRANSACTION_SUCCESS",deleteFeedSuccess);
			   tempUserFeed.deleteVO();
		   }
	   }
	   
	   private function deleteFeedError(evt:Event):void {   
		   tempUserFeed.removeEventListener("TRANSACTION_FAILED",deleteFeedError);
		   tempUserFeed.removeEventListener("TRANSACTION_SUCCESS",deleteFeedSuccess);
		   if(tempUserFeed) {
			   tempUserFeed = null;
		   }
		   Alert.show("Error removing feed");
	   }
	   
	   private function deleteFeedSuccess(evt:Event):void {
		   tempUserFeed.removeEventListener("TRANSACTION_FAILED",deleteFeedError);
		   tempUserFeed.removeEventListener("TRANSACTION_SUCCESS",deleteFeedSuccess);
		   if(tempUserFeed) {
			   tempUserFeed = null;
		   }
	   		fetchTreeData();
	   }
	   
	   private function addTextlayer():void {
		   if(!textLayer) {
			   textLayer = new TextLayer();
			   myView.imgFinalImage.addChild(textLayer);
			   textLayer.percentWidth = 100;
			   textLayer.height = 80;
			   textLayer.y = myView.imgFinalImage.height - textLayer.height;
			   myView.imgFinalImage.setChildIndex(textLayer,myView.imgFinalImage.numChildren-1);
			   objectHandles.registerComponent(textLayer,textLayer);
		   } else {
			  objectHandles.selectionManager.clearSelection(); 
			  myView.imgFinalImage.removeChild(textLayer);
			  textLayer = null;
		   }
	   }
	   
	   private function loadImageFromURL(event:GenericEvent):void {
		   var tempCropImageView:CropImageView;
		   tempCropImageView = showPopUpView("CropImageView", null, "", true, true, "Crop Image") as CropImageView;
		   tempCropImageView.controller.delegate = this;
		   tempCropImageView.controller.assignImageToCropControl(event.data.imageData);
	   }
	   
	   private function postSchedule(pageWithTarget:PageWithTargetVO = null):void {
		   
		   FlexGlobals.topLevelApplication.disableApp();
		   
		   // Validate if this is a scheduled post
		   var postScheduleTime:int;
		   var m_pageId:String;
		   var m_pageAccessToken:String;
		   var m_postText:String;
		   
		   var link:String;
		   var source:String;
		   var picture:String;
		   var postText:String;
		   var m_videoSource:String;
		   
		   var m_link:String;
		   var m_picture:*;
		   var m_source:String; 
		   
		   var m_target:String = "";
		   
		   if (myView.dtpDatetime.publishState == myView.dtpDatetime.PUBLISH_STATE_SCHEDULED ) {
			   
			   if (!validateScheduledTime(myView.dtpDatetime.selectedDate)) {
				   return;	
			   }
			   
			   if(pageWithTarget && pageWithTarget.target != "" && pageWithTarget.dateTime) {
				   postScheduleTime = QuickDateFormatter.getUnixStampTimeFromDate(pageWithTarget.dateTime);
			   } else {
				   postScheduleTime = QuickDateFormatter.getUnixStampTimeFromDate(myView.dtpDatetime.selectedDate);
			   }
		   } 		
		   
		   if(pageWithTarget) {
			   m_target = pageWithTarget.target;
		   }
		   
		   
		   //isViewDisabled(true);
		   model.appStateVars.facebookCOM.removeEventListener("WALL_TEXT_POST_SENT", wallTextPostedSuccess);
		   model.appStateVars.facebookCOM.removeEventListener("WALL_TEXT_POST_SEND_FAILED", wallTextPostFailed);
		   model.appStateVars.facebookCOM.removeEventListener("WALL_IMAGE_POST_SENT", wallImagePostedSuccess);
		   model.appStateVars.facebookCOM.removeEventListener("WALL_IMAGE_POST_SEND_FAILED", wallImagePostFailed);
		   
		   model.appStateVars.facebookCOM.addEventListener("WALL_TEXT_POST_SENT", wallTextPostedSuccess);
		   model.appStateVars.facebookCOM.addEventListener("WALL_TEXT_POST_SEND_FAILED", wallTextPostFailed);
		   model.appStateVars.facebookCOM.addEventListener("WALL_IMAGE_POST_SENT", wallImagePostedSuccess);
		   model.appStateVars.facebookCOM.addEventListener("WALL_IMAGE_POST_SEND_FAILED", wallImagePostFailed);
		   
		   m_pageId 			= pageWithTarget == null?myView.cmbFanPage.selectedItem.data:pageWithTarget.pageId;
		   if(!pageWithTarget) {
		   		m_pageAccessToken 	= FacebookPageVO(myView.cmbFanPage.selectedItem).access_token;
		   } else {
			   for each(var page:FacebookPageVO in model.appStateVars.facebookCOM.facebookPagesArray) {
				   if(page.id == pageWithTarget.pageId) {
					   m_pageAccessToken 	= page.access_token;	   
				   }
			   }
		   }
		   m_postText 			= myView.txbPostText.text;
		   
		   if (myView.currentState == "ImageState") {
			   try {
				   objectHandles.selectionManager.clearSelection();
				   myView.transformerTool.visible = false;
			   } catch(e:Error) {
				   trace("no image or text selected");
			   }
			   //isViewDisabled(true);
			   if(pageWithTarget) 
			   		captureImageAndSaveOrSend(m_pageId, m_pageAccessToken,m_target,pageWithTarget.dateTime);
			   else 
				   captureImageAndSaveOrSend(m_pageId, m_pageAccessToken,m_target,null);
		   } else if (myView.currentState == "VideoState" ) { // Video State
			   //isViewDisabled(true);
			   
			   if(tempVideoIdeaView) 
			   {
				   _selectedVideoObject = tempVideoIdeaView.controller.selectedVideoObj;
			   }
			   if(_selectedVideoObject) 
			   {
				   link 	= _selectedVideoObject.videoURI==undefined?"https://www.youtube.com/watch?v=" + _selectedVideoObject.id.videoId:_selectedVideoObject.videoURI;
				   source 	= "http://www.youtube.com/e/" + (_selectedVideoObject.videoCode==undefined?_selectedVideoObject.id.videoId:_selectedVideoObject.videoCode) + "?autoplay=1";
			   
				   picture = myView.li.selectedImageSource;
				   
				   _selectedVideoObject.videoThumbnail;
				   postText = myView.txbPostText.text;
				   m_videoSource = source;
				   
				   if (m_videoSource.toUpperCase().indexOf("YOUTUBE") != -1) {
					   m_link 		= link;
					   m_source 	= source;
					   m_picture  	= myView.li.selectedImageSource;
				   } else {
					   m_link 		= _selectedVideoObject.videoURI;
					   m_source 	= _selectedVideoObject.videoURI;
					   m_picture  	= myView.li.selectedImageSource;
				   }
			   } else {
				   m_link 		= myView.li.url;
				   m_source 	= myView.li.url;
				   m_picture  	= myView.li.selectedImageSource;
			   }
			   
			   m_postText = myView.txbPostText.text;
			   
			   model.appStateVars.facebookCOM.postLinkToPageWall((myView.cmbFanPage.selectedIndex==0),m_pageId, m_postText, m_pageAccessToken, postScheduleTime,myView.dtpDatetime.selectedDate, m_link, m_source, m_picture,m_target);
			   
		   } else { // Blog, News or RSS feed link, or just empty text ( LINK)
			   if (tempContentIdeaView) {
				   m_link 		= tempContentIdeaView.controller.selectedContentIdeaVO.content_link_url 
				   m_source 	= tempContentIdeaView.controller.selectedContentIdeaVO.content_link_url;
			   } else if (tempUserFeed) {
				   
				   if(myView.li.tUrl.text != "") 
				   {
					   m_link 		= tempUserFeed.link 
					   m_source 	= tempUserFeed.link;
				   }
				   
				   if(myView.li.url && myView.li.url != "") {
					   m_link 		= myView.li.url; 
					   m_source 	= myView.li.url;	
				   }
				   
			   } else {
				   m_link 		= myView.li.url; 
				   m_source 	= myView.li.url;
			   }
				 
			   m_picture = myView.li.getSelectedImageLink();
			   //isViewDisabled(true);
			  model.appStateVars.facebookCOM.postLinkToPageWall((myView.cmbFanPage.selectedIndex==0),m_pageId, m_postText, m_pageAccessToken, postScheduleTime,myView.dtpDatetime.selectedDate, m_link, m_source, m_picture,m_target);
		   }   
	   }
	   
	   private function clearViewContent():void {
		   
		   trace("Going to remove everything");
			
		   myView.txbPostText.text = "";
		   
		   tempContentIdeaView = null;
		   tempUserFeed = null;
		   
		   
		   
		  try { 
			   var numElements:int = myView.imgFinalImage.numElements;
			   for (var i:int = 0; i < numElements; i++) {
				   var object:IVisualElement= myView.imgFinalImage.getElementAt(i) as IVisualElement;
				   trace("id = " + object["id"] );
				  if (object["id"] == null) {
					  myView.imgFinalImage.removeElement(object);
					  i--
				  }				  
			   }
			   objectHandles.selectionManager.clearSelection();
			   myView.transformerTool.visible = false;
		  } catch(e:Error){
		   
		  }
		  finally {
			  if (myView.currentState == "ImageState") {
				myView.imageCropper.sourceImage = "";
				myView.imgFinalImage.setStyle('backgroundImage', "");
				myView.brgCrop.visible = false;
				myView.imgFinalImage.horizontalScrollPolicy = "off";
				myView.imgFinalImage.verticalScrollPolicy = "off";
				
				myView.imgBackground.source = null;
			  }
				myView.li.clearAll();
				myView.brcContent.height = 50;
				if (myView.txbVideoURI) {
					myView.txbVideoURI.label = "";
					myView.imgVideoImage.source = null;
				}
		  }
	   }
	   
	   
	   
	   private function isViewDisabled(pState:Boolean):void {
		   myView.pgrContentProgress.visible = pState;   
		   myView.enabled = !pState;
	   }
	   
	   private function viewValidated():Boolean {
			var result:Boolean = true;
			
		   if (myView.cmbFanPage.selectedIndex == -1) {
				Alert.show("Please select a fan page first !","Information");
				myView.cmbFanPage.setFocus();
				myView.cmbFanPage.drawFocus(true);
				result = false;
			}   
		   
		   return result;
	   }
	   
	   private function videoIdeaSelected(evt:Event):void {
		   _selectedVideoObject = tempVideoIdeaView.controller.selectedVideoObj;
		   myView.imgVideoImage.source=tempVideoIdeaView.controller.selectedVideoObj.snippet.thumbnails.default.url;
		   myView.txbVideoURI.label = "www.youtube.com/watch?v="+tempVideoIdeaView.controller.selectedVideoObj.id.videoId;
		   myView.li.clearAll();
		   myView.li.url = "www.youtube.com/watch?v="+tempVideoIdeaView.controller.selectedVideoObj.id.videoId;
		   myView.li.tUrl.text = "www.youtube.com/watch?v="+tempVideoIdeaView.controller.selectedVideoObj.id.videoId;
		   myView.li.attach();
	   }
	   
	   public function feedImageSelected(event:GenericEvent):void {
		   var tempCropImageView:CropImageView;
		   
		   if (myView.currentState == "ImageState") {
			   tempCropImageView = showPopUpView("CropImageView", null, "", true, true, "Crop Image") as CropImageView;
			   tempCropImageView.controller.delegate = this;
			   tempCropImageView.controller.assignImageToCropControl(event.data.imgPath);
		   } 
	   }
	   
	   private function ImageIdeaSelected(evt:Event):void {
		   var tempCropImageView:CropImageView;
	 		
		   if (myView.currentState == "ImageState") {
			   
			   tempImageIdeaView.controller.removePopup();
				//myView.imgPostImage.imgObject.load(tempImageIdeaView.controller.selectedImageURL);
				//myView.imageCropper.sourceImage = tempImageIdeaView.controller.selectedImageURL;
		 
			   
			   
			   tempCropImageView = showPopUpView("CropImageView", null, "", true, true, "Crop Image") as CropImageView;
			   tempCropImageView.controller.delegate = this;
			   tempCropImageView.controller.assignImageToCropControl(tempImageIdeaView.controller.selectedImageURL);
			   
			  
			   
				//CropImageView(tempCropImageView).imageCropper.sourceImage = sourceBMP;
				// TODO: When imagecropper is done, pass the image back to this view
				
				//myView.brgCrop.visible = true;
				//myView.imgFinalImage.horizontalScrollPolicy = "auto";
				//myView.imgFinalImage.verticalScrollPolicy = "auto";
		   } else if(myView.currentState == "VideoState") {
			   myView.imgVideoImage.load(tempImageIdeaView.controller.selectedImageURL);
		   }
		   
		   
		   
		   
			/*tempImageIdeaView.controller.removePopup();*/
		   
	   }
	   
	   public function changeImageBackgroundFill(event:ColorPickerEvent):void {
		   var change:Change = new Change();
		   if(objectHandles.selectionManager.currentlySelected.length > 0 && objectHandles.selectionManager.currentlySelected[0] is TextLayer) {
			   change.type = "textLayerBack";
			   change.oldBackgroundColor = textLayer.getStyle("backgroundColor");
		   		textLayer.setStyle("backgroundColor",event.color);
				change.newBackgroundColor = event.color;
		   } else {
			   change.type = "BackgroundColor";
			   change.oldBackgroundColor = imageBackgroundFill;
			   imageBackgroundFill = event.color;
				change.newBackgroundColor = imageBackgroundFill;
		   }
		   pushtoStack(change);
	   }
	   
	   
	   public function undo():void{
		   objectHandles.selectionManager.clearSelection();
		   var change:Change = topnpop();
		   if (change == null)
			   return;
		   if(change.type == "textLayerBack"){
		   	textLayer.setStyle("backgroundColor",change.oldBackgroundColor);
		   }else if(change.type == "BackgroundColor"){
			   imageBackgroundFill = change.oldBackgroundColor;
		   }else if(change.type == "addText"){
			   myView.imgFinalImage.removeChild(change.changedObject);
		   }else if(change.type == "addImage"){
			   myView.imgFinalImage.removeChild(change.changedObject);
		   }else if(change.type == "imageMoved"){
			   change.changedObject.x = change.oldX;
			   change.changedObject.y = change.oldY;
			   change.changedObject.height = change.oldH;
			   change.changedObject.width = change.oldW;
			   change.changedObject.rotation = change.oldRotation;
		   }else if(change.type == "backgoundImage"){
		   		myView.imgBackground.source = change.oldSource;
		   }else if(change.type == "ImageDeleted"){
			   myView.imgFinalImage.addChild(change.changedObject);
			   objectHandles.registerComponent( change.changedObject, change.changedObject );
			   change.changedObject.addEventListener(MouseEvent.MOUSE_DOWN,objectSelected);
		   }else if(change.type == "TextDeleted"){
			   myView.imgFinalImage.addChild(change.changedObject);
			   change.changedObject.addEventListener(MouseEvent.MOUSE_DOWN,objectSelected);
			   myView.connector.target = change.changedObject as UIComponent;
		   }else if(change.type == "ImageEffects"){
		   		change.changedObject.filters = change.oldFilters;
		   }
	   }
	   
	   public function redo():void{
		   objectHandles.selectionManager.clearSelection();
		   var change:Change = top();
		   if (change == null)
			   	return;
		   if(change.type == "textLayerBack"){
			   textLayer.setStyle("backgroundColor",change.newBackgroundColor);
		   }else if(change.type == "BackgroundColor"){
			   imageBackgroundFill = change.newBackgroundColor;
		   }else if(change.type == "addText"){
			   myView.imgFinalImage.addChild(change.changedObject);
			   change.changedObject.addEventListener(MouseEvent.MOUSE_DOWN,objectSelected);
			   myView.connector.target = change.changedObject as UIComponent;
		   }else if(change.type == "addImage"){
			   myView.imgFinalImage.addChild(change.changedObject);
			   change.changedObject.x = change.oldX;
			   change.changedObject.y = change.oldY;
			   change.changedObject.height = change.oldH;
			   change.changedObject.width = change.oldW;
			   objectHandles.registerComponent( change.changedObject, change.changedObject );
			   change.changedObject.addEventListener(MouseEvent.MOUSE_DOWN,objectSelected);
		   }else if(change.type == "imageMoved"){
			   change.changedObject.x = change.newX;
			   change.changedObject.y = change.newY;
			   change.changedObject.height = change.newH;
			   change.changedObject.width = change.newW;
			   change.changedObject.rotation = change.newRotation;
		   }else if(change.type == "backgoundImage"){
			   myView.imgBackground.source = change.newSource;
		   }else if(change.type == "ImageDeleted"){
			   myView.imgFinalImage.removeChild(change.changedObject);
		   }else if(change.type == "TextDeleted"){
			   myView.imgFinalImage.removeChild(change.changedObject);
		   }else if(change.type == "ImageEffects"){
			   change.changedObject.filters = change.newFilters;
		   }
	   }
	   
	   private var postSuccessList:Array = [];
	   
	   private var recentPostToPageId:String = "";
	   
	   private function wallTextPostedSuccess(evt:Event):void {
		   var postSuccessMessage:Object = {};
		   if(ApplicationModel.pagesWithTargets.length == 0) {
			   myView.enabled = true;
			   model.appStateVars.facebookCOM.removeEventListener("WALL_TEXT_POST_SENT", wallTextPostedSuccess);
			   if(myView.currentState == "State1") {
				   if(myView.li.tUrl.text == "") {
					   var postConf:PostConfirmationAlert = new PostConfirmationAlert();
					   PopUpManager.addPopUp(postConf,FlexGlobals.topLevelApplication as DisplayObject,true);
					   PopUpManager.centerPopUp(postConf);
					   
					   postSuccessMessage.postId = (evt as DataEvent).data;
					   postSuccessMessage.pageId = recentPostToPageId;
					   postSuccessList.push(postSuccessMessage);
					   postConf.postMessageList = postSuccessList;
					   
					   postConf.message = "Status Update succesfully posted to Facebook";
					   postConf.title = "Information";
				   } else {
					   var postConf:PostConfirmationAlert = new PostConfirmationAlert();
					   PopUpManager.addPopUp(postConf,FlexGlobals.topLevelApplication as DisplayObject,true);
					   PopUpManager.centerPopUp(postConf);
					   
					   postSuccessMessage.postId = (evt as DataEvent).data;
					   postSuccessMessage.pageId = recentPostToPageId;
					   postSuccessList.push(postSuccessMessage);
					   postConf.postMessageList = postSuccessList;
					   
					   postConf.message = "Link post succesfully posted to Facebook";
					   postConf.title = "Information";
				   }
			   } else {
				   var postConf:PostConfirmationAlert = new PostConfirmationAlert();
				   PopUpManager.addPopUp(postConf,FlexGlobals.topLevelApplication as DisplayObject,true);
				   PopUpManager.centerPopUp(postConf);
				   
				   postSuccessMessage.postId = (evt as DataEvent).data;
				   postSuccessMessage.pageId = recentPostToPageId;
				   postSuccessList.push(postSuccessMessage);
				   postConf.postMessageList = postSuccessList;
				   
				   postConf.message = "Video post succesfully posted to Facebook";
				   postConf.title = "Information";
			   }
			   //isViewDisabled(false);
			   
			   ApplicationModel.pagesWithTargets.removeAll();
			   
			   model.appStateVars.selectedMultiFanPagesToPost.removeAll();
			   
			   model.appStateVars.facebookCOM.getPostsSummary(myView.cmbFanPage.selectedItem.data,(myView.cmbFanPage.selectedIndex==0));
		   } else {
			   
			   if(myView.li.tUrl.text == "") {
				   
				   postSuccessMessage.message = "Status Update succesfully posted to Facebook";
				   postSuccessMessage.postId = (evt as DataEvent).data;
				   postSuccessMessage.title = "Information";
				   postSuccessMessage.pageId = recentPostToPageId;
				   postSuccessList.push(postSuccessMessage);
				   
			   } else {
				   postSuccessMessage.message = "Link post succesfully posted to Facebook";
				   postSuccessMessage.postId = (evt as DataEvent).data;
				   postSuccessMessage.title = "Information";
				   postSuccessMessage.pageId = recentPostToPageId;
				   postSuccessList.push(postSuccessMessage);
			   }
			   
			   var pageWidthTarget:PageWithTargetVO = ApplicationModel.pagesWithTargets.getItemAt(0) as PageWithTargetVO;
			   recentPostToPageId = pageWidthTarget.pageId;
			   ApplicationModel.pagesWithTargets.removeItemAt(0);
			   ApplicationModel.pagesWithTargets.refresh();
			   postSchedule(pageWidthTarget);
		   }
		   //myView.li.clearAll();
		   //clearViewContent();
		   
	   }
	   
	   private function wallImagePostedSuccess(evt:Event):void {
		   var postSuccessMessage:Object = {};
		   if(ApplicationModel.pagesWithTargets.length == 0) {
			   myView.enabled = true;
			   model.appStateVars.facebookCOM.removeEventListener("WALL_IMAGE_POST_SENT", wallImagePostedSuccess);
			   /*var postConf:PostConfirmationAlert = new PostConfirmationAlert();
			   PopUpManager.addPopUp(postConf,FlexGlobals.topLevelApplication as DisplayObject,true);
			   PopUpManager.centerPopUp(postConf);
			   postConf.message = "Image posted succesfully posted to Facebook";
			   postConf.postId = (evt as DataEvent).data;
			   postConf.title = "Information";*/
			   /*isViewDisabled(false);*/
			   
			   var postConf:PostConfirmationAlert = new PostConfirmationAlert();
			   PopUpManager.addPopUp(postConf,FlexGlobals.topLevelApplication as DisplayObject,true);
			   PopUpManager.centerPopUp(postConf);
			   
			   postSuccessMessage.postId = (evt as DataEvent).data;
			   postSuccessMessage.pageId = recentPostToPageId;
			   postSuccessList.push(postSuccessMessage);
			   postConf.postMessageList = postSuccessList;
			   
			   postConf.message = "Image posted succesfully posted to Facebook";
			   postConf.title = "Information";
			   
			   
			   //clearViewContent();
		   } else {
			   postSuccessMessage.message = "Image posted succesfully posted to Facebook";
			   postSuccessMessage.postId = (evt as DataEvent).data;
			   postSuccessMessage.title = "Information";
			   postSuccessMessage.pageId = recentPostToPageId;
			   postSuccessList.push(postSuccessMessage);
			   if(ApplicationModel.pagesWithTargets.length > 0)
			   {
				   var pageWithTarget:PageWithTargetVO = ApplicationModel.pagesWithTargets.getItemAt(0) as PageWithTargetVO;
				   ApplicationModel.pagesWithTargets.removeItemAt(0);
				   ApplicationModel.pagesWithTargets.refresh();
				   recentPostToPageId = pageWithTarget.pageId;
				   postSchedule(pageWithTarget);
			   }
		   }
		   model.appStateVars.facebookCOM.getPostsSummary(myView.cmbFanPage.selectedItem.data,(myView.cmbFanPage.selectedIndex==0));
		   FlexGlobals.topLevelApplication.enableApp();
	   }
	   
	   private function wallTextPostFailed(evt:Event):void {
		   
 		   model.appStateVars.facebookCOM.removeEventListener("WALL_TEXT_POST_SEND_FAILED", wallTextPostFailed);
		   /*isViewDisabled(false);*/
		   FlexGlobals.topLevelApplication.enableApp();
		   Alert.show("Text post failed to be posted to Facebook");
		   
	   }
	   
	   private function wallImagePostFailed(evt:Event):void {
		   /*isViewDisabled(false);*/
		   FlexGlobals.topLevelApplication.enableApp();
 		   model.appStateVars.facebookCOM.removeEventListener("WALL_IMAGE_POST_SEND_FAILED", wallImagePostFailed);
		   Alert.show("Image post failed to be posted to Facebook");
	   }
	   
	   private function contentIdeaSelected(evt:Event):void {
		   var tempContentIdea:ScheduledPostVO = evt.target.selectedContentIdeaVO as ScheduledPostVO;
		   myView.li.tUrl.text = tempContentIdea.content_link_url;
		   myView.li.attach();
		   
	   }
	   
	   public function statusIdeaSelected(evt:Event):void {
		   var tempStatusIdea:StatusIdeaVO = evt.target.selectedStatusIdea as StatusIdeaVO;
		   myView.txbPostText.text = tempStatusIdea.status_idea;
	   }
	   
	   private var collection:ArrayCollection;
	   
	   public function setupTreeData(pUserFeedsArray:Array):void {
		   var array:Array;
		   var tempFanPage:FacebookPageVO;
		   var tempUserFeed:Object;
		   
		   array = new Array();
		   
		   var str:String = "<root>"
		   for (var t:int = 0; t < model.appStateVars.facebookCOM.facebookPagesArray.length; t++) {
			   tempFanPage = model.appStateVars.facebookCOM.facebookPagesArray[t];
			   tempFanPage.children = new Array();
			   tempFanPage.label = tempFanPage.label;
			   str += "<fanpage name='"+tempFanPage.label+"'>"
			   for (var g:int = 0; g < pUserFeedsArray.length; g++) {   		
			   		if (pUserFeedsArray[g].fan_page_id == tempFanPage.id) {
						tempUserFeed = pUserFeedsArray[g];
						tempUserFeed.label = pUserFeedsArray[g].title;
						tempFanPage.children.push(tempUserFeed);
						str += "<feed link='"+pUserFeedsArray[g].link+"'>"+pUserFeedsArray[g].title+"</feed>";
					}
			   }
			   str += "</fanpage>";
			   array.push(tempFanPage);
		   }
		   str += "</root>";
		   exportString = str;
		  
		   collection = new ArrayCollection(array);
		   
		   myView.trFeeds.dataProvider = collection;
		   
		   loadContentlib();
		   
		   loadPostTemplates();
		   
		   myView.callLater(openItem);
	   }
	   
	   
	   private function openItem():void{
	   	if(lastOpenFeed != null){
			myView.trFeeds.expandItem(lastOpenFeed,true);
			myView.callLater(caclulateHeightOfTree);
		}
	   }
	   private function caclulateHeightOfTree():void {
		   myView.trFeeds.height = myView.trFeeds.measureHeightOfItems();
	   }
	   
	   public function loadContentlib():void {
		   var urlLoader:URLLoader = new URLLoader();
		   urlLoader.addEventListener(Event.COMPLETE,libLoadComplete);
		   urlLoader.load(new URLRequest("assets/contentlib.xml"));
		   
		   function libLoadComplete(evt:Event):void {
			   var array:Array = new Array();
			   var category:FacebookPageVO;
			   var libXmlDoc:XMLDocument = new XMLDocument(evt.target.data);
			   for each(var cat:XMLNode in (libXmlDoc.childNodes[0] as XMLNode).childNodes) {
				   if(cat.nodeType == XMLNodeType.ELEMENT_NODE) {
					   category = new FacebookPageVO();
					   category.name = cat.attributes.name;
					   category.label = cat.attributes.name;
					   category.children = new Array();
					   if(cat.hasChildNodes()) {
						   for each(var feed:XMLNode in cat.childNodes) {
							   if(feed.nodeType == XMLNodeType.ELEMENT_NODE) {
									category.children.push({category:null,description:"",fan_page_id:"",id:"",label:feed.attributes.label,link:feed.firstChild.nodeValue,title:feed.attributes.label});
							   }
						   }
					   }
					   array.push(category);
				   }
			   }
			   
			   //var templates:FacebookPageVO = new FacebookPageVO();
			   //templates.children = new Array();
			   //templates.label = 'Post Templates';
			   //templates.type = FacebookPageVO.TOP_CAT;
			   var libArray:Array = new Array();
			   var library:FacebookPageVO = new FacebookPageVO();
				library.label = "Content Library";
				library.type = FacebookPageVO.TOP_CAT;
			   for(var i:int=1;i<array.length;i++) {
				   array[i].type = FacebookPageVO.SUB_CAT;
					libArray.push(array[i]);
			   }
			   
			   library.children = libArray;
			   
			   var contentlibCol:ArrayCollection = new ArrayCollection();
			   //contentlibCol.addItem(templates);
			   contentlibCol.addItem(library);
			   contentlibCol.refresh();
			   
			   var newsSourcesCol:ArrayCollection = new ArrayCollection(contentlibCol.source[0].children[0].children);
			   
			   newsSourcesCol.refresh();
			   
			   contentlibCol.getItemAt(0).children.splice(0, 1);
			   
			   contentlibCol.refresh();
			   
			   myView.trContentLib.dataProvider = contentlibCol;
			   myView.callLater(expandAll);
			   
			   myView.trNewsSources.dataProvider = newsSourcesCol;
			   myView.callLater(expandAllNewsSources);
		   }
	   }
	   
	   private function expandAllNewsSources() :void {
		   var temp:Object = myView.trNewsSources.dataProvider[0];
		   myView.trNewsSources.expandItem(temp, true);
	   }
	   
	   private function expandAll() :void {
		   var temp:Object = myView.trContentLib.dataProvider[0];
		   myView.trContentLib.expandItem(temp, true);
	   }
	   
	   
	   private function loadPostTemplates():void {
		   var urlLoader:URLLoader = new URLLoader();
		   urlLoader.addEventListener(Event.COMPLETE,postTemplatesLoadComplete);
		   urlLoader.load(new URLRequest("assets/posttemplates.xml"));
		   
		   function postTemplatesLoadComplete(event:Event):void {
			   var templatesDoc:XMLDocument = new XMLDocument(event.target.data);
			   var templatesCol:ArrayCollection = new ArrayCollection();
			   var count:int = 1;
			   for(var i:int=0;i<templatesDoc.firstChild.childNodes.length;i++) {
				   var templateNode:XMLNode = templatesDoc.firstChild.childNodes[i] as XMLNode;
				   if(templateNode.nodeName == "template") {
					   var templateObj:Object = {};
					   templateObj.no = count+".";
					   templateObj.title = templateNode.attributes.title;
					   templateObj.text = StringUtil.trim(templateNode.firstChild.nodeValue);
					   templatesCol.addItem(templateObj);
					   count++;
				   }
			   }
			   templatesCol.refresh();
			   myView.dgPostTemplates.dataProvider = templatesCol;
		   }
	   }
	   
	   public function imageDragEnter(evt:Event):void {
		   var croppedImage:Image = evt.target as Image;
		   var target:String = evt.currentTarget.id;
		   var rectangle:Rectangle = new Rectangle(myView.imgFinalImage.x, myView.imgFinalImage.y, myView.imgFinalImage.width, myView.imgFinalImage.height);
		   
		 
		   croppedImage.startDrag(false, rectangle);
		   trace("started drag");
	   }
	   
	   public function startObjectDrag(evt:MouseEvent):void {
					 
		   var croppedImage:Image = evt.target as Image;
		   
		   var target:String = evt.currentTarget.id;
		   
		   var rectangle:Rectangle = new Rectangle(myView.imgFinalImage.x - 15, 
			   									   myView.imgFinalImage.y - 15, 
												   myView.imgFinalImage.width - croppedImage.width + 5, 
												   myView.imgFinalImage.height - croppedImage.height + 5);
		   
		   
		   croppedImage.startDrag(false, rectangle);
	   }
	   
	   public function stopObjectDrag(evt:MouseEvent):void {
		 
		   var croppedImage:Image = evt.target as Image;
		   var target:String = evt.currentTarget.id;
		   croppedImage.stopDrag();
				   
	   }
	   
	   /*public function tree_icon_function(item:Object):Class {
		   var iconUri:String = "";
		   if (myView.trFeeds.dataDescriptor.isBranch(item)) {
			   return treeIconA;
		   } else {
		   	   return treeIconB;
		   }
	   }*/
	   
	   public function tree_itemClick(evt:MouseEvent):void {
		   var t:Tree = evt.currentTarget as Tree;
		   var dataObj:Object = myView.trFeeds.selectedItem;
		   var objectType:String;
		   
		   objectType = getQualifiedClassName(dataObj);
		   
		   var tempUserFeed:UserFeedVO;
		   tempUserFeed = new UserFeedVO();
		   
		   if (dataObj && objectType != "VOs::FacebookPageVO") {
			   tempUserFeed.loadDataFromResult(dataObj);
		   	   displayFeedForSelection(tempUserFeed);
			  
			   myView.pgrFeedLoad.visible = true;
		   }
	   }
	   
	   public function libtree_itemClick(evt:MouseEvent):void {
		   var t:Tree = evt.currentTarget as Tree;
		   var dataObj:Object = t.selectedItem;
		   var objectType:String;
		   
		   objectType = getQualifiedClassName(dataObj);
		   
		   var tempUserFeed:UserFeedVO;
		   tempUserFeed = new UserFeedVO();
		   
		   if (dataObj && objectType != "VOs::FacebookPageVO") {
			   tempUserFeed.loadDataFromResult(dataObj);
			   displayFeedForSelection(tempUserFeed);
			   
			   myView.pgrFeedLoad.visible = true;
		   }
	   }
	   
	   public function selectFeedItem(pFeedObject:Object):void {
			/*myView.txbContentHeader.text = pFeedObject.title;
			myView.txbContentText.htmlText = pFeedObject.description;
			myView.imgContent.visible = true;
			myView.imgContent.source="Media/default-no-image.png";
			var tempContentIdea:ScheduledPostVO = new ScheduledPostVO();
			tempContentIdea.content_header = pFeedObject.title;
			tempContentIdea.content_text = pFeedObject.description;
			tempContentIdea.content_link_url = pFeedObject.link;*/
			
			
			myView.brcContent.visible = true;
			/*myView.txbContentHeader.htmlText = tempContentIdea.content_header;
			myView.txbContentText.htmlText	= tempContentIdea.content_text;*/
			
			myView.li.tUrl.text = pFeedObject.origLink?pFeedObject.origLink:(pFeedObject.link is String?pFeedObject.link:pFeedObject.link.href);
			if(myView.li.tUrl.text == "") {
				myView.li.tUrl.text = extractTargetUrl(pFeedObject.link);
			}
			myView.li.attach();
	   }
	   
	   private function extractTargetUrl(url:String):String {
		   if(url) {
			   if(url.indexOf("&url=")!=-1) {
				   var targetUrl:String = url.substring(url.indexOf("&url=")+5,url.length); 
				   return targetUrl;
			   }
			   
			   if(url.indexOf("/?")!=-1) {
				   var targetUrl:String = url.substring(url.indexOf("/?")+1,url.length); 
				   return url.replace(targetUrl,"");
			   }
			   
			   if(url.indexOf("&utm")!=-1) {
				   var targetUrl:String = url.substring(url.indexOf("&utm")+3,url.length); 
				   return url.replace(targetUrl,"");
			   }
			   
			   if(url.indexOf("?utm")!=-1) {
				   var targetUrl:String = url.substring(url.indexOf("?utm")+3,url.length); 
				   return url.replace(targetUrl,"");
			   }
		   }
		   return url;
	   }
	   
	   public function displayFeedForSelection(pFeedVO:UserFeedVO):void {
		   tempUserFeed = pFeedVO;
		   tempUserFeed.addEventListener("FEED_LOADED", feedsWereLoaded);  
		   tempUserFeed.addEventListener("FEED_LOADED_FAILED", feedsLoadFailed);  
		   tempUserFeed.loadFeedFromSource();		  
	   }
	   
	   private function feedsLoadFailed(e:Event):void {
		   Alert.show('Incorrect feed');
		   myView.pgrFeedLoad.visible = false;
	   }
	   
	   private function feedsWereLoaded(evt:Event):void {
		    var tempView:FeedItemSelectView = showPopUpView("FeedItemSelectView", tempUserFeed, "",true, true, "Select Feed Item") as FeedItemSelectView;
			tempView.controller.delegate = this;
		    myView.pgrFeedLoad.visible = false;
			evt.stopImmediatePropagation();
	   }
	   
	   public function dataHasBeenLoaded(pData:Array):void {	   		   
		   setupTreeData(pData);
	   }
	   
	   // for redo
	   public function top():Change {
		   var change:Change;
		   
		   if (_redoBuffer > _topOfStack) {
			   if (_undoRedoStack.getItemAt(_topOfStack + 1) != null) {
				   change = _undoRedoStack.getItemAt(_topOfStack + 1) as Change;
				   _topOfStack++;
			   }
		   }
		   
		   return change;
	   }
	   // for undo
	   public function topnpop():Change {
		   var change:Change;
		   
		   if (isEmpty()) {
			   return null;
		   } else {
			   change = _undoRedoStack.getItemAt(_topOfStack--) as Change; //top
			   
			   if (_redoBuffer - _topOfStack > 100) {
				   _undoRedoStack.removeItemAt(_redoBuffer--); //pop
			   }
			   
			   return change;
		   }
	   }
	   
	   private function isEmpty():Boolean {
		   return _topOfStack == -1;
	   }
	   
	   public function pushtoStack(change:Change):void {
		   if(change != null ) {
			   _undoRedoStack.addItemAt(change, ++_topOfStack);	
		   }
		   // when any style change will be in made in paragraph
		   // then it will show undo button is enable
		   _redoBuffer = _topOfStack;
	   }
	   
	}
}
	