package Controllers
{
	import Interfaces.iController;
	
	import Models.*;
	
	import VOs.GoogleBlogResultVO;
	import VOs.GoogleNewsResultVO;
	import VOs.ScheduledPostVO;
	
	import Views.*;
	
	import flash.utils.getQualifiedClassName;
	
	import mx.controls.Alert;
	import mx.utils.URLUtil;
	
	import spark.components.BorderContainer;
	
	public class ContentIdeaViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:ContentIdeaView;
 	   
	   [Bindable] public var model:ContentIdeaViewModel = new ContentIdeaViewModel(this);
	      
	   [Bindable] public var selectedContentIdeaVO:ScheduledPostVO;
	   
	   public function countHasBeenLoaded(object:Array):void{};
	   
	   public function ContentIdeaViewController() {
				super();
	   }
	   
	   public function init(event:Event):void {
		   myView = view as ContentIdeaView;
		  
	   }
		
	   private function extractTargetUrl(url:String):String {
		   if(url) {
			   if(url.indexOf("&url=")!=-1) {
				   var targetUrl:String = url.substring(url.indexOf("&url=")+5,url.length); 
				   return targetUrl;
			   }
			   
			   if(url.indexOf("/?")!=-1) {
				   var targetUrl:String = url.substring(url.indexOf("/?")+1,url.length); 
				   return url.replace(targetUrl,"");
			   }
			   
			   if(url.indexOf("&utm")!=-1) {
				   var targetUrl:String = url.substring(url.indexOf("&utm")+3,url.length); 
				   return url.replace(targetUrl,"");
			   }
			   
			   if(url.indexOf("?utm")!=-1) {
				   var targetUrl:String = url.substring(url.indexOf("?utm")+3,url.length); 
				   return url.replace(targetUrl,"");
			   }
		   }
		   return url;
	   }
	   
	   public function selectedContentIdea(selectedObject:Object, pLoadedImageData:Object):void {
		   this.selectedContentIdeaVO = new ScheduledPostVO();
		   
		   var className:String = flash.utils.getQualifiedClassName( selectedObject );
		   
		   this.selectedContentIdeaVO.content_text 		= selectedObject.content;
		   this.selectedContentIdeaVO.content_link_url 	= extractTargetUrl(selectedObject.postUrl);
	 	   this.selectedContentIdeaVO.content_header 	= selectedObject.title;
		   this.selectedContentIdeaVO.image_url			= selectedObject.image_url;
		   this.selectedContentIdeaVO.image_data		= selectedObject.img_data;
		   this.selectedContentIdeaVO.content_link_url = selectedObject.postUrl || selectedObject.link;
		   this.selectedContentIdeaVO.content_link_url = extractTargetUrl(this.selectedContentIdeaVO.content_link_url);
		   
		   this.dispatchEvent(new Event("CONTENT_IDEA_SELECTED"));
		   this.removeMe(null);  
	   }
	   
	   public function selectionChanged(evt:Event):void {
		   var target:String = evt.target.id;
		   
		   switch (target) {
			  case "cmbSource":
				   
				   model.resetSearch();
				   break;
			   
			   default:
				   trace("no case for switch on PostViewController, target = " + target);
				   break;
		   }
	   }
	   
	   public function endOfResultSetReached():void {
		   Alert.show("You have reached the end of result set","Information");
		   myView.btnNext.enabled = false;   
	   }
	   
	   public function perfromSearch(searchedText:String):void{
		   myView.pgrBar.visible = true;
		   
		   model.performSearch("Google News search", searchedText, true);
		   myView.cmbSource.selectedIndex = 0;
		   myView.txbSearch.text = searchedText;
	   }
	   
	   public function buttonClicked(evt:Event):void {
			var target:String = evt.currentTarget.id;
			
			switch (target) {
				case "btnPrevious":
					model.pageNum--;
					myView.btnPrevious.enabled = model.pageNum == 1 ? false : true;
					myView.btnNext.enabled = true;  
					model.pageNum = model.pageNum < 1 ? 1 : model.pageNum;
					myView.pgrBar.visible = true;
					model.performSearch(myView.cmbSource.selectedLabel, myView.txbSearch.text, false);
					
					break;
				case "btnNext":
					myView.pgrBar.visible = true;
					model.pageNum++;
					myView.btnPrevious.enabled = model.pageNum == 1 ? false : true;
					model.performSearch(myView.cmbSource.selectedLabel, myView.txbSearch.text, false);
					break;
				
				case "btnSearch":
					myView.pgrBar.visible = true;
					model.performSearch(myView.cmbSource.selectedLabel, myView.txbSearch.text, true);
					break;
				case "btnSettings":
					Alert.show("Settings button");
					break; 
				default:
					trace("ERROR: no case for switch on MainViewController-buttonClicked for target: " + target);
					break;
			}
	   }
	   
	  
	   public function dataHasBeenLoaded(pData:Array):void {
		   myView.pgrBar.visible = false;
	   }
	}
}
	