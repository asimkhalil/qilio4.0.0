package Controllers
{
	import Components.CustomImageControl;
	import Components.DBSocket;
	
	import Interfaces.iController;
	
	import Models.*;
	
	import Views.*;
	
	import events.GenericEvent;
	
	import flash.geom.Rectangle;
	
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.CloseEvent;
	
	import spark.components.BorderContainer;
	
	public class EmoticonSelectViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:EmoticonSelectView;
	   public function countHasBeenLoaded(object:Array):void{};
	   [Bindable] public var model:EmoticonSelectViewModel = new EmoticonSelectViewModel(this);
	   //[Bindable] public var userFeed:UserFeedVO;
	   [Bindable] public var delegate:Object;
	   
	   //public function countHasBeenLoaded(object:Array):void{};
	   
	   public function EmoticonSelectViewController() {
			super();
	   }
	   
	   public function init(event:Event):void {
		   myView = view as EmoticonSelectView;
		   myView.addEventListener(GenericEvent.EMOTICONS_SELECTED,onEmoticonsSelected);
		   /*if (model.mainObject.fan_page_id != "") {
			   myView.cmbFanPage.selectedIndex = getObjectIndexFromArray(model.appStateVars.facebookCOM.facebookPagesArray, "id", model.mainObject.fan_page_id);
			   myView.btnDelete.visible = true;
		   } else {
			   myView.btnDelete.visible = false;
		   }*/
	   }
	 	
	   private function onEmoticonsSelected(event:GenericEvent):void {
		   delegate.emoticonWasSelected(event.data.text);
	   }
		   
	   public function emoticonSelected(evt:Event):void {
		   //var iconName:String = DataGrid(evt.target).selectedItem.data;   
		   //var tempIcon:CustomImageControl = new CustomImageControl();
		   var emoticon:String = DataGrid(evt.target).selectedItem.text;
		   delegate.emoticonWasSelected(emoticon);
		   //delegate.iconWasSelected(tempIcon, "Media/emoticons/" + iconName);
		   
		   /*tempIcon.hasDelete = true;
		   tempIcon.hasResize = false;
		   tempIcon.width = 80;
		   tempIcon.height = 45;
		   tempIcon.boundArea = new Rectangle(	0,
			   0,
			   delegate.myView.brgImageComposition.width,
			   delegate.myView.brgImageComposition.height);
		   ;*/
		   
		   
	   }
	   
	  /* public function buttonClicked(evt:Event):void {
			var target:String = evt.currentTarget.id;
			
			model.mainObject.title = myView.txbTitle.text;
			model.mainObject.link = myView.txbUrl.text;
			model.mainObject.fan_page_id = myView.cmbFanPage.selectedItem.id;
			model.mainObject.addEventListener("TRANSACTION_SUCCESS", feedWasSavedSuccess);
			model.mainObject.addEventListener("TRANSACTION_FAILED", feedSaveFailed);
			
			switch (target) {
				
				case "btnSave":
					if (model.mainObject.id == 0) {
						model.mainObject.saveVO();						 
					} else {
						model.mainObject.updateVO();						 
					}
					break; 
				case "btnDelete":
					deleteEntityRequest();
					 
					break; 
				default:
					trace("ERROR: no case for switch on MainViewController-buttonClicked for target: " + target);
					break;
			}
	   }*/
	   
	   public function dataHasBeenLoaded(pData:Array):void {
		  
		   // myView.dgrStatusIdeas.dataProvider = pData;
		   //myView.pgrBar.visible = false;
	   }
	}
}
	