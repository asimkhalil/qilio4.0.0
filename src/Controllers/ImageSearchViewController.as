package Controllers
{
	import Interfaces.iController;
	
	import Models.*;
	
	import Views.*;
	
	import events.GenericEvent;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.utils.setTimeout;
	
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	
	import spark.components.BorderContainer;
	
	public class ImageSearchViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:ImageSearchView;
 	   
	   [Bindable] public var model:ImageSearchViewModel = new ImageSearchViewModel(this);
	   [Bindable] public var selectedImageURL:String;
	      
	   
	   public function countHasBeenLoaded(object:Array):void{};
	   
	   public function ImageSearchViewController() {
			super();
	   }
	   
	   public function init(event:Event):void {
		   
		   myView = view as ImageSearchView;
 		  myView.addEventListener(GenericEvent.SEARCHED_IMAGE_CLICKED,searchedImageClickedHnd);
	  }
		
	   private function searchedImageClickedHnd(event:GenericEvent):void {
		   selectedImage(event.data.imageUrl);
	   }
	   
	   public function selectItem(selectedObject:Object):void {
		  /* this.selectedContentIdeaVO = new ScheduledPostVO();
		   //this.selectedContentIdeaVO.loadDataFromResult(selectedObject, true);
		   this.selectedContentIdeaVO.content_text = selectedObject.description;
		   this.selectedContentIdeaVO.content_link_url = selectedObject.link;
		   this.selectedContentIdeaVO.content_header = selectedObject.title;
		   
		   //trace(selectedObject);
		   this.dispatchEvent(new Event("CONTENT_IDEA_SELECTED"));
		   this.removeMe(null);  */
	   }
	   
	   public function selectedImage(pSelectedImage:String):void {
		   selectedImageURL = pSelectedImage;
		   //trace("selectedImageURL= " + selectedImageURL);
		   this.dispatchEvent(new Event("IMAGE_SELECTED"));
		   /*this.removeMe(null);*/
	   }
	   
	   public function removePopup():void {
		   this.removeMe(null);
	   }
	   
	   public function selectionChanged(evt:Event):void {
		   var target:String = evt.target.id;
		   
		   switch (target) {
			  case "cmbSource":
				   
				   model.resetSearch();
				   break;
			   
			   default:
				   trace("no case for switch on PostViewController, target = " + target);
				   break;
		   }
	   }
	   
	   public function buttonClicked(evt:Event):void {
			var target:String = evt.currentTarget.id;
			
			switch (target) {
				case "btnSearch":
					myView.pgrBar.visible = true;
					model.performSearch(myView.cmbSource.selectedLabel, myView.txbSearch.text,myView.cmbSource.selectedIndex);
					break;
				case "btnSettings":
					Alert.show("Settings button");
					break; 
				default:
					trace("ERROR: no case for switch on MainViewController-buttonClicked for target: " + target);
					break;
			}
	   }
	  
	   public function dataHasBeenLoaded(pData:Array):void {
		   myView.drgImages.dataProvider = model.searchResults;
		   myView.pgrBar.visible = false;
	   }
	   
	   public function openImageSourceSite():void {
		   if(myView.cmbSource.selectedIndex == 0) {
			   navigateToURL(new URLRequest("http://pixabay.com"),"_blank");
		   } else {
			   navigateToURL(new URLRequest("http://morguefile.com"),"_blank");
		   }
	   }
	}
}
	