package Controllers
{
	import Components.DBSocket;
	import Components.QuickDateFormatter;
	
	import Interfaces.iController;
	
	import Models.*;
	
	import VOs.FacebookPageVO;
	import VOs.ScheduledPostVO;
	
	import Views.*;
	
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import mx.controls.Alert;
	import mx.controls.ToggleButtonBar;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.FlexGlobals;
	
	import spark.components.BorderContainer;
	
	public class ScheduledPostsViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:ScheduledPostsView;
 	   public var tempEditPostView:EditScheduledPostView;
	   
	   [Bindable] public var model:ScheduledPostsViewModel = new ScheduledPostsViewModel(this);
	      
	   public function ScheduledPostsViewController() {
			super();
	   }
	   
	   public function customLabelFunction(item:Object):String{
		   return item.name;
	   }
	   
	   public function init(event:Event):void {
		   myView = view as ScheduledPostsView;
 		   myView.btnSearch.setFocus();
		   
		   callSearch();
		   
		   model.appStateVars.facebookCOM.addEventListener("POSTS_FETCHING_SUCCESS", loadedPosts);
		   model.appStateVars.facebookCOM.addEventListener("POSTS_FETCHING_REACHED_END", reachedEndOfList);
		   model.appStateVars.facebookCOM.addEventListener("POSTS_FETCHING_REACHED_BEGINNING", reachedBegginingOfList);
	   }
		
	   public function handleTabChange(event:Event):void {
		   if((event.target as ToggleButtonBar).selectedIndex != 2) {
			   removeMe(null);
			   FlexGlobals.topLevelApplication.dispatchEvent(new DataEvent("TAB_CHANGED",true,false,(event.target as ToggleButtonBar).selectedIndex.toString()));
		   }
	   }
	   
	   public override function removeMe(evt:Event):void {
		   model.appStateVars.facebookCOM.removeEventListener("POSTS_FETCHING_SUCCESS", loadedPosts);
		   model.appStateVars.facebookCOM.removeEventListener("POSTS_FETCHING_REACHED_END", reachedEndOfList);
		   model.appStateVars.facebookCOM.removeEventListener("POSTS_FETCHING_REACHED_BEGINNING", reachedBegginingOfList);
			super.removeMe(evt);
	   }
	   
	   public function loadedPosts(evt:Event):void {
		    enableDisableView(true);
	   		myView.dgrScheduledPosts.dataProvider = model.appStateVars.facebookCOM.postsArray;
			trace("Assigned posts data to grid");
	   }
	   
	   public function reachedBegginingOfList(evt:Event):void {
		   enableDisableView(true);
		   Alert.show("This is the first page","Information");
		  
	   }
	   
	   public function reachedEndOfList(evt:Event):void {
		   enableDisableView(true);
		   Alert.show("There are currently no scheduled posts","Information");
	   }
	   
	   public function editItem(dataObject:Object):void {
		   var selectedObject:ScheduledPostVO = new ScheduledPostVO();
		   var pageAccessToken:String 	= FacebookPageVO(myView.cmbFanPage.selectedItem).access_token;
		   var pageId:String			= myView.cmbFanPage.selectedItem.data;
		   
		   selectedObject.loadDataFromResult(dataObject);
		   selectedObject.pageAccessToken = pageAccessToken;
		   selectedObject.pageId = pageId;
		   tempEditPostView = showPopUpView("EditScheduledPostView", selectedObject, "", true) as EditScheduledPostView;
		   tempEditPostView.controller.delegate = this;
	   }
	   
	   public function editWindowClosed():void {
		   var temp:Array = new Array();
		   var pageId:String;
		   var fromDate:int;
		   var toDate:int;
		   
		   enableDisableView(false);
		   
		   pageId 			= myView.cmbFanPage.selectedItem.data;
		   fromDate = QuickDateFormatter.getUnixStampTimeFromDate(myView.dtpFrom.selectedDate);
		   toDate	 = QuickDateFormatter.getUnixStampTimeFromDate(myView.dtpTo.selectedDate);
		   
		   if (!validateDate()) {
			   return;
		   }
		   
		   model.appStateVars.facebookCOM.getAllPagePosts(pageId, fromDate, toDate, 0); 
	   }
	   
	   public function deleteItem(dataObject:Object):void {
		   var selectedObject:ScheduledPostVO = new ScheduledPostVO();
		   selectedObject.loadDataFromResult(dataObject);
		   
		   showPopUpView("EditScheduledPostView", selectedObject, "", true);
	   }
	   
	   public function viewPost(dataObject:Object):void {
		  // trace("going to open browser for link view"); 
		   navigateToURL(new URLRequest(dataObject.permalink),"_blank");
	   }
	   
	   public function changeToPage(newPage:int):void {
		   var dateFrom:String 	= QuickDateFormatter.getDateTimeInMySQLFormatFromDate(myView.dtpFrom.selectedDate);
		   var dateTo:String 	= QuickDateFormatter.getDateTimeInMySQLFormatFromDate(myView.dtpTo.selectedDate);
		   dateTo = dateTo.slice(0, dateTo.indexOf("00:00:00")) + "23:59:59";
		   
		   model.pageNum = newPage; 
		   model.appStateVars.appDataBase.getScheduledPost(model.PAGE_SIZE, model.pageNum, dateFrom, dateTo, false);
	   }
	   
	   public function finalSearchDate():Date {
		   var date:Date = new Date();
		   date.setDate(date.date + 30);
		   return date;
	   }
	   
	   public function selectionChanged(evt:Event):void {
		  myView.dgrScheduledPosts.dataProvider = null;   
		  model.appStateVars.facebookCOM.postListPageNumber = 0;
		  //Alert.show(evt.currentTarget.inputTxt.text);
	   }
	   
	   public function enableDisableView(pAction:Boolean):void {
			myView.btnHome.enabled = pAction;
			myView.btnNext.enabled = pAction;
			myView.btnPrevious.enabled = pAction;
			myView.btnSearch.enabled = pAction;
			myView.dgrScheduledPosts.enabled = pAction;
			myView.dtpFrom.enabled = pAction;
			myView.dtpTo.enabled = pAction;
			myView.cmbFanPage.enabled = pAction;
			myView.pgrBar.visible = !pAction;
	   }
	   
	   public function buttonClicked(evt:Event):void {
			var target:String 		= evt.currentTarget.id;
			var pageId:String;
			var fromDate:int;
			var toDate:int;
			
			if ( myView.dtpFrom.selectedDate > myView.dtpTo.selectedDate) {
				Alert.show("Please select correct date range!", "Error");
				return;
			}
			
			switch (target) {
				case "btnSettings":
					Alert.show("Settings button");
					break;
				case "btnNext":
					enableDisableView(false);
					fromDate = QuickDateFormatter.getUnixStampTimeFromDate(myView.dtpFrom.selectedDate);
					toDate	 = QuickDateFormatter.getUnixStampTimeFromDate(myView.dtpTo.selectedDate);
					
					if (!validateDate()) {
						return;
					}
					
					if (myView.cmbFanPage.selectedIndex == -1) {
						Alert.show("There is no fan page selected","Information");
						return;
					}
					
					pageId 			= myView.cmbFanPage.selectedItem.data;
					model.appStateVars.facebookCOM.getAllPagePosts(pageId, fromDate, toDate, 1);
					break;
				case "btnPrevious":
					enableDisableView(false);
					fromDate = QuickDateFormatter.getUnixStampTimeFromDate(myView.dtpFrom.selectedDate);
					toDate	 = QuickDateFormatter.getUnixStampTimeFromDate(myView.dtpTo.selectedDate);
					
					if (!validateDate()) {
						return;
					}
					
					if (myView.cmbFanPage.selectedIndex == -1) {
						Alert.show("There is no fan page selected","Information");
						return;
					}
					
					pageId 			= myView.cmbFanPage.selectedItem.data;
					model.appStateVars.facebookCOM.getAllPagePosts(pageId, fromDate, toDate, -1);
					break;
				case "btnSearch":
					callSearch();
					break;
				default:
					trace("ERROR: no case for switch on MainViewController-buttonClicked for target: " + target);
					break;
			}		   
	   }
	   
	   private function validateDate():Boolean {
			var result:Boolean = true;
			var fromDate:Date = myView.dtpFrom.selectedDate;
			var toDate:Date	  = myView.dtpTo.selectedDate;
			
			if ( fromDate >= toDate) {
				Alert.show("The ending date must be larger than the beggining date","Information");
				result = false;
			}
			
			return result;
	   }
	   
	   public function callSearch():void {
		if (myView.cmbFanPage.selectedIndex == -1) {
			//Alert.show("Please select a valid FanPage first");
			return;
		}
		   var pageId:String;
		   var fromDate:int;
		   var toDate:int;
		   var temp:Array = new Array();
		   
		   enableDisableView(false);
		   var object:Object = myView.cmbFanPage;
		   
		   pageId 	= myView.cmbFanPage.selectedIndex > -1 ? myView.cmbFanPage.selectedItem.data : "";
		   fromDate = QuickDateFormatter.getUnixStampTimeFromDate(myView.dtpFrom.selectedDate);
		   toDate	= QuickDateFormatter.getUnixStampTimeFromDate(myView.dtpTo.selectedDate);
		   
		   if (!validateDate()) {
			   return;
		   }
		   
		   model.appStateVars.facebookCOM.getAllPagePosts(pageId, fromDate, toDate, 0);
		      
	   }
	   
	   public function comboLostFocus(evt:Event):void {
		   if (myView.cmbFanPage.selectedIndex == -1){
				Alert.show("The specified Fan page does not exists for this account","Information");
				myView.cmbFanPage.setFocus();
		   }
	   }
	   
	   public function dataGridDataFormatter(data:Object, dataGridColumn:AdvancedDataGridColumn):String{
		  var result:String = "";
			  
		   switch (dataGridColumn.dataField) {
			   case "created_time":
				   var refDate:Date;
				   var refDateString:String; 
				   var formatedDate:String;
				   
				   if (!data.created_time) break;
				   refDate = QuickDateFormatter.getDateFromUnixTimeStamp(data.created_time);
				   refDateString = QuickDateFormatter.getDateTimeInMySQLFormatFromDate(refDate);
				   
				   formatedDate = QuickDateFormatter.format(refDateString, 'MM/DD/YYY') + ' ' + QuickDateFormatter.formatTime(refDateString);
				   result =  formatedDate;
				   break
			   case "post_type":
				   if (!data.type) {
					   result = "not recognized";
					   break;  
				   } 
				   
				   result = model.appStateVars.facebookCOM.FacebookPostTypesOptions [getObjectIndexFromArray(model.appStateVars.facebookCOM.FacebookPostTypesOptions, "data", data.type.toString())].label;
				   break
			   case "message":
				   if (!data.message && data.description) {
					   result = "none";
					   break;  
				   } 
				   result = data.message || data.description;
				   break;
			 
			   case "scheduled_publish_time":
				   if (!data.scheduled_publish_time) {
					   result = "";
					   break;  
				   } 
				   refDate = QuickDateFormatter.getDateFromUnixTimeStamp(data.scheduled_publish_time);
				   refDateString = QuickDateFormatter.getDateTimeInMySQLFormatFromDate(refDate);
				   formatedDate = QuickDateFormatter.format(refDateString, 'MM/DD/YYY') + ' ' + QuickDateFormatter.formatTime(refDateString);
				   result = formatedDate;
				   break;
			   case "is_published":
				   if (data.is_published == true) {
					   result = "published";
				   } else if(data.is_published == false && !data.scheduled_publish_time) {
					   result = "Unpublished";
				   } else if(data.is_published == false && data.scheduled_publish_time) {
					   result = "Scheduled";
				   }
					   
				   break;
			 
			   default:
				   trace("ERROR: no case for switch in schedueldpostscontroller target= " + dataGridColumn.dataField);
				   break;
		   }
		    
		   return result;
	   }
	   
	   public function countHasBeenLoaded(pData:Array):void {
		  // trace("count has been laoded");
		   if (pData.length == 0)
			   return;
	       model.totalRecords = pData[0]['count'];
		   dispatchEvent(new Event("REFRESH_PAGING"));
	   }
	   
	   public function dataHasBeenLoaded(pData:Array):void {
		    if (pData.length == 0)
				return;
			//trace("data has been loaded");
		  	myView.dgrScheduledPosts.dataProvider = pData;
			//trace("scheduledPOst data came in"); 
	   }
	}
}
	