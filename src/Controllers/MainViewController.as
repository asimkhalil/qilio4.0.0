package Controllers
{
	import Components.DBSocket;
	import Components.FacebookCenter;
	import Components.GoogleSearchCenter;
	import Components.Home;
	import Components.QuickDateFormatter;
	
	import Interfaces.iController;
	
	import Models.*;
	
	import VOs.KeywordMaintainanceVO;
	import VOs.ScheduledPostVO;
	import VOs.StatusIdeaVO;
	import VOs.UserFeedVO;
	import VOs.UserInfoVO;
	
	import Views.*;
	
	import com.facebook.graph.FacebookDesktop;
	import com.pageone.settings.Settings;
	import com.pageone.ui.ValidationInfo;
	import com.ploscariu.facebook.ui.PostScheduleContainer;
	import com.ploscariu.facebook.ui.PostStatisticsContainer;
	
	import events.AppSettingsSaveEvent;
	
	import flash.desktop.NativeApplication;
	import flash.display.DisplayObject;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.xml.XMLDocument;
	import flash.xml.XMLNode;
	import flash.xml.XMLNodeType;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TabNavigator;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.LinkButton;
	import mx.core.FlexGlobals;
	import mx.events.CloseEvent;
	import mx.events.IndexChangedEvent;
	import mx.managers.PopUpManager;
	
	import spark.components.BorderContainer;
	import spark.components.NavigatorContent;
	
	public class MainViewController extends BaseController implements iController {
	   /* Model for this View */
	   public var myView:MainView;
	   
	   [Bindable] public var model:MainViewModel = new MainViewModel(this);
	   [Bindable] public var pop:BorderContainer;
	   [Bindable] public var userInfo:UserInfoVO = new UserInfoVO();
	   [Bindable] public var scheduledPostVO:ScheduledPostVO = new ScheduledPostVO();
	   
	   public function countHasBeenLoaded(object:Array):void{};
	   //public var facebookCOM:FacebookCenter;
	  
	   public function MainViewController() {
			super();
	   }
	   
	   private function appSettingsAvailable():Boolean {
		   /*var file:File = File.applicationStorageDirectory.resolvePath("fbappsettings.xml");
		   return file.exists;*/
		   return true;
	   }
	   
	   public function writeAppSettings():void {
		   var file:File = File.applicationStorageDirectory.resolvePath("fbappsettings.xml");
		   if(!file.exists) {
			   var fileStream:FileStream = new FileStream();
			   fileStream.open(file,FileMode.WRITE);
			   fileStream.writeUTFBytes("<root><id>"+ApplicationModel.appId+"</id><secret>"+ApplicationModel.appSecret+"</secret><createddate>"+File.applicationDirectory.modificationDate+"</createddate><headerHideMode>"+ApplicationModel.headerHideMode.toString()+"</headerHideMode></root>");
			   fileStream.close();
		   } else {
			   var fileStream:FileStream = new FileStream();
			   fileStream.open(file,FileMode.UPDATE);
			   fileStream.writeUTFBytes("<root><id>"+ApplicationModel.appId+"</id><secret>"+ApplicationModel.appSecret+"</secret><createddate>"+File.applicationDirectory.modificationDate+"</createddate><headerHideMode>"+ApplicationModel.headerHideMode.toString()+"</headerHideMode></root>");
			   fileStream.close();
		   }
	   }
	   
	   private function getAppSettings():void {
		   /*var file:File = File.applicationStorageDirectory.resolvePath("fbappsettings.xml");
		   var fs:FileStream=new FileStream();
		   fs.open(file, FileMode.READ);
		   var s:String=fs.readUTFBytes(fs.bytesAvailable);
		   fs.close();
		   var xml:XMLDocument = new XMLDocument(s);
		   
		   for each(var child:XMLNode in xml.firstChild.childNodes) {
			   if(child.nodeType == XMLNodeType.ELEMENT_NODE) {
				   if(child.childNodes[0].parentNode.nodeName == "id") {
					   ApplicationModel.appId = child.childNodes[0].nodeValue;
				   }  else if(child.childNodes[0].parentNode.nodeName == "createddate") {
					   ApplicationModel.createdDate = child.childNodes[0].nodeValue;
				   } else if(child.childNodes[0].parentNode.nodeName == "headerHideMode") {
					   ApplicationModel.headerHideMode = true;//String(child.childNodes[0].nodeValue) == 'true'?true:false;
				   } else {
					   ApplicationModel.appSecret = child.childNodes[0].nodeValue;
				   }
			   }
		   }*/ 
		   
		   ApplicationModel.appId = "540125842745826";
		   ApplicationModel.headerHideMode = true;
		   ApplicationModel.appSecret = "5f8bb47f61a566fdfd4305ec3716b85b";
	   }
	   
	   public function init(event:Event):void {
		   myView = view as MainView;
		   FlexGlobals.topLevelApplication.addEventListener("APP_VALIDATED",validateAppSettings);
		   FlexGlobals.topLevelApplication.addEventListener("STATISTICS_PAGES_ARRAY_LOADED",onPostArrayLoaded);
		   FlexGlobals.topLevelApplication.addEventListener("HOME_PAGE_OPEN_VIEW_CLICKED",onHomPageOpenViewClicked);
		   if(!isLocal) 
		   		myView.callLater(validateApp);
		   else 
			   validateViaMacs();//validateAppSettings();
	   } 
	   
	   private function validateViaMacs():void 
	   {
		   
		   var macAddresses:ArrayCollection = new ArrayCollection();
		   
		   macAddresses.addItem("00-23-5A-17-55-7A");
		   macAddresses.addItem("00-FF-29-E7-BB-30");
		   macAddresses.addItem("00-21-00-9E-4A-F2");
		   macAddresses.addItem("20-00-54-55-4E-01");
		   macAddresses.addItem("00-26-2D-F7-85-E1");
		   macAddresses.addItem("00-27-13-C9-10-36");
		   macAddresses.addItem("00-24-D7-0D-23-F4");
		   //macAddresses.addItem(ApplicationModel.macAddress);
		   
		   //Alert.show("MAC: "+ApplicationModel.macAddress);
		   /*if(macAddresses.contains(ApplicationModel.macAddress)) 
		   {*/
			   validateAppSettings();
		   /*}
		   else 
		   {
			   Alert.show("Your mac address could not be verified");
		   }*/
	   }
	   
	   private function onHomPageOpenViewClicked(event:DataEvent):void 
	   {
		   showTabButtons();
		   switch(event.data)
		   {
			   case "Content":
			   {
				   mainTabNavigator.selectedIndex = 1;
				   postView.cmbPostType.selectedIndex = 0;
				   postView.controller.selectionChanged(null,postView.cmbPostType);
				   break;
			   }
				   
			   case "Images":
			   {
				   mainTabNavigator.selectedIndex = 1;
				   postView.cmbPostType.selectedIndex = 1;
				   postView.controller.selectionChanged(null,postView.cmbPostType);
				   break;
			   }	
				   
			   case "Videos":
			   {
				   mainTabNavigator.selectedIndex = 1;
				   postView.cmbPostType.selectedIndex = 2;
				   postView.controller.selectionChanged(null,postView.cmbPostType);
				   break;
			   }	
				   
			   case "Schedule":
			   {
				   mainTabNavigator.selectedIndex = 3;
				   break;
			   }
				   
			   case "Analytics":
			   {
				   mainTabNavigator.selectedIndex = 4;
				   break;
			   }
				   
			   case "Settings":
			   {
				   if(ApplicationModel.isLite) {
				   		mainTabNavigator.selectedIndex = 4;
				   } else {
					   mainTabNavigator.selectedIndex = 5;
				   }
				   break;
			   }
		   }   
	   }
	   
	   private function onPostArrayLoaded(event:Event):void 
	   {
		   if(!ApplicationModel.isLite) {
			   analytics.pagesComboBox.dataProvider = new ArrayCollection(AppStateVars.getInstance().facebookCOM.myPages);
			   analytics.pagesComboBox.selectedIndex = 0;
			   analytics.pagesComboBox.validateNow();
		   }
		   
		   scheduledPosts.pagesComboBox.dataProvider = new ArrayCollection(AppStateVars.getInstance().facebookCOM.myPages);
		   scheduledPosts.pagesComboBox.selectedIndex = 0;
		   scheduledPosts.pagesComboBox.validateNow();
	   }
	   
	   private var valinfo:ValidationInfo;
	   
	   private var isLocal:Boolean = false;
	   
	   private function validateApp():void {
		   Settings.app.application=myView;
		   Settings.app.read();
		   valinfo=new ValidationInfo();
		   valinfo.show(myView);
	   }
	   
	   public function validateAppSettings(event:Event=null):void {
		   
		   if(!appSettingsAvailable()) {
			   var appSettingsPopup:AppSettingsPopup = new AppSettingsPopup();
			   PopUpManager.addPopUp(appSettingsPopup,FlexGlobals.topLevelApplication as DisplayObject,true);
			   PopUpManager.centerPopUp(appSettingsPopup);
			   appSettingsPopup.addEventListener(AppSettingsSaveEvent.APP_SETTINGS_SAVE,function(event:AppSettingsSaveEvent):void {
				   ApplicationModel.headerHideMode = true;
				   ApplicationModel.appId = event.appid;
				   ApplicationModel.appSecret = event.appsecret;
				   writeAppSettings();
				   PopUpManager.removePopUp(appSettingsPopup);
				   connectToFBAndContinue();
			   });
		   }  else {
			   getAppSettings(); 
			   /* if(ApplicationModel.createdDate == File.applicationDirectory.modificationDate.toString()) {
			   connectToFBAndContinue();
			   } else {*/
			   /*var newInstallationMessage:String = "Do you want to retain settings from previous installation?\nSelect yes to retain the settings\nSelect No, to enter new app settings";
			   Alert.show(newInstallationMessage,"New Installation",3,FlexGlobals.topLevelApplication.ImportEngine,importsettingsConfirmationCloseHnd);
			   
			   function importsettingsConfirmationCloseHnd(event:CloseEvent):void {
			   if(event.detail == Alert.YES) {*/
			   ApplicationModel.headerHideMode = true;
			   model.appStateVars.facebookCOM = new FacebookCenter(ApplicationModel.appId,ApplicationModel.appSecret);
			   connectToFBAndContinue();
			   /*} else {
			   createNewSettingsAndContinue();
			   }
			   }*/
			   /*}*/
		   } 
		   
	   }
	   
	   private function createNewSettingsAndContinue():void { 
		   var appSettingsPopup:AppSettingsPopup = new AppSettingsPopup();
		   PopUpManager.addPopUp(appSettingsPopup,FlexGlobals.topLevelApplication as DisplayObject,true);
		   PopUpManager.centerPopUp(appSettingsPopup);
		   appSettingsPopup.addEventListener(AppSettingsSaveEvent.APP_SETTINGS_SAVE,function(event:AppSettingsSaveEvent):void {
			   ApplicationModel.appId = event.appid;
			   ApplicationModel.appSecret = event.appsecret;
			   writeAppSettings();
			   PopUpManager.removePopUp(appSettingsPopup);
			   connectToFBAndContinue();
		   });
	   }
	   
	   private function connectToFBAndContinue():void {
		   FlexGlobals.topLevelApplication.addEventListener("TAB_CHANGED",onTabChanged);
		   model.appStateVars.facebookCOM = new FacebookCenter(ApplicationModel.appId,ApplicationModel.appSecret);
		   model.appStateVars.facebookCOM.addEventListener("PAGES_ARRAY_LOADED", pagesDataWasLoaded);
		   model.appStateVars.facebookCOM.addEventListener("LOGGED_IN"			, facebookLoggedIn);
		   model.appStateVars.facebookCOM.addEventListener("LOG_OUT_SUCCESS"			, onLogOutSuccess);
		   
		   model.appStateVars.facebookCOM.login();
		   
		   if (doesSystemHaveValidKey() == false) {
			   // Alert.show("The license for this system has expired, please contact MaturoSolutions.com for renewal");
			   var alert:Alert = Alert.show("The license for this system has expired, please contact MaturoSolutions.com for renewal", "Key expired", Alert.OK, myView, applicationExit);
			   //return;
		   }
		   
		   /*myView.btnNewPost.setFocus();*/
		   setupCurrentUser();
		   checkForExistenceOfDB();	
		   //getChannels();
		   //test();
		   
		   userInfo.id = 1; 
		   userInfo.addEventListener("TRANSACTION_SUCCESS", userLoaded);
		   userInfo.addEventListener("TRANSACTION_FAILED", userLoadFailed);
		   userInfo.getEntity();
		   
		   scheduledPostVO.getResultCount();
		   
		   var appStateVars:AppStateVars = AppStateVars.getInstance();
		   appStateVars.appDataBase = new DBSocket(this);
		   appStateVars.appDataBase.openDBConnection();
		   appStateVars.appDataBase.getKeywords();
	   }
	   
	   
	   private function onLogOutSuccess(event:Event):void{
		   NativeApplication.nativeApplication.exit();
	   }
	   
	   private function onTabChanged(event:DataEvent):void {
		   AppStateVars.getInstance().viewSelected = int(event.data);
		   switch(event.data)
		   {
			   case "1":
				   pop = showPopUpView("KeyworkMaintainanceView", new Object(),"",true);
				   pop.width = myView.width;
				   pop.height = myView.height + 50;
				   pop.y = 0;
				   pop.top = 0;
				   PopUpManager.centerPopUp(pop);
				   break;
			   case "0":
				   pop = showPopUpView("PostView", new Object(),"",true);
				   pop.width = myView.width;
				   pop.height = myView.height + 50;
				   pop.y = 0;
				   pop.top = 0;
				   PopUpManager.centerPopUp(pop);
				   break;
			   
			   case "3":
				   pop = showPopUpView("PostStatisticsView", null, "", true);
				   pop.width = myView.width;
				   pop.height = myView.height + 50;
				   pop.top = 0;
				   pop.y = 0;
				   PopUpManager.centerPopUp(pop);
				   break;
			   case "btnLogoutOfFacebook":
				   pop = showPopUpView("VideoSearchView", null, "", true);
				   pop.width = myView.width;
				   pop.height = myView.height + 50;
				   pop.top = 0;
				   pop.y = 0;
				   PopUpManager.centerPopUp(pop);
				   break;
			   case "btnInitFacebook":
				   
				   break;
			   case "btnLoginToFacebook":
				   
				   break;
			   case "btnNewPost":
				   pop = showPopUpView("PostView", new Object(),"",true);
				   pop.width = myView.width;
				   pop.height = myView.height + 50;
				   pop.y = 0;
				   pop.top = 0;
				   PopUpManager.centerPopUp(pop);
				   break;
			   case "2":
				   //model.appStateVars.appDataBase.openDBConnection();
				   pop = showPopUpView("ScheduledPostsView", new Object(),"",true);
				   pop.width = myView.width;
				   pop.height = myView.height + 50;
				   pop.y = 0;
				   pop.top = 0;
				   PopUpManager.centerPopUp(pop);
				   //Alert.show("Scheduled posts button");
				   break;
			   case "4":
				   pop = showPopUpView("SettingsView", new Object(),"",true);
				   pop.width = myView.width;
				   pop.height = myView.height + 50;
				   pop.y = 0;
				   pop.top = 0;
				   PopUpManager.centerPopUp(pop);
				   break; 
				   
			}
	   }
	   
	   private function userLoaded(evt:Event):void {
		   model.appStateVars.defaultFanPageId = userInfo.default_fan_page;
		   model.appStateVars.showFeedsCount = userInfo.show_feed_count=='0'?false:true;
	   }
	   
	   private function userLoadFailed(evt:Event):void {
		   Alert.show("There was an error loading user data from database");
	   }
	   
	   /*private function test():void {
		   facebookCOM:FacebookCenter = FacebookCenter();
		   
		   getAllPagePosts
	   }*/
	   
	   private function getChannels():void 
	   {
		   /*var readFileStream:FileStream = new FileStream();
		   var channelsFile:File = File.applicationDirectory;
		   channelsFile = channelsFile.resolvePath("databaseTemplate/channels.txt");
		   readFileStream.addEventListener(Event.COMPLETE,loadComplete);
		   readFileStream.openAsync(channelsFile, FileMode.READ);
		   
		   function loadComplete(event:Event):void 
		   {
			   var result:String = readFileStream.readUTFBytes(readFileStream.bytesAvailable);
			   readFileStream.close();*/
			   /*AppModelLocator.channels = new XMLDocument(result);*/
			   
			   /*AppModelLocator.channelsCol.removeAll();*/
			   
			   /*var xml:XML = XML(result);*/
			   
			   /*for each(var xmlNode:* in xml.channel) 
			   {
			   var channelName:String = xmlNode.toString()+'\n';
			   if(channelName.indexOf(",") == 0) 
			   channelName = channelName.substr(1,channelName.length-1);
			   AppModelLocator.channelsCol.addItem(channelName);
			   }*/
			  /* AppModelLocator.channelsCol = result;
		   }*/
	   }
	   
	   private function facebookLoggedIn(evt:Event):void {
		   trace("succesful facebook login");
		  /* myView.txtFacebookStatus.text = "Logged in";*/
		   model.appStateVars.facebookCOM.removeEventListener("LOGGED_IN", facebookLoggedIn);

		   model.appStateVars.facebookCOM.getUserPagesList();
		   model.appStateVars.facebookCOM.loadPages();
	   }
	   
	   
	   public var scheduledPosts:PostScheduleContainer;
	   
	   public var analytics:PostStatisticsContainer;
	   
	   public var settings:SettingsView;
	   
	   public var mainTabNavigator:TabNavigator;
	   
	   private function hideTabButtons():void 
	   {
		   for(var i:int=0;i<mainTabNavigator.numChildren;i++) 
		   {
		   		var tabButton:Button = (mainTabNavigator.getTabAt(i) as mx.controls.Button); 
				tabButton.visible = tabButton.includeInLayout = false;
		   }
	   }
	   
	   private function showTabButtons():void 
	   {
		   for(var i:int=0;i<mainTabNavigator.numChildren;i++) 
		   {
			   var tabButton:Button = (mainTabNavigator.getTabAt(i) as mx.controls.Button); 
			   tabButton.visible = tabButton.includeInLayout = true;
		   }
	   }
			   
	   private function pagesDataWasLoaded(evt:Event):void {
		   model.appStateVars.facebookCOM.removeEventListener("PAGES_ARRAY_LOADED", pagesDataWasLoaded);

		   /*myView.txtFanPagesCount.text =  model.appStateVars.facebookCOM.facebookPagesObjects.length.toString();*/  
		   
		   /*pop = showPopUpView("PostView", new Object(),"",true);
		   pop.width = myView.width;
		   pop.height = myView.height + 50;
		   pop.y = 0;
		   pop.top = 0;
		   PopUpManager.centerPopUp(pop);*/
		   
		   mainTabNavigator = new TabNavigator();
		   myView.addElement(mainTabNavigator);
		   mainTabNavigator.addEventListener(IndexChangedEvent.CHANGE,tabIndexChanged);
		   mainTabNavigator.percentHeight = 100;
		   mainTabNavigator.percentWidth = 100;
		   
		   homeView = new Home();
		   var homeViewNavigator:NavigatorContent = new NavigatorContent();
		   mainTabNavigator.addElement(homeViewNavigator);
		   homeViewNavigator.percentHeight = 100;
		   homeViewNavigator.percentWidth = 100;
		   homeViewNavigator.addElement(homeView);
		   homeView.percentHeight = 100;
		   homeView.percentWidth = 100;
		   homeViewNavigator.label = "1. Home"
		   
		   postView = new PostView();
		   var postViewNavigator:NavigatorContent = new NavigatorContent();
		   mainTabNavigator.addElement(postViewNavigator);
		   postViewNavigator.percentHeight = 100;
		   postViewNavigator.percentWidth = 100;
		   postViewNavigator.addElement(postView);
		   postView.percentHeight = 100;
		   postView.percentWidth = 100;
		   postViewNavigator.label = "2. Main"
		   
		   var keywords:KeyworkMaintainanceView = new KeyworkMaintainanceView();
		   var keywordsViewNavigator:NavigatorContent = new NavigatorContent();
		   mainTabNavigator.addElement(keywordsViewNavigator);
		   keywordsViewNavigator.percentHeight = 100;
		   keywordsViewNavigator.percentWidth = 100;
		   keywordsViewNavigator.addElement(keywords);
		   keywords.percentHeight = 100;
		   keywords.percentWidth = 100;
		   keywordsViewNavigator.label = "3. Keywords & Hashtags";
		   
		   scheduledPosts = new PostScheduleContainer();
		   var schedulesNavigator:NavigatorContent = new NavigatorContent();
		   mainTabNavigator.addElement(schedulesNavigator);
		   schedulesNavigator.percentHeight = 100;
		   schedulesNavigator.percentWidth = 100;
		   schedulesNavigator.addElement(scheduledPosts);
		   scheduledPosts.percentHeight = 100;
		   scheduledPosts.percentWidth = 100;
		   schedulesNavigator.label = "4. Scheduled Posts";
		   
		   if(!ApplicationModel.isLite) {
			   analytics = new PostStatisticsContainer();
			   var analyticsNavigator:NavigatorContent = new NavigatorContent();
			   mainTabNavigator.addElement(analyticsNavigator);
			   analyticsNavigator.percentHeight = 100;
			   analyticsNavigator.percentWidth = 100;
			   analyticsNavigator.addElement(analytics);
			   analytics.percentHeight = 100;
			   analytics.percentWidth = 100;
			   analyticsNavigator.label = "5. Post Analytics";
		   }
		   
		   settings = new SettingsView();
		   var settingsNavigator:NavigatorContent = new NavigatorContent();
		   mainTabNavigator.addElement(settingsNavigator);
		   settingsNavigator.percentHeight = 100;
		   settingsNavigator.percentWidth = 100;
		   settingsNavigator.addElement(settings);
		   settings.percentHeight = 100;
		   settings.percentWidth = 100;
		   settingsNavigator.label = ApplicationModel.isLite?"5. Settings":"6. Settings";
		   
		   mainTabNavigator.callLater(hideTabButtons);
		   
		  /* var help:HelpView = new HelpView();
		   var helpNavigator:NavigatorContent = new NavigatorContent();
		   mainTabNavigator.addElement(helpNavigator);
		   helpNavigator.percentHeight = 100;
		   helpNavigator.percentWidth = 100;
		   helpNavigator.addElement(help);
		   help.setStyle("borderVisible",false);
		   help.percentHeight = 100;
		   help.percentWidth = 100;
		   helpNavigator.label = "6. Help";*/
	   }
	   
	   public function tabIndexChanged(evt:IndexChangedEvent):void {
		   switch(evt.newIndex)
		   {
			   case 0:
			   {
				   hideTabButtons();
				   break;
			   }
			   case 1:
			   {
				   postView.controller.reloadData();
				   break;
			   }
			   case 3:
			   {
				   break;
			   }
			   case 4:
			   {
				   break;
			   }
			   default:
			   {
				   break;
			   }
		   }
	   }
	   
	   public function receivedDBData(pData:Array, pSource:String):void {
			trace("received data from DBSocket");
	   }	  
	   
	   public function checkForExistenceOfDB():void {
		   var dbFile:File = File.applicationDirectory.resolvePath("databaseTemplate/fanpagedb.sqlite");
		   var storageDbFilePath:File = File.applicationStorageDirectory.resolvePath("fanpagedb.sqlite");
		   //trace("source = " + dbFile.nativePath);
		   //trace("storageDbFilePath = " + storageDbFilePath.nativePath);
		   if ( storageDbFilePath.exists == false) {
			   //trace("Local data base does not exists, creating it from templete.");
			   dbFile.copyTo(storageDbFilePath, true);	
		   } else {
			   //trace("The database has already been created in the local storage, no need to make it again");
		   }   
	   }
	   
	   public function saveSuccess(evt:Event):void {
		   Alert.show("The item was saved","Information");
	   }
	   
	   public function saveFailed(evt:Event):void {
			Alert.show("Saving item failed");   
	   }
	   
	   public function viewResized(evt:Event):void {
		   if (pop) {
			   pop.width = myView.width;
			   pop.height = myView.height;
		   }
	   }
	   
	   public function buttonClicked(evt:Event):void {
			var target:String = evt.currentTarget.id;
			
			switch (target) {
				case "btnPostPlanner":
					pop = showPopUpView("PostPlannerView", null, "", true);
					pop.width = myView.width;
					pop.height = myView.height + 50;
					pop.top = 0;
					pop.y = 0;
					PopUpManager.centerPopUp(pop);
					break;
				case "btnPostStatistics":
					pop = showPopUpView("PostStatisticsView", null, "", true);
					pop.width = myView.width;
					pop.height = myView.height + 50;
					pop.top = 0;
					pop.y = 0;
					PopUpManager.centerPopUp(pop);
					break;
				case "btnLogoutOfFacebook":
					pop = showPopUpView("VideoSearchView", null, "", true);
					pop.width = myView.width;
					pop.height = myView.height + 50;
					pop.top = 0;
					pop.y = 0;
					PopUpManager.centerPopUp(pop);
					break;
				case "btnInitFacebook":
					
					break;
				case "btnLoginToFacebook":
					
					break;
				case "btnNewPost":
					pop = showPopUpView("PostView", new Object(),"",true);
					pop.width = myView.width;
					pop.height = myView.height + 50;
					pop.y = 0;
					pop.top = 0;
					PopUpManager.centerPopUp(pop);
					break;
				case "btnScheduledPost":
					//model.appStateVars.appDataBase.openDBConnection();
					pop = showPopUpView("ScheduledPostsView", new Object(),"",true);
					pop.width = myView.width;
					pop.height = myView.height + 50;
					pop.y = 0;
					pop.top = 0;
					PopUpManager.centerPopUp(pop);
					//Alert.show("Scheduled posts button");
					break;
				case "btnSettings":
					pop = showPopUpView("SettingsView", new Object(),"",true);
					pop.width = myView.width;
					pop.height = myView.height + 50;
					pop.y = 0;
					pop.top = 0;
					PopUpManager.centerPopUp(pop);

					break; 
				
				case "btnKeywordMain":
					pop = showPopUpView("KeyworkMaintainanceView", new Object(),"",true);
					pop.width = myView.width;
					pop.height = myView.height + 50;
					pop.y = 0;
					pop.top = 0;
					PopUpManager.centerPopUp(pop);
					break; 
				default:
					trace("ERROR: no case for switch on MainViewController-buttonClicked for target: " + target);
					break;
			}		   
	   }
	  
	   public function applicationExit(event:CloseEvent):void {
		 /*  var exitingEvent:Event = new Event(Event.EXITING, false, true);
		   NativeApplication.nativeApplication.dispatchEvent(exitingEvent);
		   if (!exitingEvent.isDefaultPrevented()) {
			   NativeApplication.nativeApplication.exit();
		   }*/
	   }
	   
	   private function doesSystemHaveValidKey():Boolean {
		   var licensedUntil:Date = new Date("Mon Aug 5 17:08:57 GMT+0200 2013 "); 
		  // var serverDate:Date = new Date(model.appStateVars.systemTimeStamp);
		   //trace("system date is =" + serverDate.toString() + " and license date is " + licensedUntil.toString());
		   
		  /* if (serverDate > licensedUntil) {
			  return false;
		   }*/
		   
		   return true;
	   }
	   
	   public function setupCurrentUser():void {
		  // loginPopUp = showPopUpView("LoginView", model.appStateVars.userVO, "", true);
		   
		   //loginPopUp['controller']['model'].addEventListener("USER_DATA_LOADED", userLoadSuccess);
		 
	  }
	  	  
	   public function dataHasBeenLoaded(pData:Array):void {
		   for each(var obj:Object in pData) {
			   var keywordMain:KeywordMaintainanceVO = new KeywordMaintainanceVO();
			   keywordMain.fan_page_id = obj.fan_page_id;
			   keywordMain.id = obj.id;
			   keywordMain.keywords = obj.keywords;
			   keywordMain.hash_tags = obj.hash_tags;
			   AppStateVars.getInstance().keywordsCollection.addItem(keywordMain);
		   }
		   AppStateVars.getInstance().keywordsCollection.refresh();
	   }
	}
}
	