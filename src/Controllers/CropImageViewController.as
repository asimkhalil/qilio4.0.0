package Controllers
{
	
	import Interfaces.iController;
	
	import Models.*;
	
	import Views.*;
	
	import com.imagemod.TransformTool2;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	
	import mx.controls.Alert;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	
	import spark.components.Group;
	
	
	
	public class CropImageViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:CropImageView;
	   public var delegate:Object;
 	   
	   [Bindable] public var model:CropImageViewModel = new CropImageViewModel(this);
	   private var currentCropbox:Rectangle;
	   private var currentHandleSize:uint = 10;
	   private var currentCropConstraint:Boolean = false
	   
	   // Image 1 ("Image Larger then Component"): Last values for the cropping rectangle, handle size, and aspect ratio constraint settings
	   
	   private var img1Cropbox:Rectangle;
	   private var img1HandleSize:uint = 10;
	   private var img1CropConstraint:Boolean = false;
	   
	   // Image 2 ("Image Smaller than Component"): Last values for the cropping rectangle, handle size, and aspect ratio constraint settings
	   
	   private var img2Cropbox:Rectangle;            
	   private var img2HandleSize:uint = 10;
	   private var img2CropConstraint:Boolean = false;       
	   
	   private var newImage:BitmapData;
	   
	   private var currentAction:String;
	   private var transformer:TransformTool2=new TransformTool2();
	   
	   private var originalImage:ByteArray;
	   
	   
	   public function countHasBeenLoaded(object:Array):void{};
	   
	   public function CropImageViewController() {
			super();
			
		//	myView.imageCropper.sourceImage = model.mainObject;
	   }
	   
	   public function init(event:Event):void {
		 
		   myView = view as CropImageView;		
		   
	   }
	 
	   public function assignImageToCropControl(imageData:Object):void {
		   myView.croppedImage.source = imageData;
		   myView.imageCropper.sourceImage = imageData;		   
	   }
	   
	   public function setOriginalImage(data:ByteArray):void{
	   	originalImage = data;
	   }
	   
	   // --------------------------------------------------------------------------------------------------
	   // imageReady - Called when the ImageCropper component has loaded and initialized an image
	   // --------------------------------------------------------------------------------------------------
	   
	   public function imageReady():void {
		   
		   // Enable the controls (including the imageCropper). Note that the imageCropper must be enabled before changing property values or calling setCropRect().
		   
		   //enableControls(true, true);
		   
		   // Restore the handle size that was previously saved for this image
		   
		   myView.imageCropper.handleSize = currentHandleSize;
		   //  myView.handleSize.value = currentHandleSize;
		   
		   // Restore "Constrain Crop Rectangle to Aspect Ratio" to the setting that was previously saved for the image
		   
		   myView.imageCropper.constrainToAspectRatio = currentCropConstraint;
		   //   myView.constrainToAspectRatio.selected = currentCropConstraint;
		   
		   // If this image was not previously selected then set the cropping rectangle to include the entire image. Otherwise,
		   // restore the cropping rectangle to its previous value. Note that the cropping rectangle is relative to the component
		   // and not to the image because the componentRelative parameter in the call the to setCropRect is set to true.
		   
		   if (!currentCropbox) myView.imageCropper.setCropRect();
		   else myView.imageCropper.setCropRect(currentCropbox.width, currentCropbox.height, currentCropbox.x, currentCropbox.y, true);
		   
		   // Get the cropped image 
		   
		   doCrop();                
	   }
	   
	   // --------------------------------------------------------------------------------------------------
	   // doCrop - Get the cropped image from the ImageCropper component
	   // --------------------------------------------------------------------------------------------------
	   
	   public function doCrop():void {
		   
		   // Get the cropped BitmapData
		   
		   trace("doing crop")
		   newImage = myView.imageCropper.croppedBitmapData;
		   
		   
		   // Set the width and height of the croppedImage Image based on the dimensions of the cropped image
		   
		   myView.croppedImage.width = newImage.width;
		   myView.croppedImage.height = newImage.height;
		   
		   // Create a new Bitmap from the BitmapData and assign it to the croppedImage Image
		   
		   myView.croppedImage.source = new Bitmap(newImage);
		   
		   // Display the cropping rectangle in relative to the ImageCropper component and relative to the image
		   
		   //  myView.imageCropperRect.text = myView.imageCropper.getCropRect(true, true).toString();
		   //  myView.sourceImageRect.text = myView.imageCropper.getCropRect(false, true).toString();
	   }    
	   
	   
	   // resize function code
	   
	   
	   private function doTransform(event:MouseEvent):void
	   {
		   drawActive();
		   
		   if (currentAction == "crop") {
			   applyCrop();
		   }
		   if (currentAction == "transform")
		   {
			   transformer.target=null;
			   currentAction = "";
		   }
		   else
		   {
			   currentAction="transform";
			   transformer.rotationEnabled=true;
			   transformer.maxScaleX=Infinity;
			   transformer.maxScaleY=Infinity;
			   transformer.registrationEnabled=true;
			   transformer.target=myView;
			   (transformer.parent as Group).setElementIndex(transformer, transformer.parent.numChildren - 1);
			   transformer.registration=transformer.boundsCenter;
		   }
	   }
	   
	   private function drawActive():void
	   {
		   (myView as UIComponent).top=undefined;
		   (myView as UIComponent).left=undefined;
	   }
	   
	   private function applyCrop():void
	   {
		  // bCrop.selected=false;
		   currentAction="";
		   
		  /* var rect:Rectangle=cropOverlay.getRect(_assignedObject.parent);
		   
		   _assignedObject.scrollRect=cropOverlay.getRect(_assignedObject);
		   _assignedObject.x=rect.x;
		   _assignedObject.y=rect.y;
		   
		   transformer.target=null;
		   cropOverlay.width=cropOverlay.height=0;
		   cropOverlay.visible=false;*/
	   }
	   
	   // --------------------------------------------------------------------------------------------------
	   // enableControls - Enables or disables the controls
	   // --------------------------------------------------------------------------------------------------
	   
	   
	   public function buttonClicked(evt:Event):void {
			var target:String = evt.currentTarget.id;
			var postScheduleTime:int = 0;
			
			switch (target) {
				case "btnApply":
					//var resultImage:BitmapData = myView.croppedImage.source as BitmapData;
					delegate.croppedImageReady(newImage);
					//delegate.doTransform(newImage);
					removeMe(null);
					break;
				case "btnExit":{
					//myView.imageCropper.height = myView.imageCropper.width = 200;
					if(originalImage != null){
						delegate.noCropedImage(originalImage);
					}else{
						delegate.croppedImageReady(newImage);
					}
					removeMe(null);
					break;
					
				}
				case "btnImgCropper":{
					delegate.croppedImageReady(newImage);
					//delegate.doTransform(newImage);
					removeMe(null);
					break;
					
				}
				default:
					trace("ERROR: no case for switch on MainViewController-buttonClicked for target: " + target);
					break;
			} 
	   }	
	 
	   public function dataHasBeenLoaded(pData:Array):void {}
	}
}
	