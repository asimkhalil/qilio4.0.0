package Controllers
{
	import Components.Home;
	
	import Interfaces.*;
	
	import Models.*;
	import Models.AppStateVars;
	
	import Views.*;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.net.URLLoader;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.events.*;
	import mx.managers.FocusManager;
	import mx.managers.IFocusManagerComponent;
	import mx.managers.IFocusManagerContainer;
	import mx.managers.PopUpManager;
	import mx.validators.*;
	
	import spark.components.BorderContainer;
	import spark.components.TitleWindow;
		
	public class BaseController {
		// View under control
		public var view:*;
		
		// Validation of the fields 
		public var vResult:ValidationResultEvent;

		 public static var ttlWndw:TitleWindow = null;

	    public var focusManager:FocusManager;
		 
		/* ADOBE BUG FIX , if these bogus objects are not created then the object factory fails.*/
		//public var eventsView:EventsView;
		public var scheduledPostsView:ScheduledPostsView;
		public var postView:PostView;
		public var homeView:Home;
		public var statusIdeaView:StatusIdeaView;
		public var contentIdeaView:ContentIdeaView;
		public var settingsView:SettingsView;
		public var keyworkMaintainanceView:KeyworkMaintainanceView;
		public var imageSearchView:ImageSearchView;
		public var videoSearchView:VideoSearchView;
		public var feedViewController:FeedViewController;
		public var editScheduledPostViewController:EditScheduledPostViewController;
		public var iconSelectView:IconSelectView;
		public var feedItemSelectView:FeedItemSelectView;
		public var postPlannerView:PostPlannerView;
		public var postStatisticsView:PostStatisticsView;
		public var cropImageViewController:CropImageViewController;
		
		public function BaseController() {
			
		}  
		
       /* close view */
       public function removeMe(event:Event):void {
		   //if(BaseController.ttlWndw == null) mx.controls.Alert.show("null");
		   //else mx.controls.Alert.show("not null");
		   
		   if(this.view is FeedItemSelectView || this.view is FeedView) 
		   {
			   FlexGlobals.topLevelApplication.dispatchEvent(new Event("CONTENT_IDEA_POPUP_REMOVED",true));					
		   }
		   
		   PopUpManager.removePopUp(this.view);
		   PopUpManager.removePopUp(BaseController.ttlWndw);
		   this.view.dispatchEvent(new Event("CLOSED_POP_UP", true));
		   
       }
	   
	   public function removeTitleWindow():void {
		   PopUpManager.removePopUp(BaseController.ttlWndw);
       }

	   /* Function to retrieve index from array of a particular object */
	   public function getObjectIndexFromArray(targetArray:Array, searchField:String , searchValue:String):int {
		   var indexResult:int = -1;
		   
		   for (var t:int = 0; t < targetArray.length ; t++) {
			   if (targetArray[t][searchField] == searchValue) {
				 
				   indexResult = t;
				   break;
			   } 
		   }
 		   return indexResult;
	   }
	   
	   public function setPrecision(number:Number, precision:int):Number {
		    precision = Math.pow(10, precision);
		    return (Math.round(number * precision)/precision);
	   }
	   
	   public function getObjectIndexFromArrayOfVOsWhereFieldStartsWithLetter(targetArray:Array, startSearchPosition:int, searchField:String , searchFirstLetter:String):int {
		   var indexResult:int = -1;
		   var startedPosition:int = startSearchPosition;
		   
		   searchFirstLetter = searchFirstLetter.toLocaleLowerCase();
		   startSearchPosition = (startSearchPosition < 0) ? 0 : (startSearchPosition + 1);
		   startSearchPosition = (startSearchPosition == targetArray.length) ? 0 : startSearchPosition ;
		   
		   for (var t:int = startSearchPosition; t < targetArray.length ; t++) {
			   if (targetArray[t].hasOwnProperty(searchField)) {
				   if (targetArray[t][searchField].toString().toLocaleLowerCase().indexOf(searchFirstLetter) == 0) {	
					   indexResult = t;
					   break;
				   }
			   } 
		   }
			
		   /* If the search started from a "none-beggining" position , start from the top */
		   if (indexResult == -1 && startedPosition > -1) {
			   //trace("Search came up empty, starting from 0");
				indexResult = getObjectIndexFromArrayOfVOsWhereFieldStartsWithLetter(targetArray, -1, searchField, searchFirstLetter);
		   }
		   
		   return indexResult;
	   }
	   
	   public function getObjectIndexFromArrayOfVOs(targetArray:Array, startSearchPosition:int, searchField:String , searchValue:String):int {
		   var indexResult:int = -1;
		   var startedPosition:int = startSearchPosition;
		   
		   searchValue = searchValue.toLocaleLowerCase();
		   startSearchPosition = (startSearchPosition < 0) ? 0 : (startSearchPosition + 1);
		   startSearchPosition = (startSearchPosition == targetArray.length) ? 0 : startSearchPosition ;
		   
		   for (var t:int = startSearchPosition; t < targetArray.length ; t++) {
			   if (targetArray[t].hasOwnProperty(searchField) && targetArray[t][searchField]) {
				   if (targetArray[t][searchField].toString().toLocaleLowerCase() == searchValue) {	
					   indexResult = t;
					   break;
				   }
			   } 
		   }
		   
		   /* If the search started from a "none-beggining" position , start from the top */
		   if (indexResult == -1 && startedPosition > -1) {
			   //trace("Search came up empty, starting from 0");
			   indexResult = getObjectIndexFromArrayOfVOs(targetArray, -1, searchField, searchValue);
		   }
		   
		   return indexResult;
	   }
	   
	   /* validation of all fields */
       public function validateBeforeSave():Boolean {
        	var result:Boolean = true;
        
            return result;       
        }
         
	    public function keyPressed(event:KeyboardEvent):void {
	    	
	     	if (event.keyCode == 27) {
	     	    removeMe(null);
	     	}
	     	validateBeforeSave(); 
			event.stopImmediatePropagation();
	    }
		
	 	/* can invoke a form from a grid cell click to edit an entity. itemId = is new entry, > 0 is editing entity */
		public function invokePopUp(action:String, gridName:String, itemId:int):void {
       		//trace("BaseController:invokePopUp itemId = " + itemId.toString());
       		var source:String = view.currentState;
			if ((action == "edit" || action =="delete") && itemId == -1 ) {
       			Alert.show("Please choose item to "+ action +"!","Nothing selected",4,null,null);
       			return;
       		}
       		
       		//mainState = source;
       		if (action == "delete") {
       			var alert:Alert = Alert.show("Are you sure you want to delete?",
       				"Delete", Alert.YES | Alert.NO, view, deleteItemRequest);
       			return;
       		}
       }
		 
		
		public function deleteItemRequest(evt:Event):void {
			Alert.show("going to delete object");	
		}

		protected function ttlWndw_close(evt:CloseEvent):void 
		{
			if(viewOpened == "FeedItemSelectView" || viewOpened == "FeedView") 
			{
				FlexGlobals.topLevelApplication.dispatchEvent(new Event("CONTENT_IDEA_POPUP_REMOVED",true));					
			}
			
            PopUpManager.removePopUp(evt.currentTarget as IFlexDisplayObject);
        }
		
       // common form invoker
		
		private var viewOpened:String;
		
       public function showPopUpView(viewName:String, object:Object, action:String, center:Boolean = true, wrapinwindow:Boolean = false, title:String = ""):BorderContainer {
		   
		   viewOpened = viewName;
		   
		   viewName = "Views." + viewName;
		   var dynamicClass:Class = getDefinitionByName(viewName) as Class;

		   if(wrapinwindow == true)
		   {		

			var bc:BorderContainer = new dynamicClass();
			
			dynamicClass(bc).controller.model.mainObject = object as iValueObject;

			BaseController.ttlWndw = new TitleWindow();
			BaseController.ttlWndw.title = title;
			BaseController.ttlWndw.width = bc.width + 10;
			BaseController.ttlWndw.height = bc.height + 45;

			BaseController.ttlWndw.addEventListener(CloseEvent.CLOSE, ttlWndw_close);

			BaseController.ttlWndw.addElement(bc);
			
			bc.x = 5;
			bc.y = 5;
			
			if(viewName == "Views.EmoticonSelectView") {
				PopUpManager.addPopUp(BaseController.ttlWndw, UIComponent(view.parentApplication), false);
			} else {
				PopUpManager.addPopUp(BaseController.ttlWndw, UIComponent(view.parentApplication), true);
			}
			
			PopUpManager.centerPopUp(BaseController.ttlWndw);

			//if(BaseController.ttlWndw == null) mx.controls.Alert.show("null");
			//else mx.controls.Alert.show("not null");

			return bc;

			}
			else
			{

			

		   var pop:BorderContainer = dynamicClass(PopUpManager.createPopUp(UIComponent(view.parentApplication), dynamicClass, true));
		   dynamicClass(pop).controller.model.mainObject = object as iValueObject;
		   
		    pop.currentState = action == "add" ? "" : action;
		
				
			pop.drawFocus(true);
			
			if (center) {
		   		PopUpManager.centerPopUp(pop);
			} else {
				//trace("pop x and y = 0");
				pop.y = 0;
				pop.x = 0;
			}
		  //   pop.drawFocus(true);
			return pop;

			}
       }
    }
}