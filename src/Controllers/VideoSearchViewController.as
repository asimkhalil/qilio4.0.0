package Controllers
{
	import Interfaces.iController;
	
	import Models.*;
	
	
	import Views.*;
	
	import mx.controls.Alert;
	
	import spark.components.BorderContainer;
	
	public class VideoSearchViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:VideoSearchView;
 	   
	   [Bindable] public var model:VideoSearchViewModel = new VideoSearchViewModel(this);
	   [Bindable] public var selectedVideoObj:Object;
	      
	   
	   public function countHasBeenLoaded(object:Array):void{};
	   
	   public function VideoSearchViewController() {
				super();
	   }
	   
	   public function init(event:Event):void {
		   
		   myView = view as VideoSearchView;
 		  
	  }
		   
	   public function selectItem(selectedObject:Object):void {
	   }
	   
	   public function selectedVideo(pSelectedVideo:Object):void {
		   selectedVideoObj = pSelectedVideo;
		   
		  // trace("selectedImageURL= " + selectedVideoURL);
		   this.dispatchEvent(new Event("VIDEO_SELECTED"));
		   this.removeMe(null);  
	   }
	   	   
	   public function selectionChanged(evt:Event):void {
		   var target:String = evt.target.id;
		   
		   switch (target) {
			  case "cmbSource":
				   
				   model.resetSearch();
				   break;
			   
			   default:
				   trace("no case for switch on PostViewController, target = " + target);
				   break;
		   }
	   }
	   
	   public function buttonClicked(evt:Event):void {
			var target:String = evt.currentTarget.id;
			
			switch (target) {
				case "btnSearch":
					myView.pgrBar.visible = true;
					model.performSearch(myView.cmbSource.selectedLabel, myView.txbSearch.text, true);
					break;
				case "btnSettings":
					Alert.show("Settings button");
					break; 
				default:
					trace("ERROR: no case for switch on MainViewController-buttonClicked for target: " + target);
					break;
			}
	   }
	  
	   public function dataHasBeenLoaded(pData:Array):void {
		   myView.pgrBar.visible = false;
	   }
	}
}
	