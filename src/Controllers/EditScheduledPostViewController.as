package Controllers
{
	import Components.DBSocket;
	import Components.QuickDateFormatter;
	
	import Interfaces.iController;
	
	import Models.*;
	
	import VOs.UserFeedVO;
	
	import Views.*;
	
	import flash.events.Event;
	
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	
	import spark.components.BorderContainer;
	
	public class EditScheduledPostViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:EditScheduledPostView;
	   public var delegate:Object;
 	   
	   [Bindable] public var model:EditScheduledPostViewModel = new EditScheduledPostViewModel(this);
	   //[Bindable] public var userFeed:UserFeedVO;
	   
	   public function countHasBeenLoaded(object:Array):void{};
	   
	   public function EditScheduledPostViewController() {
			super();
	   }
	   
	   public function init(event:Event):void {
		   myView = view as EditScheduledPostView;
		   
		  populateViewObjects();
		  addFacebookEventListeners();
	   }
	     
	   private function addFacebookEventListeners():void {
		    model.appStateVars.facebookCOM.addEventListener("UNPUBLISH_POST_SUCESS", unpublishPostSuccessfully);
			model.appStateVars.facebookCOM.addEventListener("UNPUBLISH_POST_FAILED", unpublishPostFailed) ;
			model.appStateVars.facebookCOM.addEventListener("RESCHEDULE_POST_SUCCESS", reschedulePostSuccess); 
			model.appStateVars.facebookCOM.addEventListener("RESCHEDULE_POST_FAILED", reschedulePostFailed); 					   
	   }
	   
	   private function unpublishPostSuccessfully(evt:Event):void {
	   		Alert.show("The post was successfully unpublished","Information");
			delegate.editWindowClosed();
			removeMe(null);
	   }
	   
	   private function unpublishPostFailed(evt:Event):void {
		   Alert.show("There was an error trying to unpublish this post.");
	   }
	   
	   private function reschedulePostSuccess(evt:Event):void {
		   Alert.show("The post was successfully re-scheduled","Information");
		   delegate.editWindowClosed();
		   removeMe(null);
	   }
	   
	   private function reschedulePostFailed(evt:Event):void {
		   Alert.show("There was an error trying to re-schedule this post.");
	   }
	   
	   private function populateViewObjects():void {
		   var refDate:Date;
		   var refDateString:String; 
		   var formatedDate:String;
		   var labelText:String = "not recognized"; 
		   var indexSearch:int = 0;
		   
		   // Created Date
		   refDate 			= QuickDateFormatter.getDateFromUnixTimeStamp(model.mainObject.created_time);
		   refDateString 	= QuickDateFormatter.getDateTimeInMySQLFormatFromDate(refDate);
		   formatedDate 	= QuickDateFormatter.format(refDateString, 'MM/DD/YYY') + ' ' + QuickDateFormatter.formatTime(refDateString);
		   myView.txtCreated.text = formatedDate;
		   // Post type
		   indexSearch = getObjectIndexFromArray(model.appStateVars.facebookCOM.FacebookPostTypesOptions, "data", model.mainObject.type.toString());
		   myView.txtType.text = indexSearch == -1 ? labelText :  model.appStateVars.facebookCOM.FacebookPostTypesOptions[indexSearch].label;
		   
		   // Message
		   if (!model.mainObject.message && model.mainObject.description) {
			   labelText = "none";
			     
		   } 
		   myView.txtMessage.text = model.mainObject.message || model.mainObject.description;
		   
		   // Scheduled_publish_time
		   if (!model.mainObject.scheduled_publish_time) {
			   labelText = "";
			     
		   } 
		   refDate = QuickDateFormatter.getDateFromUnixTimeStamp(model.mainObject.scheduled_publish_time);
		   refDateString = QuickDateFormatter.getDateTimeInMySQLFormatFromDate(refDate);
		   formatedDate = QuickDateFormatter.format(refDateString, 'MM/DD/YYY') + ' ' + QuickDateFormatter.formatTime(refDateString);
		   myView.txtScheduledDate.text = formatedDate;
		   
		   // Status
		   if (model.mainObject.is_published == true) {
			   labelText = "published";
		   } else if(model.mainObject.is_published == false && !model.mainObject.scheduled_publish_time) {
			   labelText = "Unpublished";
		   } else if(model.mainObject.is_published == false && model.mainObject.scheduled_publish_time) {
			   labelText = "Scheduled";
		   }
		   myView.txtPostStatus.text = labelText;
		   
		   // Actions options
		   myView.btnUnpublish.visible  = model.mainObject.is_published == false && model.mainObject.scheduled_publish_time ? true : false;
		   myView.dtpDatetime.visible   = model.mainObject.is_published == false ? true : false;
		 
		   // image if any
		   //myView.imgPost.
	   }
	   
	  /* private function feedWasSavedSuccess(evt:Event):void {
		   trace("received confirmation on save, removing ");
		   this.dispatchEvent(new Event("PROCESSED_FEED"));
		   removeMe(null);   
	   }*/
	   
	  /* private function feedSaveFailed(evt:Event):void {
		   Alert.show("There was an error performing this operation");
	   }*/
	   
	   public function buttonClicked(evt:Event):void {
			var target:String = evt.currentTarget.id;
			var postScheduleTime:int = 0;
			
			switch (target) {
				case "btnPostSchedule":
					// Validate if this is a scheduled post
					if (myView.dtpDatetime.publishState == myView.dtpDatetime.PUBLISH_STATE_SCHEDULED ) {
						
						if (!validateScheduledTime(myView.dtpDatetime.selectedDate)) {
							return;	
						}
						
						postScheduleTime = QuickDateFormatter.getUnixStampTimeFromDate(myView.dtpDatetime.selectedDate);
						model.appStateVars.facebookCOM.reSchedulePost(model.mainObject.pageId, model.mainObject.post_id, postScheduleTime, model.mainObject.pageAccessToken)
					} else {
						model.appStateVars.facebookCOM.reSchedulePost(model.mainObject.pageId, model.mainObject.post_id, 0, model.mainObject.pageAccessToken)
					}
					break;
				case "btnUnpublish":
					var alert:Alert = Alert.show("Are you sure you want to unpublish this post?",
						"Delete", Alert.YES | Alert.NO, view, unpublishPostRequestResponse);
					
					break; 
				
				default:
					trace("ERROR: no case for switch on MainViewController-buttonClicked for target: " + target);
					break;
			} 
	   }
	   
	   public function unpublishPostRequestResponse(evt:CloseEvent):void {
		   if (evt.detail == Alert.NO) {
			   return;
		   }
		   
		   model.appStateVars.facebookCOM.unpublishScheduledPost(model.mainObject.pageId, model.mainObject.post_id, model.mainObject.pageAccessToken);
	   }
	   
	   private function validateScheduledTime(pDate:Date):Boolean {
		   var date:Date = new Date();
		   var diff:int = QuickDateFormatter.getMinuteDiffInDates(pDate, date);
		   
		   if (diff < 10 && myView.dtpDatetime.publishState == myView.dtpDatetime.PUBLISH_STATE_SCHEDULED) {
			   Alert.show("The scheduled date must be at least 10 minutes ahead of current time.", "Scheduled post error");
			   return false;
		   }
		   
		   // trace("date difference " + diff.toString());
		   return true;
	   }
	   
	   public function dataHasBeenLoaded(pData:Array):void {
		  
		   // myView.dgrStatusIdeas.dataProvider = pData;
		   //myView.pgrBar.visible = false;
	   }
	   
	   /* 1) The user requested to delete an item, she gets a chance to say yes or no here */
	   public function deleteEntityRequest():void{
		   var alert:Alert = Alert.show("Are you sure you want to delete this feed?", "Delete", Alert.YES | Alert.NO, view, deleteEntityRequestResponse);
	   }
	   
	   /* 2) The user answer to the "Delete" question goes here, if YES then the comment is deleted for good. */
	   public function deleteEntityRequestResponse(event:CloseEvent):void {
		   if (event.detail == Alert.YES) {
			   deleteEntity();
		   }
	   }
	   
	   /* 3) The real deleting call goes here */
	   public function deleteEntity():void{
		   model.mainObject.deleteVO();
	   }
	}
}
	