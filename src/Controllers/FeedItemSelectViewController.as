package Controllers
{
	import Components.CustomImageControl;
	import Interfaces.iController;
	import Models.*;
	import Views.*;
	import flash.geom.Rectangle;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.CloseEvent;
	
	import spark.components.BorderContainer;
	
	public class FeedItemSelectViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:FeedItemSelectView;
	   public function countHasBeenLoaded(object:Array):void{};
	   public function dataHasBeenLoaded(pData:Array):void {}
	   [Bindable] public var model:FeedItemSelectViewModel = new FeedItemSelectViewModel(this);
	   [Bindable] public var delegate:Object;
	   
	   public function FeedItemSelectViewController() {
			super();
	   }
	   
	   public function init(event:Event):void {
		   myView = view as FeedItemSelectView;
		   model.originalResultSet = model.mainObject.resultsArray;
		   model.totalResults = model.originalResultSet.length;
		   model.loadPage();
		   model.dispatchEvent(new Event("REFRESH_PAGING"));
	   }
	   
	   public function FeedItemSelected(feedItem:Object):void {
		   delegate.selectFeedItem(feedItem);
		   this.removeMe(null);
	   }  
	}
}
	