package Controllers
{
	import Components.DBSocket;
	import Components.QuickDateFormatter;
	
	import Interfaces.iController;
	
	import Models.*;
	
	import VOs.UserInfoVO;
	
	import Views.*;
	
	import flash.display.DisplayObject;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import mx.controls.Alert;
	import mx.controls.ToggleButtonBar;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.FlexGlobals;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import spark.components.BorderContainer;
	
	public class SettingsViewController extends BaseController implements iController {
		
	   /* Model for this View */
	   public var myView:SettingsView;
 	   
	   [Bindable] public var model:SettingsViewModel = new SettingsViewModel(this);
	   [Bindable] public var userInfo:UserInfoVO = new UserInfoVO();
	   
	   public function countHasBeenLoaded(object:Array):void{};

	   public function SettingsViewController() {
			super();
	   }
	   
	   public function handleTabChange(event:Event):void {
		   if((event.target as ToggleButtonBar).selectedIndex != 4) {
			   removeMe(null);
			   FlexGlobals.topLevelApplication.dispatchEvent(new DataEvent("TAB_CHANGED",true,false,(event.target as ToggleButtonBar).selectedIndex.toString()));
		   }
	   }
	   
	   public function init(event:Event):void {
		   
		   model.appStateVars.welcomeshown = false;
		   
		   myView = view as SettingsView;
 		
		   userInfo.id = 1; 
		   userInfo.addEventListener("TRANSACTION_SUCCESS", userLoaded);
		   userInfo.addEventListener("TRANSACTION_FAILED", userLoadFailed);
		   userInfo.getEntity();
	   }
	  
	   private var welcomeVideo:WelcomeVideoPopup;
	   
	   private function userLoaded(evt:Event):void {
		   
		   /*userInfo.removeEventListener("TRANSACTION_SUCCESS", userLoaded);
		   userInfo.removeEventListener("TRANSACTION_FAILED", userLoadFailed);*/
		   
		   model.appStateVars.defaultFanPageId = userInfo.default_fan_page;
		   model.appStateVars.showFeedsCount = userInfo.show_feed_count=='0'?false:true;
		   model.appStateVars.showWelcomeVideo = userInfo.show_welcome_video;
		   myView.chk_showFeedsCount.selected = model.appStateVars.showFeedsCount;
		     
		   myView.dropdown_fanPages.selectedIndex = 0;
		   
		   AppModelLocator.channelsCol = userInfo.channels;
		   
		   AppModelLocator.channelsCol = AppModelLocator.channelsCol.replace("CNBC","");
		   
		   AppModelLocator.channelsCol = AppModelLocator.channelsCol.replace("ESPN","");
		   
		   for(var i:int=0;i<model.appStateVars.facebookCOM.facebookPagesArray.length;i++) {
			   if(model.appStateVars.facebookCOM.facebookPagesArray[i].id == model.appStateVars.defaultFanPageId) {
				   myView.dropdown_fanPages.selectedIndex = i;
				   break;
			   }
		   }
		   
		  /* if(model.appStateVars.showWelcomeVideo == "1" && !model.appStateVars.welcomeshown) {
			   model.appStateVars.welcomeshown = true;
			   model.appStateVars.showWelcomeVideo = "0";
			   saveSettingsClicked(true);
			   welcomeVideo = new WelcomeVideoPopup();
			   PopUpManager.addPopUp(welcomeVideo,FlexGlobals.topLevelApplication as DisplayObject,true);
			   PopUpManager.centerPopUp(welcomeVideo);
			   welcomeVideo.addEventListener("welcomeVideoClosed",closeHnd);
		   }*/
	   }
	   
	   private function closeHnd(event:Event):void {
		   /*FlexGlobals.topLevelApplication.ImportEngine.controller.postView.controller.fetchTreeData();
		   saveSettingsClicked(true);*/
		   myView.callLater(removeWelcomeVideo);
	   }
	   
	   private function removeWelcomeVideo():void {
		   PopUpManager.removePopUp(welcomeVideo);
	   }
	   
	   public function dataChanged(evt:Event):void {
		   model.hasSomethingChanged = true;
	   }
	   
	   
	   
	   private function userLoadFailed(evt:Event):void {
		  Alert.show("There was an error loading user data from database");
	   }
	   
	   public function buttonClicked(evt:Event):void {
			var target:String = evt.currentTarget.id;
			 
			switch (target) {
				case "btnSave":
				
					userInfo.access_key = myView.txbLicense.text;
					
					userInfo.addEventListener("TRANSACTION_SUCCESS", userSavedSuccess);
					userInfo.addEventListener("TRANSACTION_FAILED", userSaveFailed);
					userInfo.updateVO();
					break;
				
				default:
					trace("ERROR: no case for switch on MainViewController-buttonClicked for target: " + target);
					break;
			}		  
	   } 
	   
	   private var _silentSave:Boolean = false;
	   
	   public function saveSettingsClicked(silentSave:Boolean = false):void {
		   
		   FlexGlobals.topLevelApplication.ImportEngine.controller.writeAppSettings();
		   
		   _silentSave = silentSave;
		   model.appStateVars.defaultFanPageId = String(myView.dropdown_fanPages.selectedItem.id);
		   userInfo.default_fan_page = model.appStateVars.defaultFanPageId;
		   userInfo.channels = AppModelLocator.channelsCol;
		   userInfo.show_feed_count = model.appStateVars.showFeedsCount?'1':'0';
		   userInfo.show_welcome_video = model.appStateVars.showWelcomeVideo;
		   userInfo.addEventListener("TRANSACTION_SUCCESS", userSavedSuccess);
		   userInfo.addEventListener("TRANSACTION_FAILED", userSaveFailed);
		   userInfo.updateVO();
	   }
	   
	   public function addChannels(channelNames:String):void 
	   {
		   /*var channels:Array = channelNames.split("\n");*/
		   
		   AppModelLocator.channelsCol = channelNames;
		   
		   /*for(var i:int=0;i<channelNames.length;i++) 
		   {
		   var node:XMLNode = new XMLNode(XMLNodeType.ELEMENT_NODE,'channel');
		   var channelName:String = channels[i];
		   node.appendChild(new XMLNode(XMLNodeType.TEXT_NODE,channelName));
		   AppModelLocator.channels.firstChild.appendChild(node);
		   }*/
		   
		   /*var writeFileStream:FileStream = new FileStream();
		   var channelsFile:File = File.applicationDirectory;
		   channelsFile = new File(channelsFile.nativePath+"/databaseTemplate/channels.txt");
		   
		   writeFileStream.open(channelsFile, FileMode.WRITE);
		   var fileContents:String = channelNames;
		   writeFileStream.writeUTFBytes("");
		   writeFileStream.close();
		   writeFileStream.open(channelsFile, FileMode.WRITE);
		   writeFileStream.writeUTFBytes(fileContents);
		   writeFileStream.close();*/
		   
		   
		   saveSettingsClicked();
	   }
	   
	   private function userSavedSuccess(evt:Event):void {
		   //myView.btnSave0.enabled = false;
		   if(!_silentSave) {
			   model.appStateVars.channelsCollectionDirty = true;
		   		Alert.show("Settings saved successfully! The changes will take effect on next restart","Information");
		   }
		   //myView.lbl_saveSuccessMsg.visible = true;
	   }
	   
	   private function userSaveFailed(evt:Event):void {
		   Alert.show("There was an error saving user data in the database");
	   }
	   
	   public function dataHasBeenLoaded(pData:Array):void {
		    if (pData.length == 0)
				return;
	   }
	}
}
	