package Components
{
	import Interfaces.iController;
	
	import Models.BaseModel;
	
	import VOs.VOBaseCode;
	
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import mx.controls.Alert;

	public class PixaBayCenter extends VOBaseCode {
		
		public const userName:String = "joshwaf";
		public const apiKey:String = "6582fac25d1dfa3c046c";

		public const imageSearchApiURLA:String = "http://pixabay.com/api/?username=" +
												userName +
												"&key=" + 
												apiKey + 
												"&search_term=";
		
		public const imageSearchURLB:String = "&image_type=photo";

		/* Paging components */
		[Bindable] public var originalResultSet:Array;
		[Bindable] public var pagedResults:Array;
		[Bindable] public var pageNum:int = 1;
		[Bindable] public var pageSize:int = 3;
		[Bindable] public var elementsPerRow:int = 3;
		[Bindable] public var totalResults:int = 0;
		
		private var searchWord:String = "";
		
		public var searchResults:int = 400;
		public var urlAddress:String;
		
		//[Bindable] public var imageResultsArray:Array;
	
		
		private var myImagesLoader:URLLoader = new URLLoader();
	
		public function PixaBayCenter() {
			//super(this);
		}
		
		public function searchImages(pSearchString:String, isNewSearch:Boolean, pageNumber:int=1,per_page:int=9):void {
			searchWord = pSearchString;
			if (isNewSearch) {
				pSearchString = pSearchString.split(" ").join("+");
				urlAddress = imageSearchApiURLA + pSearchString + imageSearchURLB;
				trace("pixa address?= " + urlAddress);
				urlAddress += "&page="+pageNumber+"&per_page="+per_page;
				myImagesLoader.addEventListener(Event.COMPLETE, imagesObjectsReceivedFromServer);
				myImagesLoader.load(new URLRequest(urlAddress));
			} else {
				pagedResults = formatArrayForDisplay(originalResultSet);	
				pagedResults = pageResultsSetArray(pagedResults);
				this.dispatchEvent(new Event("PIXABAY_SEARCH_DATA_READY"));
			}
		}
		
		public function imagesObjectsReceivedFromServer(e:Event):void	{
			var rawData:String = String(e.target.data);
			var obj:Object = JSON.parse(rawData);
			
			originalResultSet = new Array();
			originalResultSet = (obj.hits as Array);
			
			//trace("search results count = " + originalResultSet.length.toString());
			if(originalResultSet.length > 0) {
				totalResults = originalResultSet.length;
				
				pagedResults = new Array();
				pagedResults = formatArrayForDisplay(originalResultSet);	
				
				pagedResults = pageResultsSetArray(pagedResults);
				
				
				totalResults = originalResultSet.length;
				
				totalResults = Number(obj.totalHits);
				
			    this.dispatchEvent(new Event("PIXABAY_SEARCH_DATA_READY"));
			} else {
				Alert.show("Search results not found for '"+searchWord+"'","Information");
			}
		}
		
		/* Function to visualize elements in rows of X elements. */
		public function formatArrayForDisplay(pTargetArray:Array):Array {
			var rowArray:Array = new Array();
			var itemCount:int = 1;
			var formattedArray:Array = new Array(); 
			
			for (var t:int = 0; t < totalResults ; t++) {
				rowArray.push(pTargetArray[t]);
				//trace(pTargetArray[t].id.toString());
				if (itemCount % elementsPerRow == 0 && itemCount > 0) {
					formattedArray.push(rowArray);
					rowArray = new Array();
				} 	
				itemCount++;
			}
			
			formattedArray.push(rowArray);
			
			return formattedArray;
		}
		
		private function pageResultsSetArray(pResultSet:Array):Array {
			//trace("paging: pageNum = " + pageNum.toString() +  ", elementsPerRow= " + elementsPerRow.toString() +  ", pageSize= " + pageSize.toString());
			var offset:int 		= 0;/* (pageNum - 1) * pageSize*/;
			var lastRecord:int 	= (pageNum * pageSize) ;
			var pageResultSet:Array = new Array();
			
			for(var t:int = 0 ; t < pResultSet.length; t++) {
				if (t > lastRecord) break;
				
				if (t >= offset && t < lastRecord) {
					pageResultSet.push(pResultSet[t]);
				}
			}
			return pageResultSet;
			//this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
		}
		
		/*public function getObjectFromItem(pObject:Object):Object {
			var tempObject:Object = new Object();
			for (var key:String in pObject) {
				tempObject[key] = pObject[key];
			}
			
			return pObject;
		}		*/
		
		
	}
}