package Components
{
	import Interfaces.iController;
	
	import Models.BaseModel;
	
	import VOs.GoogleBlogResultVO;
	import VOs.GoogleNewsResultVO;
	
	import events.GenericEvent;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.sampler.stopSampling;
	import flash.xml.XMLDocument;
	import flash.xml.XMLNode;
	
	import mx.controls.Alert;
	import mx.rpc.xml.SimpleXMLDecoder;
	import mx.utils.ArrayUtil;
	import mx.utils.StringUtil;

	public class GoogleSearchCenter extends BaseModel {
		public const blogSearchUrlA:String = "https://ajax.googleapis.com/ajax/services/search/blogs?v=1.0&q=";
		public const blogSearchUrlB:String = "&start=";
			
		public const newsSearchUrlA:String = "http://news.google.com/news?hl=en&gl=us&q=";
		public const newsSearchUrlB:String = "&um=1&ie=UTF-8&output=rss";
		
		public const videoSearchA:String = "https://gdata.youtube.com/feeds/api/videos?q=";
		public const videoSearchB:String = "&orderby=published&start-index=1&max-results=";
		
		//public var isLoppedSearch:Boolean = false;
		public var blogLoopSearchStartPoint:int = 0;
		public var blogSearchString:String = "";
		public var blogMaxResults:int = 50;
		
		public var videoSearchResultsCount:int = 50;
		public var urlAddress:String;
		
		[Bindable] public var originalResultSet:Array = new Array();
		[Bindable] public var pagedResults:Array;
		
		[Bindable] public var headlineResults:Array;
		
		private var myVideoLoader:URLLoader = new URLLoader();
		private var myNewsLoader:URLLoader  = new URLLoader();
		private var myBlogLoader:URLLoader  = new URLLoader();

		[Bindable] public var pageNum:int = 1;
		[Bindable] public var pageSize:int = 3;
		[Bindable] public var elementsPerRow:int = 1;
		[Bindable] public var totalResults:int = 0;
		
		private var requestedPage:int = 2;
		
		//public const CUSTOM_PAGE_SIZE:int = 3;		
		
		public function GoogleSearchCenter() {
			super(this);
		}
		
		public function searchVideos(pSearchString:String, isNewSearch:Boolean):void {
			if (isNewSearch) {
				pSearchString = pSearchString.split(" ").join("+");
				urlAddress = videoSearchA + pSearchString + videoSearchB + videoSearchResultsCount.toString();
				
				myVideoLoader.addEventListener(Event.COMPLETE, receivedVideoResults);
				myVideoLoader.load(new URLRequest("https://www.googleapis.com/youtube/v3/search?part=snippet&q="+pSearchString+"&key=AIzaSyCIKxCblLTK0BIJRNNTZbkLDJtTyt6s06w&maxResults=50&type=video"));
			} else {
				pagedResults = formatArrayForDisplay(originalResultSet);	
				pagedResults = pageResultsSetArray(pagedResults);
				this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
			}
		}
		
		public function loadImagesFeed(feedUrl:String, isNewSearch:Boolean):void {
			if (isNewSearch) {
				urlAddress = feedUrl;
				myVideoLoader.addEventListener(Event.COMPLETE, receivedImageFeedResults);
				myVideoLoader.addEventListener(IOErrorEvent.IO_ERROR,ioHandler)
				myVideoLoader.load(new URLRequest(urlAddress));
			} else {
				pagedResults = formatArrayForDisplay(originalResultSet);	
				pagedResults = pageResultsSetArray(pagedResults);
				this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
			}
		}
		
		private function ioHandler(event:IOErrorEvent):void{
			totalResults = originalResultSet.length;	
			
			if (elementsPerRow > 1) {
				pagedResults = formatArrayForDisplay(originalResultSet);	
			}
			
			pagedResults = pageResultsSetArray(pagedResults);
			
			this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
		}
		
		public function getHeadlineFeeds(pSearchString:String):void {
			urlAddress = newsSearchUrlA + pSearchString + newsSearchUrlB + "&num=20";
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE,headlineNewsLoaded);
			urlLoader.load(new URLRequest(urlAddress));
		}
		
		private function headlineNewsLoaded(e:Event):void {
			var xml_doc:XMLDocument = new XMLDocument(StringUtil.trimArrayElements(e.target.data, ">"));
			var xml_dec:SimpleXMLDecoder = new SimpleXMLDecoder();
			var data:Object = xml_dec.decodeXML(xml_doc);
			
			originalResultSet = ArrayUtil.toArray(data.rss.channel.item);
			
			for ( var t:int = 0; t < originalResultSet.length; t++) {
				if (originalResultSet[t]) {
					originalResultSet[t].content = originalResultSet[t].description;	
					originalResultSet[t].postUrl = originalResultSet[t].link;
					originalResultSet[t].imgURL = getMainImageFromContent(originalResultSet[t].description);
				}
			}
			
			headlineResults = originalResultSet;
			
			this.dispatchEvent(new Event("GOOGLE_HEADLINE_FEED_READY"));
		}
		
		public function searchNews(pSearchString:String, isNewSearch:Boolean):void {
			if (isNewSearch) {
				pSearchString = pSearchString.split(" ").join("+");
				urlAddress = newsSearchUrlA + pSearchString + newsSearchUrlB + "&num=100";
				trace("News address = " + urlAddress);
				myNewsLoader.addEventListener(Event.COMPLETE, receivedNewsResults);
				myNewsLoader.load(new URLRequest(urlAddress));
			} else {
				pagedResults = pageResultsSetArray(originalResultSet);
				
				
				trace("pagedResults count = " + pagedResults.length.toString());
				if (pagedResults.length == 0) {
					this.dispatchEvent(new Event("GOOGLE_SEARCH_END_OF_RESULTS"));
				} else {
					this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
				}
				
				
				//this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
			}
		}
		
		public function searchBlog(pSearchString:String):void {
			pSearchString = pSearchString.split(" ").join("+");
			//urlAddress = blogSearchUrlA + pSearchString + blogSearchUrlB + ((pageNum - 1) * 4).toString() + "&rsz=10";
			urlAddress = blogSearchUrlA + pSearchString + blogSearchUrlB + ((pageNum - 1) * 4).toString();
			trace("News blog = " + urlAddress);
				
			myBlogLoader.addEventListener(Event.COMPLETE, receivedBlogData);
			myBlogLoader.load(new URLRequest(urlAddress));	
		}
		
		public function receivedVideoResults(e:Event):void	{
			var xmlData:Object = JSON.parse(e.target.data);
			originalResultSet = xmlData.items;
			
			//originalResultSet = cleanUpResultsFromVideoSearch(xmlData);
			totalResults = originalResultSet.length;	
			
			if (elementsPerRow > 1) {
				elementsPerRow++; 
				pagedResults = formatArrayForDisplay(originalResultSet);	
			}
			
			pagedResults = pageResultsSetArray(pagedResults);
			
			this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
		}		
		
		public function receivedImageFeedResults(e:Event):void	{
			//originalResultSet = new Array();
			
			originalResultSet = originalResultSet.concat(cleanUpResultsFromImageFeedSearch(e));
			urlAddress += "&paged="+requestedPage;
			myVideoLoader.load(new URLRequest(urlAddress));
			requestedPage++;
		}		
		
		public function cleanUpResultsFromVideoSearch(xml:XML):Array {
			var tempList:Array = new Array();
			var tempObject:Object;
			var atom:Namespace = new Namespace("http://www.w3.org/2005/Atom");
			var media:Namespace = new Namespace("http://search.yahoo.com/mrss/");
			var gd:Namespace = new Namespace("http://schemas.google.com/g/2005");
			var yt:Namespace = new Namespace("http://gdata.youtube.com/schemas/2007");
			default xml namespace = atom; // IMPORTANT
			var xlist:XMLList = new XMLList(xml..media::player);
			var xTlist:XMLList = new XMLList(xml..media::title);
			var xDlist:XMLList = new XMLList(xml.entry);
			var url:String;
			var code:String;
			
			for (var t:int = 0; t < xlist.length(); t++) {
				url = xlist[t].@url;
				url = url.substr(0, url.indexOf("&feature"));
				code= url.substr((url.indexOf("?v=") + 3), url.length);
				
//				Alert.show(xTlist[t]);
				tempObject = new Object();
				tempObject.videoURI = url;
				tempObject.title = xTlist[t];
				tempObject.content = xDlist[t].content.toString();
				tempObject.source = "Youtube";
				tempObject.videoCode = code;
				tempObject.videoThumbnail = "http://img.youtube.com/vi/" + code + "/0.jpg";
				tempList.push(tempObject);
			}
			default xml namespace = new Namespace( "" );
			return tempList;
		} 
		
		public function cleanUpResultsFromImageFeedSearch(event:Event):Array {
			var tempImgFeed:Array = new Array();
			var xmlFeed:XMLDocument = new XMLDocument(event.target.data);
			var rssNode:XMLNode;
			for each(var rssN:XMLNode in xmlFeed.childNodes) {
				if(rssN.nodeName == "rss") {
					rssNode = rssN;
				}
			}
			
			if(rssNode) {
			
				var feedEntries:Array = rssNode.firstChild.nextSibling.childNodes;
			
				for each(var feed:XMLNode in feedEntries) 
				{
					if(feed.nodeName == "item") 
					{
						
						var imgFeeds:Array = feed.childNodes;
						var path:String, title:String,link:String;
						for each(var img:XMLNode in imgFeeds){
							
							if(img.nodeName == "content:encoded"){
								var x:XMLDocument = new XMLDocument(img.childNodes[0].nodeValue);
								path = (x.childNodes[0].childNodes[0].attributes.href);
							}else if(img.nodeName == "title"){
								title = img.childNodes[0].nodeValue;
							}else if(img.nodeName == "link"){
								link = img.childNodes[0].nodeValue;
							}
							if((path != null) && (title != null) && (link != null)){
								tempImgFeed.push({title:title,imgPath:path,link:link});
								path = title = link = null;
							}
						}
					}
					
				}
			} 
			else 
			{
				for each(var rssN:XMLNode in xmlFeed.childNodes) {
					if(rssN.nodeName == "root") {
						for each(var imageNode:XMLNode in rssN.childNodes) {
							if(imageNode.firstChild) {
								tempImgFeed.push({title:"",imgPath:imageNode.firstChild.nodeValue.toString(),link:imageNode.firstChild.nodeValue.toString()});
							}
						}
					}
				}
			}
			return tempImgFeed;
		} 
		
		public function receivedNewsResults(e:Event):void	{
			var xml_doc:XMLDocument = new XMLDocument(StringUtil.trimArrayElements(e.target.data, ">"));
			var xml_dec:SimpleXMLDecoder = new SimpleXMLDecoder();
			var data:Object = xml_dec.decodeXML(xml_doc);
			
			originalResultSet = ArrayUtil.toArray(data.rss.channel.item);
			 
			for ( var t:int = 0; t < originalResultSet.length; t++) {
				if (originalResultSet[t]) {
					originalResultSet[t].content = originalResultSet[t].description;	
					originalResultSet[t].postUrl = originalResultSet[t].link;
					originalResultSet[t].imgURL = getMainImageFromContent(originalResultSet[t].description);
				}
			}
			
			totalResults = originalResultSet.length;
			pagedResults = pageResultsSetArray(originalResultSet);
			
			this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
			
		}
		
		private function getMainImageFromContent(pContent:String):String {
			var imgSource:String;
			
			imgSource = pContent.substring(
											(pContent.indexOf("img src=") + 11),
											(pContent.indexOf("\"", (pContent.indexOf("img src=") + 11)))
										  );
			//trace("imgSource = " + imgSource);								
			return imgSource;
		}
		
		private function receivedBlogData(e:Event):void	{
			/*
			Warning: You should include the userip parameter, which supplies the IP address of the end-user who made the request and validates that you are not making a
			utomated requests in violation of the Terms of Service. 
			*/
			
			var tempObject:Object = JSON.parse(e.target.data);
			
			if (!tempObject.responseData && tempObject.responseDetails != "out of range start") {
				Alert.show("Error: " + tempObject.responseDetails);
				this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_FAILED"));
				return;
			} else if (tempObject.responseDetails == "out of range start") {
				this.dispatchEvent(new Event("GOOGLE_SEARCH_END_OF_RESULTS"));
				return;
			}
			
			for ( var t:int = 0; t < tempObject.responseData.results.length; t++) {
				if (tempObject.responseData.results[t]) {
					tempObject.responseData.results[t].content = tempObject.responseData.results[t].content;
					tempObject.responseData.results[t].postUrl = tempObject.responseData.results[t].link!=undefined?tempObject.responseData.results[t].link:tempObject.responseData.results[t].postUrl;
					tempObject.responseData.results[t].imgURL = "";
				}
			}
			
			originalResultSet = tempObject.responseData.results;
			pagedResults = tempObject.responseData.results;
			this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
			
			//originalResultSet = arrConcatUnique(tempObject.responseData.results, originalResultSet);
					
			/*if (tempObject.responseData.results.length > 0 && blogMaxResults > originalResultSet.length) {
				blogLoopSearchStartPoint++;
				searchBlog(blogSearchString);
			} else {
				blogLoopSearchStartPoint = 0;
				totalResults = originalResultSet.length;
				pagedResults = pageResultsSetArray(originalResultSet);
				this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
			}*/
		}		
		
		/* Add array together */
		/*private function arrConcatUnique(...args):Array
		{
			var retArr:Array = new Array();
			for each (var arg:* in args)
			{
				if (arg is Array)
				{
					for each (var value:* in arg)
					{
						if (retArr.indexOf(value) == -1)
							retArr.push(value);
					}
				}
			}
			return retArr;
		}
		*/
		
		/*private function pageResultsSetXML(pResultSet:XMLList):void {
			//trace("pageResultsSet ran");
			var offset:int 		= (pageNum - 1) * pageSize;
			var lastRecord:int 	= (pageNum * pageSize) ;
			var cleanDescription:String; 
			 
			pagedResults		= new Array();
			
			for(var t:int ; t < pResultSet.length(); t++) {
				if (t > lastRecord) break;
				
				if (t > offset) {
					pResultSet[t].description = pResultSet[t].description.split("src=\"//").join("src=\""); 
					pagedResults.appendChild(pResultSet[t]);
				}
			}
			this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
		}*/
			
		/* Function to visualize elements in rows of X elements. */
		public function formatArrayForDisplay(pTargetArray:Array):Array {
			var rowArray:Array = new Array();
			var itemCount:int = 1;
			var formattedArray:Array = new Array(); 
						
			for (var t:int = 0; t < totalResults ; t++) {
				rowArray.push(pTargetArray[t]);
				
				if (itemCount % elementsPerRow == 0 && itemCount > 0) {
					formattedArray.push(rowArray);
					rowArray = new Array();
				} 	
				itemCount++;
			}
			
			formattedArray.push(rowArray);
			return formattedArray;
		}
		
		public function pageResultsSetArray(pResultSet:Array):Array {
			trace("paging: pageNum = " + pageNum.toString() +  ", elementsPerRow= " + elementsPerRow.toString() +  ", pageSize= " + pageSize.toString());
			var offset:int 		= (pageNum - 1) * pageSize;
			var lastRecord:int 	= (pageNum * pageSize) ;
			var pageResultSet:Array = new Array();
						
			for(var t:int = 0 ; t < pResultSet.length; t++) {
				if (t > lastRecord) break;
				
				if (t >= offset && t < lastRecord) {
					pageResultSet.push(pResultSet[t]);
				}
			}
			
			return pageResultSet;
			//this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
		}
	}
}
