		// ActionScript file
		   
	/* Available font list */
	[Embed(source="BaroqueScript.ttf",
					fontName = "BaroqueScript",
					mimeType = "application/x-font")]
	public var BaroqueScript:Class;
	
	[Embed(source="Cake Nom.ttf",
					fontName = "CakeNom",
					mimeType = "application/x-font")]
	public var CakeNom:Class;
	
	[Embed(source="arial.ttf",
					fontName = "Arial Normal",
					mimeType = "application/x-font")]
	public var ArialN:Class;
	
	[Embed(source="arialbd.ttf",
					fontName = "Arial Bold",
					mimeType = "application/x-font")]
	public var ArialB:Class;
	
	[Embed(source="arialbi.ttf",
					fontName = "Arial Bold Italic",
					mimeType = "application/x-font")]
	public var ArialBI:Class;
	
	[Embed(source="ariali.ttf",
					fontName = "Arial Italic",				
					mimeType = "application/x-font")]
	public var ArialI:Class;

	[Embed(source="BebasNeue.otf",
					fontName = "BebasNeue",				
					mimeType = "application/x-font")]
	public var BebasNeue:Class;

	[Embed(source="impact.ttf",
					fontName = "Impact",				
					mimeType = "application/x-font")]
	public var Impact:Class;

	[Embed(source="Press Style.ttf",
					fontName = "Press Style",				
					mimeType = "application/x-font")]
	public var PressStyle:Class;
	
	[Embed(source="The Rainmaker.otf",
					fontName = "The Rainmaker",				
					mimeType = "application/x-font")]
	public var TheRainmaker:Class;

	[Embed(source="varsity_regular.ttf",
					fontName = "Varsity Regular",				
					mimeType = "application/x-font")]
	public var Varsity:Class;
	
	[Embed(source="verdana.ttf",
					fontName = "Verdana",
					mimeType = "application/x-font")]
	public var Verdana:Class;
	
	[Embed(source="verdanab.ttf",
					fontName = "Verdana Bold",
					mimeType = "application/x-font")]
	public var VerdanaB:Class;
	
	[Embed(source="verdanai.ttf",
					fontName = "Verdana Italic",
					mimeType = "application/x-font")]
	public var VerdanaI:Class;
	
	[Embed(source="verdanaz.ttf",
					fontName = "Verdana Z",
					mimeType = "application/x-font")]
	public var VerdanaZ:Class;		