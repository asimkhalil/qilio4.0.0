package Components
{
	import Interfaces.iController;
	
	import Models.BaseModel;
	
	import VOs.VOBaseCode;
	
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import mx.controls.Alert;

	public class MorgueFileCenter extends VOBaseCode {

		public const imageSearchApiURLA:String = "http://www.morguefile.com/archive/search/";
		
		/* Paging components */
		[Bindable] public var originalResultSet:Array;
		[Bindable] public var pagedResults:Array;
		[Bindable] public var pageNum:int = 1;
		[Bindable] public var pageSize:int = 3;
		[Bindable] public var elementsPerRow:int = 3;
		[Bindable] public var totalResults:int = 0;
		
		private var searchWord:String = "";
		
		public var searchResults:int = 400;
		public var urlAddress:String;
		
		//[Bindable] public var imageResultsArray:Array;
	
		
		private var myImagesLoader:URLLoader = new URLLoader();
	
		public function PixaBayCenter() {
			//super(this);
		}
		
		public function searchImages(pSearchString:String, isNewSearch:Boolean, pageNumber:int=1,per_page:int=9):void {
			searchWord = pSearchString;
			if (isNewSearch) {
				pSearchString = pSearchString.split(" ").join("+");
				urlAddress = imageSearchApiURLA+"/"+pageNumber+"/?q="+pSearchString+"&photo_lib=morgueFile";
				myImagesLoader.addEventListener(Event.COMPLETE, imagesObjectsReceivedFromServer);
				myImagesLoader.load(new URLRequest(urlAddress));
			}
		}
		
		public function imagesObjectsReceivedFromServer(e:Event):void	{
			var rawData:String = String(e.target.data);
			var obj:Object = JSON.parse(rawData);
			
			originalResultSet = new Array();
			originalResultSet = (obj.doc as Array);
			
			//trace("search results count = " + originalResultSet.length.toString());
			if(originalResultSet.length > 0) {
				totalResults = originalResultSet.length;
				pagedResults = new Array();
				pagedResults = formatArrayForDisplay(originalResultSet);	
				pagedResults = pageResultsSetArray(pagedResults);
				totalResults = originalResultSet.length;
				totalResults = Number(obj.total);
			    this.dispatchEvent(new Event("MORGUEFILE_SEARCH_DATA_READY"));
			} else {
				Alert.show("Search results not found for '"+searchWord+"'","Information");
			}
		}
		
		/* Function to visualize elements in rows of X elements. */
		public function formatArrayForDisplay(pTargetArray:Array):Array {
			var rowArray:Array = new Array();
			var itemCount:int = 1;
			var formattedArray:Array = new Array(); 
			
			for (var t:int = 0; t < totalResults ; t++) {
				rowArray.push(getObjectFromData(pTargetArray[t]));
				//trace(pTargetArray[t].id.toString());
				if (itemCount % elementsPerRow == 0 && itemCount > 0) {
					formattedArray.push(rowArray);
					rowArray = new Array();
				} 	
				itemCount++;
			}
			
			formattedArray.push(rowArray);
			
			return formattedArray;
		}
		
		private function getObjectFromData(item:Object):Object {
			var obj:Object = new Object();
			obj.comments = "";
			obj.downloads = item["Archive"]["downloads"];
			obj.id = item["Archive"]["unique_id"];
			obj.imageHeight = item["Archive"]["file_height"];
			obj.imageWidth = item["Archive"]["file_width"];
			obj.likes = item["Archive"]["likes"];
			obj.model_release = item["Archive"]["license"];
			obj.pageURL = "http://www.morguefile.com/archive/display/"+item["Archive"]["unique_id"];
			obj.previewHeight = item["Archive"]["file_height"];
			obj.previewURL =item["Archive"]["file_path_med"].replace("/med/", "/preview/");
			obj.previewWidth = item["Archive"]["file_width"];;
			obj.tags = "0";
			obj.types = "0";
			obj.user = "";
			obj.views = "";
			obj.webformatHeight = item["Archive"]["file_height"];
			obj.webformatURL = item["Archive"]["file_path_med"].replace("/med/", "/preview/");
			obj.webformatWidth = item["Archive"]["file_height"];
			return obj;
		}
		
		private function pageResultsSetArray(pResultSet:Array):Array {
			var offset:int 		= 0;
			var lastRecord:int 	= (pageNum * pageSize) ;
			var pageResultSet:Array = new Array();
			
			for(var t:int = 0 ; t < pResultSet.length; t++) {
				if (t > lastRecord) break;
				
				if (t >= offset && t < lastRecord) {
					pageResultSet.push(pResultSet[t]);
				}
			}
			return pageResultSet;
		}
		
	}
}