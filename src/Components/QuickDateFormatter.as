package Components
{
	import mx.controls.Alert;
	import mx.formatters.DateFormatter;
	
	public class QuickDateFormatter
	{
		public static function format(str_dateString:String, str_dateFormat:String):String
		{
			if (!str_dateString)
				return "";
			
			if (str_dateString.length > 8) {
				str_dateString = str_dateString.substr(0,10);	
			}
			
			var f:DateFormatter = new DateFormatter();
			f.formatString = str_dateFormat;
			return f.format(str_dateString);
		}
		
		public static function getUnixStampTimeFromDate(pDate:Date):int {
			var utc:Number = Math.round(pDate.time/1000);
			return utc;
		}
		
		public static function getDateFromUnixTimeStamp(pUnixTimeStamp:int):Date {
			var rDate:Date = new Date(pUnixTimeStamp * 1000);
			return rDate;
		}
		
		public static function formatTime(time:String):String {
			if (time.length > 8) {
				time = time.substr(11,19) ;
			}
				 
			var hour:Number = Number(time.substr(0,2));
			var minutes:Number = Number(time.substr(3,2));
			var amPm:String;
			var min:String;
			var hourS:String;
			
			if (hour > 12) {
				hourS = (hour - 12).toString();
				hourS = hourS.length == 1 ? ("0"+hourS) : hourS;
				amPm = " pm";
			} else if(hour == 0) {
				hourS = "12";
				amPm = " am";
			} else {
				hourS = hour.toString();
				amPm = " am";
			}
			
			min = minutes.toString();
			min = min.length == 1 ? ("0" + min) : min;
			min = min.length == 1 ? ("0" + min) : min;
			hourS = hourS.length == 1 ? ("0" + hourS) : hourS;
			return hourS + ":" + min + amPm;
		}
		
		public static function getDateTimeInMySQLFormatFromDate(date:Date):String {
			var stringFormat:String;
			var day:int			= date.getDate();
			var month:int		= date.getMonth() + 1;
			var year:int		= date.getFullYear();
			var hour:int		= date.getHours();
			var minutes:int		= date.getMinutes();
			var seconds:int		= date.getSeconds();
			
			 // 2011-12-29 13:00:00
			stringFormat = 	year.toString() 	+ "-" +
							(month.toString().length == 1 ? "0" : "")   + month.toString()	  + "-" +
							(day.toString().length == 1 ? "0" : "")     + day.toString()	  + " " +
							(hour.toString().length == 1 ? "0" : "")    + hour.toString()     + ":" +
							(minutes.toString().length == 1 ? "0" : "") + minutes.toString()  + ":" +
							(seconds.toString().length == 1 ? "0" : "") + seconds.toString();
			
			return stringFormat;	
		}
		
		/* convert string to dateTime */
		public static function convertStringToDateTime(dateString:String):Date {    
			trace("convertStringToDateTime           processing " + dateString);
		    if ( dateString == "" ) {
				Alert.show("QuickDateTimeFormatter: convertStringToDateTime: This date is not  in correct format: "+ dateString);
			    return null;    
			}
		    
			if ( dateString.length != 10 && dateString.length != 19) {
				Alert.show("QuickDateTimeFormatter: convertStringToDateTime: This date is not  in correct format: "+ dateString);
			    return null;
			}
		
		    dateString = dateString.replace("-", "/");
		    dateString = dateString.replace("-", "/");
		
		    return new Date(Date.parse( dateString ));
		}
		
		public static function getMinuteDiffInDates(pTopDate:Date, pBottomDate:Date):int {
			var milliseconds:Number = pTopDate.getTime() - pBottomDate.getTime();
			// make sure it’s in the future
			//if (milliseconds > 0)
			//{
				var seconds:Number = milliseconds / 1000;
				var minutes:Number = seconds / 60;
				var hours:Number = minutes / 60;
				var days:Number = Math.floor(hours / 24);
				
				return minutes;
				//trace("days left: " + days);
				
				//if (days == 0)
				//	trace("it’s today!!");
			//}
			//else
			//{
			//	trace("not in the future");
			//}	
		}
		
		public static function datetimeDifference(topDateTime:Date, bottomDateTime:Date):int {
			// when first date is older than second, the answer is 1, younger gives -1, 0 = equals
			if (!topDateTime || !bottomDateTime)
				return 0;
			
			 var date1Timestamp : Number = topDateTime.getTime ();
			 var date2Timestamp : Number = bottomDateTime.getTime ();
			//trace("date 1 = "+ topDateTime.toString());
			//trace("date 2 = "+ bottomDateTime.toString());
			    var result : Number = -1;
			
			    if (date1Timestamp == date2Timestamp)
			    {
				        result = 0;
			    }
			    else if (date1Timestamp > date2Timestamp)
			    {
				        result = 1;
			    }
			
			    return result;

		}
		
		public static function dateAdd(datepart:String = "", number:Number = 0, date:Date = null):Date {
			if (date == null) {
				/* Default to current date. */
				date = new Date();
			}
			 
			var returnDate:Date = new Date(date.time);;
			 
			switch (datepart.toLowerCase()) {
				case "fullyear":
				case "month":
				case "date":
				case "hours":
				case "minutes":
				case "seconds":
				case "milliseconds":
					returnDate[datepart] += number;
					break;
				default:
					/* Unknown date part, do nothing. */
					break;
			}
			return returnDate;
		}
	}
}