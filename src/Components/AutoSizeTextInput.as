package Components {
	
	import flash.events.Event;  
	import mx.controls.TextInput;
	
	public class AutoSizeTextInput extends TextInput {
		
		private var txtSpan:uint = 10;
		
		public function AutoSizeTextInput() {
			super();
			this.addEventListener(Event.CHANGE, onTextChange);
		}
		
		private function onTextChange(evnt:Event):void {
			this.textField.scrollH = 0;
			this.width = this.autoSizeWidth;
		}
		
		override protected function commitProperties():void {
			super.commitProperties();
			this.textField.scrollH = 0;
			this.width = this.autoSizeWidth;
		}
		
		private function get autoSizeWidth():uint {
			return (this.textWidth+txtSpan);            
		}
	}
}