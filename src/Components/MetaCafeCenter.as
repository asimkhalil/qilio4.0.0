package Components
{
	import Interfaces.iController;
	
	import Models.BaseModel;
	
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import mx.controls.Alert;

	public class MetaCafeCenter extends BaseModel {
		//http://www.metacafe.com/api/videos/?vq=funny+dogs
		
		public const searchUrl:String = "http://www.metacafe.com/api/videos/?vq=";
		
		public var searchResults:int = 50;
		public var urlAddress:String;
		
		[Bindable] public var originalResultSet:Array;
		[Bindable] public var pagedResults:Array;
		
		private var myVideoLoader:URLLoader = new URLLoader();
		
		[Bindable] public var pageNum:int = 1;
		[Bindable] public var pageSize:int = 3;
		[Bindable] public var elementsPerRow:int = 1;
		
		[Bindable] public var totalResults:int = 0;
		
		public function MetaCafeCenter() {
			super(this);
		}
		
		public function searchVideos(pSearchString:String, isNewSearch:Boolean):void {
			if (isNewSearch) {
				pSearchString = pSearchString.split(" ").join("+");
				urlAddress = searchUrl + pSearchString;
				
				myVideoLoader.addEventListener(Event.COMPLETE, receivedVideoResults);
				myVideoLoader.load(new URLRequest(urlAddress));
			} else {
				pagedResults = formatArrayForDisplay(originalResultSet);	
				pagedResults = pageResultsSetArray(pagedResults);
				this.dispatchEvent(new Event("METACAFE_SEARCH_DATA_READY"));
			}
		}
		
		/* Function to visualize elements in rows of X elements. */
		public function formatArrayForDisplay(pTargetArray:Array):Array {
			var rowArray:Array = new Array();
			var itemCount:int = 1;
			var formattedArray:Array = new Array(); 
			
			for (var t:int = 0; t < totalResults ; t++) {
				rowArray.push(pTargetArray[t]);
				
				if (itemCount % elementsPerRow == 0 && itemCount > 0) {
					formattedArray.push(rowArray);
					rowArray = new Array();
				} 	
				itemCount++;
			}
			
			formattedArray.push(rowArray);
			return formattedArray;
		}
		
		public function receivedVideoResults(e:Event):void	{
			var xmlData:XML = new XML(e.target.data);
			
			originalResultSet = cleanUpResultsFromVideoSearch(xmlData);
			totalResults = originalResultSet.length;	
			
			if (elementsPerRow > 1) {
				pagedResults = formatArrayForDisplay(originalResultSet);	
			}
			
			pagedResults = pageResultsSetArray(pagedResults);
			
			
			this.dispatchEvent(new Event("METACAFE_SEARCH_DATA_READY"));
		}
		
		private function pageResultsSetArray(pResultSet:Array):Array {
			trace("paging: pageNum = " + pageNum.toString() +  ", elementsPerRow= " + elementsPerRow.toString() +  ", pageSize= " + pageSize.toString());
			var offset:int 		= (pageNum - 1) * pageSize;
			var lastRecord:int 	= (pageNum * pageSize) ;
			var pageResultSet:Array = new Array();
			
			for(var t:int = 0 ; t < pResultSet.length; t++) {
				if (t > lastRecord) break;
				
				if (t >= offset && t < lastRecord) {
					pageResultSet.push(pResultSet[t]);
				}
			}
			return pageResultSet;
			//this.dispatchEvent(new Event("GOOGLE_SEARCH_DATA_READY"));
		}
		
		/*private function pageVideoResultsSet(pResultSet:XMLList):void {
			var offset:int 		= (pageNum - 1) * pageSize;
			var lastRecord:int 	= (pageNum * pageSize) ;
			var cleanDescription:String; 
			//trace("paging results, items =" + pResultSet.length().toString());
			/*pagedResults		= new XMLList("<root/>");
			
			for(var t:int ; t < pResultSet.length(); t++) {
				if (t > lastRecord) break;
				
				if (t > offset) {
					//pResultSet[t].description = pResultSet[t].description.split("src=\"//").join("src=\""); 
					pagedResults.appendChild(pResultSet[t]);
				}
			}	
			//trace(pagedResults);
			
			this.dispatchEvent(new Event("METACAFE_SEARCH_DATA_READY"));
			//trace ("total records = "+ totalResults.toString())
		}*/
		
		public function cleanUpResultsFromVideoSearch(xml:XML):Array {
			var tempList:Array = new Array();
			var tempObject:Object;
			var atom:Namespace = new Namespace("http://www.w3.org/2005/Atom");
			var media:Namespace = new Namespace("http://search.yahoo.com/mrss/");
			//var gd:Namespace = new Namespace("http://schemas.google.com/g/2005");
			//var yt:Namespace = new Namespace("http://gdata.youtube.com/schemas/2007");
			default xml namespace = atom; 
			var xlist:XMLList = new XMLList(xml..media::player);
			var imgList:XMLList = new XMLList(xml..media::thumbnail); 
			var url:String;
			var imgURL:String;
			
			/*trace("0",xml.children()[22]);                  
			trace("1",xml.inScopeNamespaces());
			trace("2",xml..media::group);                   
			trace("3",xml..yt::statistics.@favoriteCount);                  
			trace("4",xml..yt::duration.@seconds);                  
			trace("5",xml..author ); // why?????????
			trace("6",xml.author ); // why?????????
			*/
			//trace(xml..media::group);
			//var x:int = 0;
			
			//trace(xml..media::player);
			
			//trace("player=", xlist);
			//trace("length=", xlist.length().toString());
			
			for (var t:int = 0; t < xlist.length(); t++) {
				url    = xlist[t].@url;
				imgURL = imgList[t].@url;
				//url = url.substr(0, url.indexOf("&feature"));
				//code= url.substr((url.indexOf("?v=") + 3), url.length);
				
				tempObject = new Object();
				tempObject.source = "Metacafe";
				tempObject.videoURI = url;
				tempObject.videoCode = "";
				tempObject.videoThumbnail = imgURL;
				
				tempList.push(tempObject);
			}
			//trace("Done getting data, item count = " + tempList.length.toString());
			return tempList;
		} 
		
		/*private function pageResultsSet(pResultSet:XMLList):void {
			var offset:int 		= (pageNum - 1) * pageSize;
			var lastRecord:int 	= (pageNum * pageSize) ;
			var cleanDescription:String; 
			//trace("paging results =" + pResultSet);
			pagedResults		= new XMLList("<root/>");
			
			for(var t:int ; t < pResultSet.length(); t++) {
				if (t > lastRecord) break;
				
				if (t > offset) {
					pResultSet[t].description = pResultSet[t].description.split("src=\"//").join("src=\""); 
					pagedResults.appendChild(pResultSet[t]);
				}
			}
			this.dispatchEvent(new Event("METACAFE_SEARCH_DATA_READY"));
			//trace ("total records = "+ totalResults.toString())
			trace(pagedResults);
		}*/
	}
}