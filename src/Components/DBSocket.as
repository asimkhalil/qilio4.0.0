package Components
{
	import Interfaces.iController;
	
	import Models.BaseModel;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.errors.SQLError;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.xml.XMLDocument;
	import flash.xml.XMLNode;
	import flash.xml.XMLNodeType;
	
	import mx.controls.Alert;

	public class DBSocket extends BaseModel {
		public var conn:SQLConnection 		= new SQLConnection();
		public var selectData:SQLStatement;
		public var delegate:Object;
		
		public function DBSocket(pDelegate:Object) {
			super(this);
			this.delegate = pDelegate;
		}
		
		public function openDBConnection():void {
			if (this.conn.connected) {
				//trace("Connectionalready opened");
				return;
			}
		
			var dbFile:File = File.applicationStorageDirectory.resolvePath("fanpagedb.sqlite");
			
			try
			{
				conn.open(dbFile, SQLMode.UPDATE);
				///trace("the database opened successfully");
			
			}
			catch (error:SQLError)
			{
				appStateVars.isDBReady = false;
				trace("Error message:", error.message);
				trace("Details:", error.details);
			}
		}		
		
		/*public function processSchedulePost():void {
			
		}
		
		public function processUserinfo():void {
			
		}*/
		
		/*public function enternewIdeas():void {
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE,compl);
			urlLoader.load(new URLRequest("ideas.xml"));
			
			
		}
		
		private function compl(event:Event):void {
			selectData = new SQLStatement();
			selectData.sqlConnection = this.conn;
			selectData.addEventListener(SQLEvent.RESULT, tempSuccess);
			selectData.addEventListener(SQLErrorEvent.ERROR, temperror);
			
			var evt:XMLDocument = new XMLDocument(event.target.data);
			
			for(var i:int=0;i<evt.firstChild.childNodes.length;i++) {
				var xmlNode:XMLNode = evt.firstChild.childNodes[i] as XMLNode;
				if(xmlNode.nodeType == XMLNodeType.ELEMENT_NODE) {
					var idea:String = xmlNode.firstChild.nodeValue;
					var sqlStatement:String = "INSERT INTO status_ideas(status_category,status_idea, enabled) VALUES('Facebook Status Collection','"+idea+"',0)";
					selectData.text = sqlStatement;
					selectData.execute();
				}
			}
		}
		
		private function tempSuccess(event:SQLEvent):void {
			trace("Success: "+event.type);
		}
		
		private function temperror(event:SQLEvent):void {
			trace("Error: "+event.type);
		}*/
		
		public function getStatusCategories():void {
			var sqlStatement:String		= "select status_category as label from status_ideas group by status_category";
			
			selectData = new SQLStatement();
			
			selectData.sqlConnection = this.conn;
			selectData.text = sqlStatement;
			
			selectData.addEventListener(SQLEvent.RESULT, getStatusCategorySuccessHandler);
			selectData.addEventListener(SQLErrorEvent.ERROR, getStatusCategorySuccessFailed);
			selectData.execute();			
		}
		
		public function getStatusIdeasCount(pCategory:String,keyword:String = ""):void {
			var sqlStatement:String	= "";
			if(keyword != "") {
				sqlStatement = "select count(*) as cuenta from status_ideas where status_idea LIKE '%"+keyword+"%'";
			} else {
				sqlStatement = "select count(*) as cuenta from status_ideas where status_category = '" + pCategory + "'";
			}
			selectData = new SQLStatement();
			selectData.sqlConnection = this.conn;
			selectData.text = sqlStatement;
			
			selectData.addEventListener(SQLEvent.RESULT		, statusIdeasCountSuccessHandler);
			selectData.addEventListener(SQLErrorEvent.ERROR	, statusIdeasCountSuccessFailed);
			selectData.execute();			
		}
		
		private function statusIdeasCountSuccessHandler(event:SQLEvent):void {
			var result:SQLResult = selectData.getResult();
			
			delegate.statusIdeasCountDataLoaded(result.data[0].cuenta);
		}
		
		private function statusIdeasCountSuccessFailed(event:SQLEvent):void {
			Alert.show("There was an error counting status ideas");
		}
		
		private function getStatusCategorySuccessHandler(event:SQLEvent):void {
			var result:SQLResult = selectData.getResult();
			
			delegate.statusCategoriesDataLoaded(result.data);
		}
		
		private function getStatusCategorySuccessFailed(evt:Event):void {
			Alert.show("There was a problem loading status category ideas");
		}
		
		public function getEntities(pSourceName:String, pSearchFields:Array, pOrderBy:String, pPageSize:int, pPageNumber:int, onlyGetCount:Boolean, keyword:String = ""):void {
			var offset:int 				= 0;
			var limitStatement:String 	= "";
			var orderByStatement:String = "";
			var searchPatch:String 		= "";
			var sqlStatement:String		= "";
			var getFieldsOrCount:String = "*";
			
			
			/* apply paging if any was requested */
			if (pPageSize > 0) {
				offset = (pPageNumber - 1) * pPageSize;
				limitStatement = " limit " + offset + "," + pPageSize;
			}
			
			/* set order field if requested */
			if (pOrderBy != "") {
				orderByStatement = " order by " + pOrderBy ;
			}
			
			/* if only returning count then */
			if (onlyGetCount == true) {
				getFieldsOrCount = " count(*) as count";
				limitStatement	 = "";
				orderByStatement = "";
				
			}
			
			/* Add search parameters if any */
			if (pSearchFields) {
				for (var t:int = 0; t < pSearchFields.length; t++){
					searchPatch += pSearchFields[t]['field'] + "= '" + pSearchFields[t]['value'] + "' and ";
				}
			}
			
			/* put the statement parts together */
			if (searchPatch != "") {
				searchPatch = searchPatch.substr(0, searchPatch.length - 4);
				if(keyword != "" && pSourceName == "status_ideas") {
					sqlStatement 	   = "SELECT " + getFieldsOrCount + " from " + pSourceName + " where status_idea LIKE '%"+keyword+"%'" + orderByStatement + " " + limitStatement;
				} else {
					sqlStatement 	   = "SELECT " + getFieldsOrCount + " from " + pSourceName + " where " + searchPatch + " " + orderByStatement + " " + limitStatement;
				}
			} else {
				sqlStatement 	   = "SELECT " + getFieldsOrCount + " from " + pSourceName + " " + orderByStatement + " " + limitStatement;
			}	
			
			selectData = new SQLStatement();
			selectData.addEventListener(SQLEvent.RESULT, transactionResultHandler);
			selectData.addEventListener(SQLErrorEvent.ERROR, transactionErrorHandler);
			
			//trace("sql = " + sqlStatement);
			selectData.sqlConnection = this.conn;
			selectData.text = sqlStatement;
			selectData.execute();			
		}
		
		public function getKeywords():void {
			var sqlStatement:String		= "";
			selectData 					= new SQLStatement();
			
			selectData.addEventListener(SQLEvent.RESULT, transactionResultHandler);
			selectData.addEventListener(SQLErrorEvent.ERROR, transactionErrorHandler);
			
			sqlStatement = "select * from keyword_maintainance";
			selectData.sqlConnection = this.conn;
			selectData.text = sqlStatement;
			selectData.execute();
		}
		
		public function getUserFeeds():void {
			var sqlStatement:String		= "";
			selectData 					= new SQLStatement();
			
			selectData.addEventListener(SQLEvent.RESULT, transactionResultHandler);
			selectData.addEventListener(SQLErrorEvent.ERROR, transactionErrorHandler);
			
			sqlStatement 	   = "select * from user_feeds";
			selectData.sqlConnection = this.conn;
			selectData.text = sqlStatement;
			selectData.execute();			
		}
		
		public function getScheduledPost(pPageSize:int, pPageNumber:int, dateFrom:String, dateTo:String, onlyGetCount:Boolean):void {
			var offset:int 				= 0;
			var limitStatement:String 	= "";
			var sqlStatement:String		= "";
			var getFieldsOrCount:String = "*, STRFTIME('%Y-%m-%d %H:%M:%S',scheduled_date) as formated_post_date," + 
 								 		  " STRFTIME('%Y-%m-%d %H:%M:%S',entered_date) as formated_entered_date";
			
			/* apply paging if any was requested */
			if (pPageSize > 0) {
				offset = (pPageNumber - 1) * pPageSize;
				limitStatement = " limit " + offset + "," + pPageSize;
			}
			
			selectData = new SQLStatement();
			
			/* if only returning count then */
			if (onlyGetCount == true) {
				getFieldsOrCount = " count(*) as count";
				limitStatement	 = "";
				selectData.addEventListener(SQLEvent.RESULT, countResultHandler);
				selectData.addEventListener(SQLErrorEvent.ERROR, countErrorHandler);
			} else {
				selectData.addEventListener(SQLEvent.RESULT, transactionResultHandler);
				selectData.addEventListener(SQLErrorEvent.ERROR, transactionErrorHandler);
			}
			
			sqlStatement 	   = "select " + getFieldsOrCount + " from scheduled_posts" + " where scheduled_date between STRFTIME('%J','" + dateFrom + "') and STRFTIME('%J','" + dateTo + "') " + limitStatement;
			
			//trace("scheculed_posts sql = " + sqlStatement);
			selectData.sqlConnection = this.conn;
			selectData.text = sqlStatement;
			selectData.execute();			
		}
		
		public function removeImageFromScheduledPost():void {
			
		}
		/*
		public function errorHandler(event:SQLErrorEvent):void {
			trace("Error message:", event.error.message);
			trace("Details:", event.error.details);
		}
		
		public function openHandler(event:SQLEvent):void {
			//trace("the database was created successfully");
			appStateVars.isDBReady = true;
		}*/
		
		public function transactionResultHandler(event:SQLEvent):void {
			var result:SQLResult = selectData.getResult();
			
			if (result.data) {
				var numResults:int = result.data.length;
 				delegate.dataHasBeenLoaded(result.data);
			} else {
				delegate.dataHasBeenLoaded(new Array());
			}		
		}
		
		public function transactionErrorHandler(event:SQLErrorEvent):void {
			trace("Error message:", event.error.message);
			trace("Details:", event.error.details);
			Alert.show("There was an error with this transaction: " + event.error.message + " " + event.error.details);
		}
		
		public function countResultHandler(event:SQLEvent):void {
			var result:SQLResult = selectData.getResult();
			var numResults:int = result.data.length;
			delegate.countHasBeenLoaded(result.data);		
		}
		
		public function countErrorHandler(event:SQLErrorEvent):void {
			trace("Error message:", event.error.message);
			trace("Details:", event.error.details);
			Alert.show("There was an error counting records with message " + event.error.message + " " + event.error.details);
		}
	}
}