package 
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.Event;
	
	public class BaseEventDispatcher implements IEventDispatcher
	{
		private static var _instance:BaseEventDispatcher;
		private var _eventDispatcher:EventDispatcher;
		
		public function BaseEventDispatcher(enforcer:SingletonEnforcer) {
			_eventDispatcher = new EventDispatcher(this);
		}
		
		public static function getInstance():BaseEventDispatcher
		{
			if (_instance == null)
			{
				_instance = new BaseEventDispatcher(new SingletonEnforcer());
			}
			
			return _instance;
		}
		
		public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0.0, useWeakReference:Boolean = false):void {
			_eventDispatcher.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}
		
		public function dispatchEvent(event:Event):Boolean {
			//Debugger.sendMessage("the event type registered is " + event.type);
			return _eventDispatcher.dispatchEvent(event);
		}
		
		public function hasEventListener(type:String):Boolean {
			return _eventDispatcher.hasEventListener(type);
		}
		
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void {
			//Debugger.sendMessage("removing the event type registered  " + type);
			_eventDispatcher.removeEventListener(type, listener, useCapture);
		}
		
		public function willTrigger(type:String):Boolean {
			return _eventDispatcher.willTrigger(type);
		}
	}
}
class SingletonEnforcer {}