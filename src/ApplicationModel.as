package 
{
	import mx.collections.ArrayCollection;

	public class ApplicationModel
	{
		[Bindable]
		public static var appId:String = "";
		
		[Bindable]
		public static var appSecret:String = "";
		
		public static var createdDate:String = "";
		
		public static var SINGLE_USER_LICENSE:String = "singleUserlicense";
		
		public static var MULTI_USER_LICENSE:String = "multiUserlicense";
		
		public static var licenseType:String = MULTI_USER_LICENSE;
		
		//Schedule posts batch loading
		
		/*public static var loadingOffset:Number = 0;
		
		public static var loadingOffsetInterval:Number = 12;*/
		
		public static var loadingOffset:Number = 0;
		
		public static var loadingOffsetInterval:Number = 200;
		
		[Bindable]
		public static var headerHideMode:Boolean = false;
		
		[Bindable]
		public static var applyHideHeader:Boolean = false;
		
		public static var welcomeTextURL:String = "http://commentmaximiser.com/welcome.html";
		
		public static var targeting:ArrayCollection = new ArrayCollection();
		
		public static var pagesWithTargets:ArrayCollection = new ArrayCollection();
		
		public static var isLite:Boolean = false;
		
		public static var macAddress:String = "";
	}
}