package Interfaces
{
	public interface iValueObject
	{
		function saveVO():void;
		function deleteVO():void;
		function getEntity():void;
		
		//function getVOStoredProcedureParams():Array;
	}
}