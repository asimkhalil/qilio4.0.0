package Interfaces
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;

	public interface iController
	{
		function removeMe(event:Event):void;
		function validateBeforeSave():Boolean;
		function init(event:Event):void;
		function dataHasBeenLoaded(object:Array):void;
		function countHasBeenLoaded(object:Array):void;
	}
}