package Interfaces
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;

	public interface iModel
	{
		//function getEntityList(tableOrView:String, searchField:String, searchValue:String, userId:int, accessKey:String, 
		//					   orderBy:String, pageSize:int, pageNumber:int, flexIdentifier:String):void;
		function loadListOfObjectsFromArray(objectList:Array, targetArray:String, objectClass:String,labelForComboBoxOption:String = "", dataForComboBoxOption:String = ""):void;
	}
}