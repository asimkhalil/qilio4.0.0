package net.freesource
{
	import mx.core.IFactory;

	public class CustomFlex4HandleFactory implements IFactory
	{
		public function CustomFlex4HandleFactory()
		{
		}
		
		public var deleteType:Boolean = false;
		public var rotateType:Boolean = false;
		
		public function newInstance():*
		{
			return new MyCustomHandlesClass(deleteType,rotateType);
		}
	}
}