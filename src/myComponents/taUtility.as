package myComponents {

    import flash.events.KeyboardEvent;
    import flash.text.TextField;
    
    import mx.containers.VBox;
    import mx.controls.TextArea;
    import mx.controls.textClasses.TextRange;
    import mx.core.IUITextField;
    import mx.core.mx_internal;
        
    public class taUtility {
        
        // The two commented constants are the default values for an A4 page
        //public static const DEFAULT_WIDTH:int = 792;
        //public static const DEFAULT_HEIGHT:int = 1121;
        
        public static const DEFAULT_WIDTH:int = 300;
        public static const DEFAULT_HEIGHT:int = 300;
        
        public static function getTextArea():TextArea {
            var ta:TextArea = new TextArea();
            ta.width = DEFAULT_WIDTH;
            ta.height = DEFAULT_HEIGHT;
            ta.horizontalScrollPolicy="off";
            ta.verticalScrollPolicy="on";
         
            return ta;
        }
        
        public static function getDefaultWidth():int {
        	return DEFAULT_WIDTH;
        }
        
		public static function isBeginningOfTextArea(ta:TextArea):Boolean {
			var tf:TextField = ta.mx_internal::getTextField();
			if(tf.caretIndex == 0)
				return true;
				else
				return false;
		}
		
		public static function taDeselect(allTaSelected:Boolean, taContainer:VBox):void {
			if(allTaSelected) {
				for(var i:uint=0; i<taContainer.numChildren; i++) {
					var taTextSelection:TextArea = taContainer.getChildAt(i) as TextArea;
					taTextSelection.setSelection(0,0);
					taTextSelection.setStyle("backgroundColor", 0xFFFFFF);
				}
			}
		}
		
		/* Will be used for combo boxes and color pickers "close" event 
		   to shift focus back to the textarea */
		public static function taFocus(userTaSelection:TextRange):void {
			if(userTaSelection != null && userTaSelection.text != "") {
				var ta:TextArea = userTaSelection.owner as TextArea;
				if(ta is TextArea) {
					ta.setFocus();
				}
			}
		}
		
		public static function taRemoveChildren(event:KeyboardEvent, taContainer:VBox):void {
			if(taContainer.numChildren > 1) {					
				for(var i:uint; i<taContainer.numChildren; i++) {
					if(taContainer.numChildren > 1) {
						taContainer.removeChildAt(i);
					}
				}
			}
		}
		
		public static function getFirstLine(ta:TextArea):String {
			// get the textArea's textField
			ta.validateNow();
			var textField:IUITextField = ta.mx_internal::getTextField();
			// get the number of lines in the textArea
			var numLines:Number = textField.numLines;
			// get the last line of text
			var firstLine:String = textField.getLineText(0);
			
			return firstLine;
		}
		
		public static function getLastLine(ta:TextArea):String {
			// get the textArea's textField
			ta.validateNow();
			var textField:IUITextField = ta.mx_internal::getTextField();
			// get the number of lines in the textArea
			var numLines:Number = textField.numLines;
			// get the last line of text
			var lastLine:String = textField.getLineText(numLines - 1);
			
			return lastLine;
		}
		
		public static function isEndOfTextArea(ta:TextArea):Boolean {
			if((ta.height - ta.mx_internal::getTextField().height) 
					>= ((ta.height - ta.textHeight) - 10))
			return true;
			else
			return false;
		}
		
		public static function enoughTextAreaSpace(ta:TextArea):Boolean {
			if((ta.height - ta.mx_internal::getTextField().height) 
					< ((ta.height - ta.textHeight) - 10))
			return true;
			else
			return false;
		}
    }
}