package myComponents {
	
	import flash.events.*;
	import mx.containers.VBox;
	import mx.controls.ComboBase;
	import mx.controls.ComboBox;
	import mx.controls.TextArea;
	import mx.controls.sliderClasses.Slider;
	import mx.controls.textClasses.TextRange;
	import mx.events.*;
	
	public class taToolbar
	{		
		public static function setBold(userTaSelection:TextRange):void {
			if(userTaSelection is TextRange && userTaSelection.text != "") {
				if(userTaSelection.fontWeight != "bold") {
					userTaSelection.fontWeight="bold";
				} else {
					userTaSelection.fontWeight="normal";
				}
				// Set focus to textarea 
				(userTaSelection.owner as TextArea).setFocus();
			}
		}
		
		public static function setItalic(userTaSelection:TextRange):void {
			if(userTaSelection is TextRange && userTaSelection.text != "") {
				if(userTaSelection.fontStyle != "italic") {
					userTaSelection.fontStyle = "italic";
				} else {
					userTaSelection.fontStyle = "normal";
				}
				// Set focus to textarea 
				(userTaSelection.owner as TextArea).setFocus();
			}
		}
		
		public static function setUnderline(userTaSelection:TextRange):void {
			if(userTaSelection is TextRange && userTaSelection.text != "") {
				if(userTaSelection.textDecoration != "underline") {
					userTaSelection.textDecoration = "underline";
				} else {
					userTaSelection.textDecoration = "normal";
				}
				// Set focus to textarea 
				(userTaSelection.owner as TextArea).setFocus();
			}
		}

		public static function setColor(event:ColorPickerEvent, userTaSelection:TextRange):void {
			if(userTaSelection is TextRange && userTaSelection.text != "") {
				userTaSelection.color = event.color;
				// Set focus to textarea 
				(userTaSelection.owner as TextArea).setFocus();
			}
		}

		public static function setAlign(event:ItemClickEvent, userTaSelection:TextRange):void {
			if(userTaSelection is TextRange && userTaSelection.text != "") {
				userTaSelection.textAlign = event.label;
				// Set focus to textarea 
				(userTaSelection.owner as TextArea).setFocus();
			}
		}
				
		public static function setBulletPoint(userTaSelection:TextRange):void {
			if(userTaSelection is TextRange && userTaSelection.text != "") {
				if(!userTaSelection.bullet) {
					userTaSelection.bullet = true;
				} else {
					userTaSelection.bullet = false;
				}
				// Set focus to textarea 
				(userTaSelection.owner as TextArea).setFocus();
			}
		}
		
		public static function setFont(event:ListEvent, userTaSelection:TextRange):void {
			if(userTaSelection is TextRange && userTaSelection.text != "") {
				userTaSelection.fontFamily = (event.currentTarget as ComboBox).selectedItem.toString();
			}
		}
		
		public static function setFontSize(event:ListEvent, userTaSelection:TextRange):void {
			var fSize:Object = (event.currentTarget as ComboBox).selectedItem;
			if(userTaSelection is TextRange && userTaSelection.text != "" && fSize != null) {
				userTaSelection.fontSize = fSize.toString();
			}
		}
		
		/* ***BUGGY FUNCTION*** */
		public static function setCustomFontSize(event:FlexEvent, userTaSelection:TextRange):void {
			var fSize:int = (event.currentTarget as int);
			if(userTaSelection is TextRange && userTaSelection.text != "" && fSize.toString() != "") {
				userTaSelection.fontSize = fSize;
			}
		}
				
		public static function setPageSize(taContainer:VBox, event:ListEvent):void {
			var cBox:ComboBox = event.currentTarget as ComboBox;
			
			if(cBox != null) {
				var size:Number = (Number(cBox.selectedItem) / 100) * 1;
				
				for(var i:uint=0; i<taContainer.numChildren; i++) {
					taContainer.getChildAt(i).scaleX = size;
					taContainer.getChildAt(i).scaleY = size;
				}
			}
		}
	}
}