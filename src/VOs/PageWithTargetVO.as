package VOs
{
	public class PageWithTargetVO
	{
		public var pageId:String = "";
		public var target:String = "";
		public var dateTime:Date;
	}
}