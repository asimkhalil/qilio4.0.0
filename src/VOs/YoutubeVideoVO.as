package VOs
{

	import Interfaces.iValueObject;
	
	import Models.AppStateVars;
	
	public class YoutubeVideoVO extends VOBaseCode implements iValueObject
	{
		/* VO Properties */
		[Bindable] public var id:String;
		/* All inside <media:group> */
		[Bindable] public var title:String 	= ""; //media:title
		[Bindable] public var name:String		= "";
		
		/* These next two fields must be extracted from "https://www.youtube.com/watch?v=ELtzZ5lJnBk&amp;feature=youtube_gdata_player"/> 
		
			params.link		= pLink; 	//"https://www.youtube.com/watch?v=5ByYZgXFOvA";
			params.source	= pSource; 	//"http://www.youtube.com/v/5ByYZgXFOvA";
		
		*/
		[Bindable] public var source:String		= ""; //player url
		[Bindable] public var link:String 		= ""; // player url 
		
		
		[Bindable] public var thumbnail:String		= ""; // media:thumbnail There might be many ending in (0-5).jpg
		[Bindable] public var duration:String	= ""; //yt:duration		
		
		public function YoutubeVideoVO(){
			super();
		}
		
		public function saveVO():void {}
		public function getEntity():void{};
		public function dataHasBeenLoaded(pData:Array):void {}
		public function deleteVO():void {}
	}
}