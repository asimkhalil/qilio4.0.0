package VOs
{
	/* Getters, setters and XML Proxy are in the parent class */
	import Components.DBSocket;
	
	import Interfaces.iValueObject;
	
	import Models.AppStateVars;
	
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.net.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import mx.collections.ArrayCollection;
	import mx.formatters.DateFormatter;
	import mx.rpc.events.ResultEvent;
	import mx.utils.Base64Decoder;
	import mx.utils.Base64Encoder;
	
	public class ScheduledPostVO extends VOBaseCode implements iValueObject
	{
		/* VO Properties */
		[Bindable] public var id:int;
		[Bindable] public var post_name:String 			= "";
		[Bindable] public var post_type:Number			= 0;
		[Bindable] public var scheduled_date:String		= "";
		[Bindable] public var status:Number				= 0;
		[Bindable] public var entered_date:String		= "";
		[Bindable] public var content_text:String		= "";
		[Bindable] public var content_link_url:String	= "";
		[Bindable] public var contentImage:Bitmap;
		[Bindable] public var video_url:String			= "";
		[Bindable] public var content_header:String		= "";
		[Bindable] public var image_url:String			= "";
		[Bindable] public var image_data:Object			= "";
		
		// Facebook API fields for scheduledPost
		[Bindable] public var attachment:Object;
		[Bindable] public var created_time:int;
		[Bindable] public var description:String		= "";
		[Bindable] public var is_published:int;
		[Bindable] public var message:String		= "";
		[Bindable] public var post_id:String		= "";
		[Bindable] public var scheduled_publish_time:int	;
		[Bindable] public var source_id:int;
		[Bindable] public var type:int;
		[Bindable] public var updated_time:int;
		
		[Bindable] public var pageAccessToken:String;
		[Bindable] public var pageId:String;
		[Bindable] public var permalink:String;
		
		private var sqls:SQLStatement = new SQLStatement();
		
		public function ScheduledPostVO()
		{
			super();
			//setServicesParameters("events","id","process_event");
		}
		
		private function toByteString(content:String):String 
		{
			var base64Encoder:Base64Encoder = new Base64Encoder();
			base64Encoder.encode(content);
			return base64Encoder.toString();
		}
		
		private function bytesToString(content:String):String 
		{
			var base64Decoder:Base64Decoder = new Base64Decoder();
			base64Decoder.decode(content);
			return base64Decoder.toByteArray().toString();
		}
		
		public function saveVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			var contentBytes:String = toByteString(content_text);
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "INSERT INTO scheduled_posts (post_name, post_type, scheduled_date, status, entered_date, content_text, content_link_url, video_url) VALUES('" + 
				//+first_name.text+"','"+last_name.text+"');";
				post_name			+ "','" +
				post_type.toString()+ "',  STRFTIME('%J','" +
				scheduled_date		+ "'),'" +
				status.toString()	+ "',  STRFTIME('%J','" +
				entered_date		+ "'),'" +
				contentBytes		+ "','" +
				content_link_url	+ "','" +
				video_url			+ "');"; 
			
			//trace("insert statement = " + sqls.text);
				
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
	
		}
		
		public function getEntity():void{
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			var params:Array = new Array({field:"id", value:id.toString()});
			appStateVars.appDataBase.getEntities("scheduled_posts", params, "", 0, 0, false);
		};
		
		public function getResultCount():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			
			sqls.text = "select * from scheduled_posts order by scheduled_date"; 
			
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, onSchedulePostCountResult);	
			sqls.execute();
		}
		
		private function filterFunc(item:Object):Boolean {
			var currentDate:Date = new Date();
			if(item.scheduled_date > currentDate)
				return true;
			return false;
		}
		
		private function onSchedulePostCountResult(evt:Event):void {
			var result:SQLResult = sqls.getResult();
			var col:ArrayCollection = new ArrayCollection(result.data);
			col.filterFunction = filterFunc;
			col.refresh();
			if(col && col.length > 0) {
				var appStateVars:AppStateVars = AppStateVars.getInstance();
				appStateVars.scheduledPostsCount = col.length;
				var lastResult:Object = col.getItemAt(0);
				var df:DateFormatter = new DateFormatter();
				df.formatString = "MM/DD/YYYY JJ:NN A";
				appStateVars.nextScheduledPostTime = df.format(lastResult.scheduled_date);
			}
		}
		
		public function dataHasBeenLoaded(pData:Array):void {
			var scheduledPostObject:Object = pData[0];
			scheduledPostObject.content_text = bytesToString(scheduledPostObject.content_text);
			this.loadDataFromResult(scheduledPostObject, false);
			this.dispatchEvent(new Event("TRANSACTION_SUCCESS", false));			
		}
		
		public function updateVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "update scheduled_posts set " + //(post_name, post_type, scheduled_date, status, entered_date, content_text, content_link_url, video_url) VALUES('" + 
 				"post_name = '" 					+ post_name				+ "'," +
				"post_type = " 						+ post_type.toString()	+ ",  " +
				"scheduled_date = STRFTIME('%J','" 	+ scheduled_date		+ "')," +
				"status = " 						+ status.toString()		+ ",  " +
				"entered_date = STRFTIME('%J','"	+ entered_date			+ "')," +
				"content_text = '"					+ content_text			+ "'," +
				"content_link_url = '"				+ content_link_url		+ "'," +
				"video_url = '"						+ video_url				+ "' where id = " + this.id.toString(); 
			
			//trace("update statement = " + sqls.text);
			
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
		}
		
		public function deleteVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "delete from scheduled_posts where id = " + this.id.toString(); 
			
			//trace("update statement = " + sqls.text);
			
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
		}
		
		private function transactionError(evt:Event):void {
			this.dispatchEvent(new Event("TRANSACTION_FAILED", false));
		}
		
		private function transactionResult(evt:Event):void {
			this.dispatchEvent(new Event("TRANSACTION_SUCCESS", false));
		}
	}
}