package VOs
{
	/* Getters, setters and XML Proxy are in the parent class */
	import Components.DBSocket;
	
	import Interfaces.iValueObject;
	
	import Models.AppStateVars;
	
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.net.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;
	
	import mx.rpc.events.ResultEvent;
	
	public class FacebookPageVO extends VOBaseCode 
	{
		
		/* VO Properties */
		[Bindable] public var id:String;
		[Bindable] public var access_token:String 		= "";
		[Bindable] public var status_idea:String		= "";
		[Bindable] public var category:String			= "";
		[Bindable] public var name:String				= "";
		[Bindable] public var perms:Object; // (0-x as index, names on each example EDIT PROFILE, etc.);
		
		[Bindable] public var thumb:ByteArray;
		
		//used for content lib tree rendering icons
		public static const TOP_CAT:String = "topcat";
		public static const SUB_CAT:String = "subcat";
		[Bindable] public var type:String
		
		[Bindable] public var children:Array; // used for adding feeds that are related to this Fan page in the database.
		
		public function FacebookPageVO(){
			super();
		}	
	}
}