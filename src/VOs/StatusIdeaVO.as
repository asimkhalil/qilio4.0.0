package VOs
{
	/* Getters, setters and XML Proxy are in the parent class */
	import Components.DBSocket;
	
	import Interfaces.iValueObject;
	
	import Models.AppStateVars;
	
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.net.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import mx.rpc.events.ResultEvent;
	
	public class StatusIdeaVO extends VOBaseCode implements iValueObject
	{
		/* VO Properties */
		[Bindable] public var id:int;
		[Bindable] public var status_category:String 	= "";
		[Bindable] public var status_idea:String		= "";
		[Bindable] public var enabled:Number			= 0;
		
		private var sqls:SQLStatement = new SQLStatement();
		
		public function StatusIdeaVO(){
			super();
		}
		
		public function saveVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "INSERT INTO status_ideas (status_category, status_idea, enabled) VALUES('" + 
				status_category		+ "','" +
				status_idea			+ "'," +
				enabled.toString()	+ ")"; 
			
			//trace("insert statement = " + sqls.text);
				
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
	
		}
		
		public function getEntity():void{
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			var params:Array = new Array({field:"id", value:id.toString()});
			appStateVars.appDataBase.getEntities("status_idea", params, "", 0, 0, false);
		};
		
		public function dataHasBeenLoaded(pData:Array):void {
			
			this.loadDataFromResult(pData[0], false);
			this.dispatchEvent(new Event("TRANSACTION_SUCCESS", false));			
		}
		
		public function updateVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "update status_idea set " + //(post_name, post_type, scheduled_date, status, entered_date, content_text, content_link_url, video_url) VALUES('" + 
 				"status_category = '" 					+ status_category			+ "'," +
				"status_idea = '" 						+ status_idea.toString()	+ ",  " +
				"enabled = " 	+ enabled.toString() 	+ " where id = " 			+ this.id.toString(); 
			
			//trace("update statement = " + sqls.text);
			
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
		}
		
		public function deleteVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "delete from status_idea where id = " + this.id.toString(); 
			
			//trace("update statement = " + sqls.text);
			
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
		}
		
		private function transactionError(evt:Event):void {
			this.dispatchEvent(new Event("TRANSACTION_FAILED", false));
		}
		
		private function transactionResult(evt:Event):void {
			this.dispatchEvent(new Event("TRANSACTION_SUCCESS", false));
		}
	}
}