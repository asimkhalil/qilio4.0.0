package VOs
{
	/* Getters, setters and XML Proxy are in the parent class */
	import Components.DBSocket;
	
	import Interfaces.iValueObject;
	
	import Models.AppStateVars;
	
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.net.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import mx.rpc.events.ResultEvent;
	
	public class GoogleNewsResultVO extends VOBaseCode implements iValueObject
	{
		/* VO Properties */
		[Bindable] public var id:int;
		[Bindable] public var title:String 			= "";
		[Bindable] public var link:String			= "";
		[Bindable] public var guid:String			= "";
		[Bindable] public var pubDate:String 		= "";
		[Bindable] public var description:String	= "";  

		public function GoogleNewsResultVO(){
			super();
		}
		
		public function saveVO():void {}
		
		public function getEntity():void{};
		
		public function dataHasBeenLoaded(pData:Array):void {}
		
		public function deleteVO():void {}
	}
}