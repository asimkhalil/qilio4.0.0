package VOs
{
	/* Getters, setters and XML Proxy are in the parent class */
	import Components.DBSocket;
	
	import Interfaces.iValueObject;
	
	import Models.AppStateVars;
	
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.net.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import mx.rpc.events.ResultEvent;
	
	public class UserInfoVO extends VOBaseCode implements iValueObject
	{
		/* VO Properties */
		[Bindable] public var id:int;
		[Bindable] public var facebook_login:String 	= "";
		[Bindable] public var facebook_password:String	= "";
		[Bindable] public var allow_auto_connect:Number	= 1;
		[Bindable] public var access_key:String 		= "";
		[Bindable] public var last_auto_post:String		= ""; // Will be depricated
		[Bindable] public var default_fan_page:String = "";
		[Bindable] public var show_feed_count:String = "0";
		[Bindable] public var show_welcome_video:String = "1";
		[Bindable] public var channels:String = "";
		
		private var sqls:SQLStatement = new SQLStatement();
		
		public function UserInfoVO(){
			super();
		}
		
		public function saveVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "INSERT INTO user_info (facebook_login, facebook_password, allow_auto_connect, show_feed_count, default_fan_page, access_key, show_welcome_video) VALUES('" + 
				facebook_login					+ "','" +
				facebook_password				+ "'," +
				allow_auto_connect.toString()	+ ",'" + 
				show_feed_count.toString()	+ ",'" + 
				default_fan_page.toString()	+ ",'" +
				access_key.toString()	+ ",'" + 
				show_welcome_video.toString()			+ "')"; 
				
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
	
		}
		
		public function getEntity():void{
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			var params:Array = new Array({field:"id", value:id.toString()});
			appStateVars.appDataBase.getEntities("user_info", params, "", 0, 0, false);
		};
		
		public function dataHasBeenLoaded(pData:Array):void {
			this.loadDataFromResult(pData[0], false);
			
			this.dispatchEvent(new Event("TRANSACTION_SUCCESS", false));			
		}
		
		public function updateVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "update user_info set " +  
 				"facebook_login = '" 			+ facebook_login				+ "'," +
				"facebook_password = '" 		+ facebook_password				+ "', " +
				"allow_auto_connect = " 		+ allow_auto_connect.toString() + ", " +
				"show_feed_count = '" 		    + show_feed_count.toString()     + "', " +
				"default_fan_page = '" 		    + default_fan_page.toString()   + "', " +
				"channels = '" 		    		+ channels   + "', " +
				"access_key = '" 				+ access_key 					+ "', " +
				"show_welcome_video = '" 		+ show_welcome_video 					+ "' " +
				" where id = " 					+ this.id.toString(); 
			
			trace("update statement = " + sqls.text);
			
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
		}
		
		public function deleteVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "delete from user_info where id = " + this.id.toString(); 
			
			//trace("update statement = " + sqls.text);
			
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
		}
		
		private function transactionError(evt:Event):void {
			this.dispatchEvent(new Event("TRANSACTION_FAILED", false));
		}
		
		private function transactionResult(evt:Event):void {
			this.dispatchEvent(new Event("TRANSACTION_SUCCESS", false));
		}
	}
}