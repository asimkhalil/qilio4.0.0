package VOs
{
	/* Getters, setters and XML Proxy are in the parent class */
	import Components.DBSocket;
	
	import Interfaces.iValueObject;
	
	import Models.AppStateVars;
	
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.net.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import mx.rpc.events.ResultEvent;
	
	public class PixaBayVO extends VOBaseCode implements iValueObject
	{
		/* VO Properties */
		[Bindable] public var id:int;
		[Bindable] public var previewHeight:String 	= "";
		[Bindable] public var previewURL:String		= "";
		[Bindable] public var tags:String			= "";
		[Bindable] public var thumbURL:String		= "";
		[Bindable] public var pageURL:String		= "";
		[Bindable] public var webformatURL:String	= "";
		[Bindable] public var type:String			= "";
		[Bindable] public var user:String			= "";		
		
		[Bindable] public var webformatHeight:Number	= 0;
		[Bindable] public var webformatWidth:Number		= 0;
		[Bindable] public var imageWidth:Number			= 0;
		[Bindable] public var model_release:Number		= 0;
		[Bindable] public var previewWidth:Number		= 0;
	
		
		
		public function PixaBayVO(){
			super();
		}
		
		public function saveVO():void {}
		
		public function getEntity():void{};
		
		public function dataHasBeenLoaded(pData:Array):void {}
		
		public function deleteVO():void {}
		
	
	}
}