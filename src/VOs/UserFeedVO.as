package VOs
{
	/* Getters, setters and XML Proxy are in the parent class */
	import Components.DBSocket;
	
	import Interfaces.iValueObject;
	
	import Models.AppStateVars;
	
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.net.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.xml.XMLDocument;
	
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.graphics.shaderClasses.ExclusionShader;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.xml.SimpleXMLDecoder;
	import mx.utils.ArrayUtil;
	import mx.utils.StringUtil;
	
	public class UserFeedVO extends VOBaseCode implements iValueObject
	{
		/* VO Properties */
		[Bindable] public var id:int = 0;
		[Bindable] public var fan_page_id:String;
		[Bindable] public var title:String				= "";
		[Bindable] public var link:String				= ""; 
		[Bindable] public var description:String 		= "";
		[Bindable] public var category:String 			= "";
		[Bindable] public var resultsArray:Array;
		
		 
		
		private var sqls:SQLStatement 		= new SQLStatement();
		private var testLoader:URLLoader  = new URLLoader();
		
		public function UserFeedVO(){
			super();
		}
		
		public function initVO(
							   pFanPageId:String,
							   pTitle:String,
							   pLink:String,
							   pDescription:String):void {
			
			fan_page_id		= pFanPageId;
			title			= pTitle;
			link 			= pLink;
			description		= pDescription;
			 
		}
		
		public function saveVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "INSERT INTO user_feeds (	fan_page_id 		" +
				"								,	title " +
				"								,	link" +
				"								,	description) 		" +
				" VALUES(" 												  +
												"'" + fan_page_id  	  +
												"','" + title 			  +
												"','" + link         	  +
												"','" + description  	  + "')"; 
				
			//trace("update statement = " + sqls.text);
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
	
		}
		
		public function getEntity():void{
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			var params:Array = new Array({field:"id", value:id.toString()});
			appStateVars.appDataBase.getEntities("user_feeds", params, "", 0, 0, false);
		};
		
		public function dataHasBeenLoaded(pData:Array):void {
			this.loadDataFromResult(pData[0], false);
			
			this.dispatchEvent(new Event("TRANSACTION_SUCCESS", false));			
		}
		
		public function updateVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = 
				"update user_feeds set  " +  
				"	fan_page_id 	 =	'" + fan_page_id +
				"',	title 			 =	'" + title 				+
				"',	link	     	 =	'" + link				+
				"',	description	     =	'" + description		+
				"'  where id = " 					+ this.id.toString(); 
			
			//trace("update statement = " + sqls.text);
			
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
		}
		
		public function deleteVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "delete from user_feeds where id = " + this.id.toString(); 
			
			//trace("update statement = " + sqls.text);
			
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
		}
		
		private function transactionError(evt:Event):void {
			this.dispatchEvent(new Event("TRANSACTION_FAILED", false));
			//trace("Saved VO Failed");
		}
		
		private function transactionResult(evt:Event):void {
			this.dispatchEvent(new Event("TRANSACTION_SUCCESS", false));
		 	//trace("Saved VO Success");
		}
		
		public function loadFeedFromSource():void {
			var urlAddress:String = this.link;
			
			testLoader.addEventListener(Event.COMPLETE, receivedFeedResults);
			testLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			//trace("Going to load feed from " + link);
			testLoader.load(new URLRequest(urlAddress)); 
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void {
			
			this.dispatchEvent(new Event("FEED_LOADED_FAILED", false));
		}
		
		public function receivedFeedResults(e:Event):void	{
			try {
			
				var xml_doc:XMLDocument = new XMLDocument(StringUtil.trimArrayElements(e.target.data, ">"));
				var xml_dec:SimpleXMLDecoder = new SimpleXMLDecoder();
				var data:Object = xml_dec.decodeXML(xml_doc);
				if(data.rss) 
				{
					resultsArray = ArrayUtil.toArray(data.rss.channel.item);
				}
				else 
				{
					resultsArray = ArrayUtil.toArray(data.feed.entry);
				}
				this.dispatchEvent(new Event("FEED_LOADED"));
				if((resultsArray[0].hasOwnProperty("description") || resultsArray[0].hasOwnProperty("content") || resultsArray[0].hasOwnProperty("summary")) && 
					resultsArray[0].hasOwnProperty("link") && resultsArray[0].hasOwnProperty("title"))
				{
					
				}
				else 
				{
					Alert.show("Incorrect feed, no main fields present");	
				}
				
			} catch(err:Error) {
				Alert.show("Incorrect feed");
				FlexGlobals.topLevelApplication.ImportEngine.controller.postView.pgrFeedLoad.visible = false;
			}
		}
	}
}