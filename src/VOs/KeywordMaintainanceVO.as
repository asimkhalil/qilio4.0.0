package VOs
{
	/* Getters, setters and XML Proxy are in the parent class */
	import Components.DBSocket;
	
	import Interfaces.iValueObject;
	
	import Models.AppStateVars;
	
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.net.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import mx.collections.ArrayCollection;
	import mx.formatters.DateFormatter;
	import mx.rpc.events.ResultEvent;
	
	public class KeywordMaintainanceVO extends VOBaseCode implements iValueObject
	{
		/* VO Properties */
		[Bindable] public var id:int;
		[Bindable] public var fan_page_id:String 			= "";
		[Bindable] public var keywords:String			= "";
		[Bindable] public var hash_tags:String = "";
		
		// Facebook API fields for scheduledPost
		/*[Bindable] public var attachment:Object;
		[Bindable] public var created_time:int;
		[Bindable] public var description:String		= "";
		[Bindable] public var is_published:int;
		[Bindable] public var message:String		= "";
		[Bindable] public var post_id:String		= "";
		[Bindable] public var scheduled_publish_time:int	;
		[Bindable] public var source_id:int;
		[Bindable] public var type:int;
		[Bindable] public var updated_time:int;
		
		[Bindable] public var pageAccessToken:String;
		[Bindable] public var pageId:String;
		[Bindable] public var permalink:String;*/
		
		private var sqls:SQLStatement = new SQLStatement();
		
		public function KeywordMaintainanceVO()
		{
			super();
			//setServicesParameters("events","id","process_event");
		}
		
		public function saveVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "INSERT INTO keyword_maintainance (fan_page_id, keywords, hash_tags) VALUES('" + 
				//+first_name.text+"','"+last_name.text+"');";
				fan_page_id	+ "','" +
				keywords	+ "','" +
				hash_tags			+ "');"; 
			
			//trace("insert statement = " + sqls.text);
				
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
	
		}
		
		public function getEntity():void{
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			/*var params:Array = new Array({field:"id", value:id.toString()});*/
			/*appStateVars.appDataBase.getEntities("keyword_maintainance", params, "", 0, 0, false);*/
			appStateVars.appDataBase.getKeywords();
		};
		
		/*public function getResultCount():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			
			sqls.text = "select * from scheduled_posts order by scheduled_date"; 
			
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, onSchedulePostCountResult);	
			sqls.execute();
		}*/
		
		/*private function filterFunc(item:Object):Boolean {
			var currentDate:Date = new Date();
			if(item.scheduled_date > currentDate)
				return true;
			return false;
		}*/
		
		/*private function onSchedulePostCountResult(evt:Event):void {
			var result:SQLResult = sqls.getResult();
			var col:ArrayCollection = new ArrayCollection(result.data);
			col.filterFunction = filterFunc;
			col.refresh();
			if(col && col.length > 0) {
				var appStateVars:AppStateVars = AppStateVars.getInstance();
				appStateVars.scheduledPostsCount = col.length;
				var lastResult:Object = col.getItemAt(0);
				var df:DateFormatter = new DateFormatter();
				df.formatString = "MM/DD/YYYY JJ:NN A";
				appStateVars.nextScheduledPostTime = df.format(lastResult.scheduled_date);
			}
		}*/
		
		public function dataHasBeenLoaded(pData:Array):void {
			this.dispatchEvent(new Event("TRANSACTION_SUCCESS", false));			
		}
		
		public function updateVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "update keyword_maintainance set " + //(post_name, post_type, scheduled_date, status, entered_date, content_text, content_link_url, video_url) VALUES('" + 
				"keywords = '"				+ keywords		+ "', " +
				"hash_tags = '"						+ hash_tags				+ "' where id = " + this.id.toString(); 
			
			//trace("update statement = " + sqls.text);
			
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
		}
		
		public function deleteVO():void {
			var appStateVars:AppStateVars = AppStateVars.getInstance();
			appStateVars.appDataBase = new DBSocket(this);
			appStateVars.appDataBase.openDBConnection();
			
			sqls.sqlConnection = appStateVars.appDataBase.conn;
			sqls.text = "delete from keyword_maintainance where fan_page_id = '" + this.fan_page_id.toString()+"'"; 
			
			//trace("update statement = " + sqls.text);
			
			sqls.addEventListener(SQLErrorEvent.ERROR, transactionError);
			sqls.addEventListener(SQLEvent.RESULT, transactionResult);	
			sqls.execute();
		}
		
		private function transactionError(evt:Event):void {
			this.dispatchEvent(new Event("TRANSACTION_FAILED", false));
		}
		
		private function transactionResult(evt:Event):void {
			this.dispatchEvent(new Event("TRANSACTION_SUCCESS", false));
		}
	}
}