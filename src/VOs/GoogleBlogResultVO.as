package VOs
{
	/* Getters, setters and XML Proxy are in the parent class */
	import Components.DBSocket;
	
	import Interfaces.iValueObject;
	
	import Models.AppStateVars;
	
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.net.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import mx.rpc.events.ResultEvent;
	
	public class GoogleBlogResultVO extends VOBaseCode implements iValueObject
	{
		/* VO Properties */
		[Bindable] public var id:int;
		[Bindable] public var blogUrl:String 				= "";
		[Bindable] public var GsearchResultClass:String		= "";
		[Bindable] public var author:String					= "";
		[Bindable] public var publishedDate:String 			= "";
		[Bindable] public var titleNoFormatting:String		= "";  
		[Bindable] public var content:String				= "";  
		[Bindable] public var postUrl:String				= "";  
		[Bindable] public var title:String					= "";  

		public function GoogleBlogResultVO(){
			super();
		}
		
		public function saveVO():void {}
		
		public function getEntity():void{};
		
		public function dataHasBeenLoaded(pData:Array):void {}
		
		public function deleteVO():void {}
	}
}