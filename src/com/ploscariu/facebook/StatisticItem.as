/**
 * Created with IntelliJ IDEA.
 * User: simi
 * Date: 11/13/13
 * Time: 4:21 PM
 * To change this template use File | Settings | File Templates.
 */
package com.ploscariu.facebook {
public class StatisticItem {
    public var name:String;
    public var title:String;
    public var valueArray:Array;
    public var value:Number=0;
    public var period:String;
    private var id:String;
    public function StatisticItem(o:Object) {
		if(o) {
	        this.name=o["name"];
	        this.title=o["title"];
	        this.valueArray=o["values"];
	        this.period=o["period"];
	        this.id=o["id"];
	        if(valueArray&&valueArray.length>0)
	            value=(valueArray[0].value);
		} else {
			value = 0;
		}
    }
    public static function fromObject(o:*,...rest):StatisticItem{
        return new StatisticItem(o);
    }

    public function toString():String {
        var res:String=(value>=0)?value.toString():"No Data";
        return res;
    }
}
}
