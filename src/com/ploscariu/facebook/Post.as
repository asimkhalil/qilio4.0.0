/**
 * Created with IntelliJ IDEA.
 * User: simi
 * Date: 11/14/13
 * Time: 11:10 AM
 * To change this template use File | Settings | File Templates.
 */
package com.ploscariu.facebook {
import Models.AppStateVars;

import flash.globalization.DateTimeFormatter;

public class Post {
    private var _id:String;
    public var type:String;
    public var status_type:String;

    [Bindable]
    public var scheduled_publish_time:String;
	public var scheduled_published_date_object:Date = new Date();
    public var created_time:String;
    private var updated_time:String;
    private var name:String;
    public var message:String;
    private var picture:String;
    private var icon:String;
    private var permalink:String;
    public var likes:int;
    public var shares:int;
	public var comments:int;
    private var source:String;
    private var dynamicObject:*;
    private var _page:Page;
	public var link:String;
	public var from:Object;
	
    private var postStatistics:PostStatistics;
    [Bindable]
    public var post_engaged_users:StatisticItem;
    [Bindable]
    public var post_impressions_unique:StatisticItem;
    [Bindable]
    public var post_stories:String;
    [Bindable]
    public var post_storytellers:String;
    [Bindable]
    public var post_negative_feedback_unique:String;
	
	public var color:uint = 0xffffff;
	
	public var engagement_percentage:Number = 0;

    public function getPermalink():String{
		if(this.permalink) {
			return this.permalink;	
		}
		return "http://facebook.com/"+AppStateVars.getInstance().facebookUserProfileId+"/posts/"+this.id;
    }
    public function getId():String{
        return this._id;
    }
    /*    11 - Group created
     12 - Event created
     46 - Status update
     56 - Post on wall from another user
     66 - Note created
     80 - Link posted
     128 -Video posted
     247 - Photos posted
     237 - App story
     257 - Comment created
     272 - App story
     285 - Checkin to a place
     308 - Post in Group

     */
    public function Post(o:*) {
        this.dynamicObject = o;
        this._id = o.id? o.id: o.post_id;
        this.name = o.name;
        this.icon = o.icon;
        this.permalink = o.permalink? o.permalink:o.link;
        this.message = o.message;
        this.picture = o.picture;
        this.source = o.source;
        this.type = o.type;
        this.created_time =formatDate( o.created_time);
		this.from = o.from;
        if(o.updated_time)
            this.updated_time = o.updated_time;
        if(o.scheduled_publish_time) {
            this.scheduled_publish_time=formatDate(o.scheduled_publish_time);
			this.scheduled_published_date_object = toDateObject(o.scheduled_publish_time);
		}
        this.status_type = o.status_type;
        this.likes = o.likes ? o.likes.data.length : (o.like_info?o.like_info.like_count:0);
        this.shares= o.shares? o.shares.count: o.share_count;
		this.comments= o.comments? o.comments.count: o.comments_count;
    }

    private function formatDate(dateStr:String):String {
        if(dateStr.indexOf('1')==0){
            var dtf:DateTimeFormatter = new DateTimeFormatter("en-US");
            dtf.setDateTimePattern("yyyy-MM-dd hh:mm");
            var date:Date=new Date(parseFloat(dateStr+"000"));
            return (dtf.format(date));
        }

        //"2013-09-22T19:04:07+0000"
        var res:String=dateStr.replace("T"," ");
        res=res.substr(0,16);
        return res;
    }
	
	private function toDateObject(dateStr:String):Date {
		var date:Date;
		if(dateStr.indexOf('1')==0){
			var dtf:DateTimeFormatter = new DateTimeFormatter("en-US");
			dtf.setDateTimePattern("yyyy-MM-dd hh:mm");
			date=new Date(parseFloat(dateStr+"000"));
		}
		return date;
	}

    public static function fromObject(o:*, ...rest):Post {
        return new Post(o);
    }

    public function setPostStatistics(ps:PostStatistics):void {
        this.postStatistics = ps;
        this.post_engaged_users=ps.post_engaged_users;
        this.post_impressions_unique=ps.post_impressions_unique;
        this.post_negative_feedback_unique=ps.post_negative_feedback_unique;
        this.post_stories=ps.post_stories;
        this.post_storytellers=ps.post_storytellers;
		
		var percentageMetric:Number = (post_engaged_users.value/post_impressions_unique.value)*100;
		if(isNaN(percentageMetric)) {
			percentageMetric = 0;
		}
		engagement_percentage = percentageMetric;
		if(percentageMetric > 5) {
			color = 0xC8E6D1;
		} 
		
		if(percentageMetric > 10) {
			color = 0x9DE6A9;
		}
		
		if(percentageMetric < 2.5) {
			color = 0xE6C9C8;
		} 
		
		if(percentageMetric < 1) {
			color = 0xE6B8B8;
		}
    }

    public function get page():Page {
        return _page;
    }

    public function set page(value:Page):void {
        _page = value;
    }

    public function get id():String {
        return _id;
    }

    public function setScheduledTime(date:Date):void {
        var dtf:DateTimeFormatter = new DateTimeFormatter("en-US");
        dtf.setDateTimePattern("yyyy-MM-dd hh:mm");
        scheduled_publish_time= (dtf.format(date));
    }
}
}
