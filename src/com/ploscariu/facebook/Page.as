/**
 * Created with IntelliJ IDEA.
 * User: simi
 * Date: 11/14/13
 * Time: 10:39 AM
 * To change this template use File | Settings | File Templates.
 */
package com.ploscariu.facebook {
import flash.utils.ByteArray;

import mx.collections.ArrayCollection;

public class Page {
    public var id:String;
    public var name:String;
    public var type:String;
    public var access_token:String;
    public var permissions:Array;
	public var thumb:ByteArray;
	public var link:String;
    [Bindable]
    public var posts:ArrayCollection;
    public function Page(o:*) {
        this.id= o.id;
        this.name= o.name;
        this.permissions= o.perms;
        this.access_token= o.access_token;
        this.type= o.category;
    }
    public static function fromObject(o:Object,...rest):Page{
        return new Page(o);
    }

    public function toString():String {
        return name;
    }
}
}
