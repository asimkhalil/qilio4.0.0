/**
 * Created with IntelliJ IDEA.
 * User: simi
 * Date: 11/13/13
 * Time: 4:15 PM
 * To change this template use File | Settings | File Templates.
 */
package com.ploscariu.facebook {
public class PostStatistics {
    public var post_engaged_users:StatisticItem;
    public var post_impressions_unique:StatisticItem;
    public var post_stories:String = "0";
    public var post_storytellers:String = "0";
    public var post_negative_feedback_unique:String = "0";
    public function PostStatistics() {

    }
    public function loadStats():void{
        loadInsights();
        getLikesForPost();
    }

    private function loadInsights():void {

    }
    private function getLikesForPost():void{

    }

    public static  function fromArrayOfResults(data:Array):PostStatistics{
        var res:PostStatistics=new PostStatistics();
		var si:StatisticItem;
        for each (var o:Object in data) {
            if(!(o is StatisticItem))
                si=StatisticItem.fromObject(o);
            else
                si=o as StatisticItem;
            if(res.hasOwnProperty(si.name))
            res[si.name]=si;
        }
		if(!o) {
			res.post_engaged_users = new StatisticItem(null);
			res.post_impressions_unique =  new StatisticItem(null);
		}
        return res;
    }
}
}
