package com.imagemod
{
	import flash.display.DisplayObject;
	
	import mx.core.IVisualElement;
	
	import spark.components.BorderContainer;
	
	public class BCBorderContainer extends BorderContainer
	{
		public function BCBorderContainer()
		{
			super();
		}
		
		public override function addChild(child:DisplayObject):DisplayObject {
			return addElement(child as IVisualElement) as DisplayObject;
		}
		
		public override function addChildAt(child:DisplayObject, index:int):DisplayObject {
			return addElementAt(child as IVisualElement, index) as DisplayObject;
		}
		
		public override function setChildIndex(child:DisplayObject, index:int):void {
			setElementIndex(child as IVisualElement, index);
		}
		
		public override function removeChild(child:DisplayObject):DisplayObject {
			return removeElement(child as IVisualElement) as DisplayObject;
		}
		
		public override function removeChildAt(index:int):DisplayObject {
			return removeElementAt(index) as DisplayObject;
		}
		
		public override function removeChildren(beginIndex:int=0, endIndex:int=int.MAX_VALUE):void {
			for(var i:int=beginIndex; i<Math.min(endIndex, this.numElements); i++) {
				removeElementAt(i);
			}
		}
		
		/*public override function get numChildren():int {
			return numElements;
		}*/
	}
}