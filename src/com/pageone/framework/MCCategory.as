package com.pageone.framework
{
	import flash.utils.ByteArray;
	
	import mx.utils.StringUtil;

	public class MCCategory
	{
		public function MCCategory()
		{
		}
		
		public var name:String;
		public var description:String;
		public var url:String;
		public var id:int;
		
		public function toXML():String {
			var s:String=StringUtil.substitute("<cat name=\"{0}\" id=\"{1}\">", this.name, this.id);
			s+=StringUtil.substitute("<desc>{0}</desc>", escape(this.description));
			s+=StringUtil.substitute("<url>{0}</url>", escape(this.url));
			s+="</cat>";
			return s;
		}
		
		public static function readXML(x:XML):MCCategory {
			var c:MCCategory=new MCCategory();
			c.name=x.@name;
			c.id=x.@id;
			c.description=unescape(x.desc);
			c.url=unescape(x.url);
			return c;
		}
	}
}