package com.pageone.flickr {
	import com.pageone.flickr.events.FlickrCallCompleteEvent;
	import com.pageone.flickr.events.FlickrCallFailedEvent;
	import com.pageone.flickr.frame.FlickrResult;
	import com.pranav.util.DateRange;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.globalization.DateTimeStyle;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DateChooser;
	import mx.formatters.DateFormatter;
	import mx.utils.URLUtil;
	
	import spark.formatters.DateTimeFormatter;


	[Event(name="flickrCallComplete", type="com.pageone.flickr.events.FlickrCallCompleteEvent")]
	[Event(name="flickrCallFailed", type="com.pageone.flickr.events.FlickrCallFailedEvent")]
	public class FlickrAPIHandle extends EventDispatcher {
		
		private static const flickrKey:String="d4f0c26ec707d4661b570a8cef7c8245";
		
		private var urlExtras:Array=["url_n", "url_z", "url_q"];
		
		public function FlickrAPIHandle() {
			apiParams={};
			apiParams["method"]="flickr.photos.search";
			apiParams["api_key"]=flickrKey;
			apiParams["format"]="rest";
			apiParams["extras"]="owner_name,license" + urlExtras.join(",");
			apiParams["sort"]="interestingness-desc";


			apiParams["is_commons"]="true";
			apiParams["media"]="photos";

		/*
		   apiParams["min_taken_date"]="mm/dd/yyyy";
		   apiParams["max_taken_date"]="mm/dd/yyyy";
		 */

		/* Safe search setting:

		   1 for safe.
		   2 for moderate.
		   3 for restricted.*/
			   //apiParams["safe_search"]="1|2|3";

			   //apiParams["media"]="all|photos|videos";

			   //apiParams["text"]="text_to_search";

		/*<licenses>
		   <license id="4" name="Attribution License" url="http://creativecommons.org/licenses/by/2.0/" />
		   <license id="6" name="Attribution-NoDerivs License" url="http://creativecommons.org/licenses/by-nd/2.0/" />
		   <license id="3" name="Attribution-NonCommercial-NoDerivs License" url="http://creativecommons.org/licenses/by-nc-nd/2.0/" />
		   <license id="2" name="Attribution-NonCommercial License" url="http://creativecommons.org/licenses/by-nc/2.0/" />
		   <license id="1" name="Attribution-NonCommercial-ShareAlike License" url="http://creativecommons.org/licenses/by-nc-sa/2.0/" />
		   <license id="5" name="Attribution-ShareAlike License" url="http://creativecommons.org/licenses/by-sa/2.0/" />
		   <license id="7" name="No known copyright restrictions" url="http://flickr.com/commons/usage/" />
		   </licenses>*/
			//apiParams["license"]="1|2|3|4|5|6|7";
			apiParams["license"]="7";


		/*Content Type setting:

		   1 for photos only.
		   2 for screenshots only.
		   3 for 'other' only.
		   4 for photos and screenshots.
		   5 for screenshots and 'other'.
		   6 for photos and 'other'.
		   7 for photos, screenshots, and 'other' (all).*/
			//apiParams["content_type"]="1|2|3|4|5|6|7";
		}

		//http://api.flickr.com/services/rest?method=flickr.photos.search&api_key=d4f0c26ec707d4661b570a8cef7c8245&text=football&is_commons=true

		private const apiLocation:String="http://api.flickr.com/services/rest";

		private var apiParams:Object;

		private var _nextCallPage:Number=1;
		private var _maxPages:Number=1;
		private var _currentPage:Number=1;

		private var _lastSearch:String="";

		private var loader:URLLoader;

		private var _data:ArrayCollection;

		private var _busy:Boolean;

		public function get maxPages():Number {
			return _maxPages;
		}
		
		public function get page():Number {
			return _currentPage;
		}
		
		public function get data():ArrayCollection {
			return _data;
		}

		public function get busy():Boolean {
			return _busy;
		}

		public function set contentType(value:Number):void {
			if (value == -1) {
				delete apiParams["content_type"];
			} else {
				apiParams["content_type"]=value.toString();
			}
		}

		public function set dateRange(value:DateRange):void {
			if (value == null) {
				delete apiParams["min_taken_date"];
				delete apiParams["max_taken_date"];
			} else {
				var d:DateFormatter=new DateFormatter();
				d.formatString="MM/DD/YYYY";
				if (value.startDate == null) {
					delete apiParams["min_taken_date"];
				} else {
					apiParams["min_taken_date"]=d.format(value.startDate);
				}

				if (value.endDate == null) {
					delete apiParams["max_taken_date"];
				} else {
					apiParams["max_taken_date"]=d.format(value.endDate);
				}
			}
		}

		public function set license(value:String):void {
			if (value == "") {
				delete apiParams["license"];
			} else {
				apiParams["license"]=value;
			}
		}

		public function set safeSearch(value:Number):void {
			if (value == -1) {
				delete apiParams["safe_search"];
			} else {
				apiParams["safe_search"]=value.toString();
			}
		}

		public function set isCommons(value:Number):void {
			if (value < 1) {
				delete apiParams["is_commons"];
			} else {
				apiParams["is_commons"]=(value == 1 ? "true" : "false");
			}
		}

		public function search(query:String):void {
			apiParams["text"]=query.split(" ").join("+");
			apiParams["page"]=_nextCallPage;
			_busy=true;
			_lastSearch=apiParams["text"];
			loader=new URLLoader();
			loader.dataFormat=URLLoaderDataFormat.TEXT;
			var fUrl:String=apiLocation + "?" + (URLUtil.objectToString(apiParams, "&", true)).split("%2B").join("+");
			var request:URLRequest=new URLRequest(fUrl);
			request.method=URLRequestMethod.GET;
			//request.data=apiParams;

			loader.addEventListener(Event.COMPLETE, dataLoaded);
			loader.addEventListener(IOErrorEvent.IO_ERROR, loadFailed);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, loadFailed);

			loader.load(request);
		}

		public function nextPage():void {
			_nextCallPage++;
			if (_nextCallPage > _maxPages) {
				_nextCallPage=0;
			}
			search(_lastSearch);
		}

		public function prevPage():void {
			_nextCallPage--;
			if (_nextCallPage <= 0) {
				_nextCallPage=_maxPages;
			}
			search(_lastSearch);
		}

		private function dataLoaded(e:Event):void {
			var evt:FlickrCallCompleteEvent=new FlickrCallCompleteEvent(FlickrCallCompleteEvent.flickrCallComplete);
			_busy=false;
			loader.removeEventListener(Event.COMPLETE, dataLoaded);
			_data=new ArrayCollection();
			var x:XML=new XML(loader.data);
			var s:String=(x.@stat);
			if (s == "ok") {
				var photos:XMLList=x.photos.photo;
				_maxPages=x.photos.@pages;
				_currentPage=x.photos.@page;

				for each (var photo:XML in photos) {
					var r:FlickrResult=new FlickrResult();
					r.farm=photo.@farm;
					r.id=photo.@id;
					r.title=photo.@title;
					r.owner=photo.@owner;
					r.secret=photo.@secret;
					r.server=photo.@server;
					r.licenseId=photo.@license;
					r.owner_name=photo.@ownername;
					
					for each(var key:String in urlExtras) {
						r[key]=String(photo["@" + key]);
					}
					
					_data.addItem(r);
				}
			}
			dispatchEvent(evt);
		}

		private function loadFailed(e:Event):void {
			var evt:FlickrCallFailedEvent=new FlickrCallFailedEvent(FlickrCallFailedEvent.flickrCallFailed);
			if (e is IOErrorEvent) {
				evt.reason=(e as IOErrorEvent).text;
			} else if (e is SecurityErrorEvent) {
				evt.reason=(e as SecurityErrorEvent).text;
			}
			dispatchEvent(evt);
		}

		public static const licenses:XML=<licenses>
				<license id="7" name="No known copyright restrictions" url="http://flickr.com/commons/usage/" />
				<license id="4" name="Attribution License" url="http://creativecommons.org/licenses/by/2.0/" />
				<license id="6" name="Attribution-NoDerivs License" url="http://creativecommons.org/licenses/by-nd/2.0/" />
				<license id="3" name="Attribution-NonCommercial-NoDerivs License" url="http://creativecommons.org/licenses/by-nc-nd/2.0/" />
				<license id="2" name="Attribution-NonCommercial License" url="http://creativecommons.org/licenses/by-nc/2.0/" />
				<license id="1" name="Attribution-NonCommercial-ShareAlike License" url="http://creativecommons.org/licenses/by-nc-sa/2.0/" />
				<license id="5" name="Attribution-ShareAlike License" url="http://creativecommons.org/licenses/by-sa/2.0/" />
			</licenses>;

		public static const contentTypes:XML=<content-types>
				<content-type id="1" name="Photos only" />
				<content-type id="2" name="Screenshots only" />
				<content-type id="3" name="'Other' only" />
				<content-type id="4" name="Photos and Screenshots" />
				<content-type id="5" name="Screenshots and 'Other'" />
				<content-type id="6" name="Photos and 'Other'" />
				<content-type id="7" name="Everything" />
			</content-types>;

		public static const safeSearchTypes:XML=<safe-search-types>
				<safe-search id="1" name="safe" />
				<safe-search id="2" name="moderate" />
				<safe-search id="3" name="restricted" />
			</safe-search-types>;

	}
}
