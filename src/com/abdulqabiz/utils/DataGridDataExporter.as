/**
	_________________________________________________________________________________________________________________

	DataGridDataExporter is a util-class to export DataGrid's data into different format.	
	@class DataGridDataExporter (public)
	@author Abdul Qabiz (mail at abdulqabiz dot com) 
	@version 0.01 (2/8/2007)
	@availability 9.0+
	@usage<code>DataGridDataExporter.<staticMethod> (dataGridReference)</code>
	@example
		<code>
			var csvData:String = DataGridDataExporter.exportCSV (dg);
		</code>
	__________________________________________________________________________________________________________________

	*/
package com.abdulqabiz.utils
{
	import mx.collections.ArrayCollection;
	import mx.collections.CursorBookmark;
	import mx.collections.IList;
	import mx.collections.IViewCursor;
	import mx.collections.XMLListCollection;
	import mx.controls.DataGrid;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.controls.dataGridClasses.DataGridColumn;

	public class DataGridDataExporter
	{

		private static function formData(label:String):String {
			if(label != "") {
				return label;
			} else {
				return "";
			}
		}
		
		public static function exportCSV(dataProvider:ArrayCollection, columns:Array, csvSeparator:String=",", lineSeparator:String="\n"):String
		{
			var data:String = "";
			var columnCount:int = columns.length;
			var column:AdvancedDataGridColumn;
			var header:String = "";
			var headerGenerated:Boolean = false;
			var dataProvider:ArrayCollection = dataProvider;

			var rowCount:int = dataProvider.length;
			var dp:Object = null;

		
			var cursor:IViewCursor = dataProvider.createCursor ();
			var j:int = 0;
			
			//loop through rows
			while (!cursor.afterLast)
			{
				var obj:Object = null;
				obj = cursor.current;
				
				//loop through all columns for the row
				for(var k:int = 0; k < columnCount; k++)
				{
					column = columns[k];

					//Exclude column data which is invisible (hidden)
					if(!column.visible)
					{
						continue;
					}
				
					var columnData:String = ""+formData(column.itemToLabel(obj));
					
					if(columnData.length > 50 ) {
						columnData = columnData.substring(0,49)+"...";
					}
					
					data += "\""+ columnData+ "\"";
					
					if(k < (columnCount -1))
					{
						data += csvSeparator;
					}

					//generate header of CSV, only if it's not genereted yet
					if (!headerGenerated)
					{
						header += "\"" + column.headerText + "\"";
						if (k < columnCount - 1)
						{
							header += csvSeparator;
						}
					}
					
				
				}
				
				headerGenerated = true;

				if (j < (rowCount - 1))
				{
					data += lineSeparator;
				}

				j++;
				cursor.moveNext ();
			}
			
			//set references to null:
			dataProvider = null;
			columns = null;
			column = null;

			
			return (header + "\r\n" + data);
		}	
	}

}
