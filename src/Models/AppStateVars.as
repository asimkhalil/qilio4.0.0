package Models
{
	
	import Components.DBSocket;
	import Components.FacebookCenter;
	
	import Views.*;
	
	import mx.collections.ArrayCollection;
	import mx.core.IFlexDisplayObject;
	import mx.events.*;
	import mx.validators.*;
	
	public class AppStateVars {
		
	  include "Constants.as";
	  
	  public var welcomeshown:Boolean = false;
	  
	  public var channelsCollectionDirty:Boolean = false;
	  
	  public var defaultFanPageId:String = "";
	  
	  [Bindable]
	  public var showWelcomeVideo:String = "1";
	  
	  [Bindable]
	  public var showFeedsCount:Boolean = false;
	  
	  [Bindable]
	  public var scheduledPostsCount:int;
	  
	  [Bindable]
	  public var nextScheduledPostTime:String;
	  
	  [Bindable]
	  public var viewSelected:int = 0;
	  
	  public static var appVersion:Number = 1.5;
	  
	  [Bindable]
	  public var fanPageSelectedIndex:int = 0; 
	  
	  [Bindable]
	  public var selectedFanPageId:String = "";
	  
	  [Bindable]
	  public var keywordsCollection:ArrayCollection = new ArrayCollection;
	  
	  [Bindable]
	  public var userProfilePhoto:String = "";
	  
	  [Bindable]
	  public var facebookUserName:String = "";
	  
	  public var facebookUserProfileId:String = "";
	  
	  public var selectedMultiFanPagesToPost:ArrayCollection = new ArrayCollection();
	  
	  private static var instance:AppStateVars;
	  private static var allowInstantiation:Boolean;
	  //private static var formObject:IFlexDisplayObject;
	  
	  [Bindable] public var currentSessionId:String;
	  public var appDataBase:DBSocket;
	  [Bindable] public var isDBReady:Boolean = true;
	  [Bindable] public var facebookCOM:FacebookCenter;
	  
	  [Bindable] public var enableIndicators:Boolean = true;
	 
	  /* Constructor  */
	  public function AppStateVars():void {
		  if (!allowInstantiation) {
	           throw new Error("Error: Instantiation failed: Use ServerCall.getInstance() instead of new.");
	       }
	  }
	  
	  /* General getter for AppStateVars */
	  public function getProperty(propName:String):* {
	  		return this[propName];
	  }

	  /* General setter for AppStateVars */
	  public function setProperty(propName:String, value:*, dispatch:Boolean):void {
	  	 if (this.hasOwnProperty(propName)) {
			this[propName] = null;
			this[propName] = value;
			
			if (dispatch) dispatchEvent(new Event(propName)); 
		 }
	  }
		
	 /* Get AppStateVars singleton */	
	  public static function getInstance():AppStateVars {
         if (instance == null) {
            allowInstantiation = true;
            instance = new AppStateVars();
            allowInstantiation = false;
		  }
         return instance;
       }
	}
}
