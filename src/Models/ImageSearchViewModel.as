package Models
{
	import Components.GoogleSearchCenter;
	import Components.MorgueFileCenter;
	import Components.PixaBayCenter;
	
	import Controllers.*;
	
	import Interfaces.iController;
	import Interfaces.iModel;
	
	import VOs.*;
	
	import flash.events.*;
	import flash.net.*;
	import flash.utils.getQualifiedClassName;
	
	import mx.rpc.events.ResultEvent;
	
	public class ImageSearchViewModel extends BaseModel implements iModel
	{ 
 		public var mainObject:Object;
		public const ELEMENTS_PER_ROW_SIZE:int = 3;		
		[Bindable] public var pageNum:int = 1;
		[Bindable] public var source:int = 0;
		[Bindable] public var totalResults:int = 0;
		public const CUSTOM_PAGE_SIZE:int = 3;		
		[Bindable] public var searchWords:String = "";
		[Bindable] public var searchSource:String = "";
		[Bindable] public var searchResults:Array;
		[Bindable] public var pixaBayCom:PixaBayCenter;
		[Bindable] public var morgueFileCom:MorgueFileCenter;
		
		[Bindable] public var resultSetPaged:Array;
		
		/* Constructor */
		public function ImageSearchViewModel(myController:*) {
			super(myController);
			init();
		}		
		
		public function init():void {
			pixaBayCom = new PixaBayCenter();
			pixaBayCom.addEventListener("PIXABAY_SEARCH_DATA_READY", finishedSearch);
			morgueFileCom = new MorgueFileCenter();
			morgueFileCom.addEventListener("MORGUEFILE_SEARCH_DATA_READY",morgueFileFinishedSearch);
		}
		
		public function performSearch(pSource:String, pSearchWords:String,source:int):void {
	
			searchWords = pSearchWords != "" ? pSearchWords : searchWords;
			searchSource= pSource != "" ? pSource : searchSource;
			
			this.source = source;
			
			if(source == 0) {	
				pixaBayCom.pageNum = this.pageNum;
				pixaBayCom.searchImages(pSearchWords, true,this.pageNum);
			} else {
				morgueFileCom.pageNum = this.pageNum;
				morgueFileCom.searchImages(pSearchWords,true,this.pageNum);
			}
		}
		
		public function resetSearch():void {
			pageNum = 1;
			totalResults = 0;
			source = 0;
			dispatchEvent(new Event("REFRESH_PAGING"));
			searchResults = new Array();
			//searchResults = new XMLList("<root/>");
		}
		
		public function changeToPage(newPage:int):void {
			
			pageNum = newPage;	
			performSearch(searchSource, searchWords,source);
		}
		
		/*public function formatArrayForDisplay():void {
			//trace("will format array");	
			var tempObject:Object = new Object();
			var itemCount:int = 0;
			
			tempObject.imageArray = new Array();
			
			formattedArray = new Array(); 
		//trace("totalResults =" + totalResults.toString());
			for (var t:int = 0; t < (totalResults + 1) ; t++) {
				//trace("t= " + t.toString() + ", itemCount =  " + itemCount.toString() + ",itemCount % 3 = " + (itemCount % 3).toString());
				if (itemCount % 3 == 0 && itemCount > 0) {
					//trace("Creating new tempObject and adding 1");
					formattedArray.push(tempObject);
					tempObject = new Object();
					tempObject.imageArray = new Array();
					tempObject.imageArray.push(searchResults[t]);
					//itemCount++;
				} else {
					//trace("adding 1");
					tempObject.imageArray.push(searchResults[t]);
				}		
				itemCount++;
			}
			formattedArray.push(tempObject);
			//trace("new array ready");
		}
		*/
		//public function pageResultSet():void {
			/*
			var offset:int 		= (pageNum - 1) * CUSTOM_PAGE_SIZE;
			var lastRecord:int 	= (pageNum * CUSTOM_PAGE_SIZE);
			
			
			
			for(var t:int ; t < imageResultsArray.length(); t++) {
				if (t > lastRecord) break;
				
				if (t > offset) {
					resultSetPaged.appendChild(imageResultsArray[t]);
				}
			}
			
			this.dispatchEvent(new Event("PIXABAY_SEARCH_DATA_READY"));
			//trace ("total records = "+ totalResults.toString())
			//trace(pagedResults);*/
		//} 
		
		public function morgueFileFinishedSearch(evt:Event):void {
			totalResults = morgueFileCom.totalResults;
			//pixaBayCom.formatArrayForDisplay();
			searchResults = new Array();
			searchResults = morgueFileCom.pagedResults;
			//formatArrayForDisplay();
			//trace(searchResults);
			controller.dataHasBeenLoaded(new Array());	
			dispatchEvent(new Event("REFRESH_PAGING"));
		}
		
		public function finishedSearch(evt:Event):void {
			totalResults = pixaBayCom.totalResults;
			//pixaBayCom.formatArrayForDisplay();
			searchResults = new Array();
			searchResults = pixaBayCom.pagedResults;
			//formatArrayForDisplay();
			//trace(searchResults);
			controller.dataHasBeenLoaded(new Array());	
			dispatchEvent(new Event("REFRESH_PAGING"));
					
		}
	}	
}
