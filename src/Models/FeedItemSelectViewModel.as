package Models
{
	import Controllers.*;
	
	import Interfaces.iController;
	import Interfaces.iModel;
	
	import VOs.*;
	
	import flash.events.*;
	import flash.net.*;
	import flash.utils.getQualifiedClassName;
	
	import mx.rpc.events.ResultEvent;
	
	public class FeedItemSelectViewModel extends BaseModel implements iModel
	{ 
 		[Bindable] public var mainObject:Object;
		 
		public const CUSTOM_PAGE_SIZE:int = 7;		// number of rows per page
		
		[Bindable] public var pageNum:int = 1;
		[Bindable] public var totalResults:int = 0;
		
		[Bindable] public var originalResultSet:Array;
		[Bindable] public var pagedResults:Array;
				
		/* Constructor */
		public function FeedItemSelectViewModel(myController:*) 
		{
			super(myController);
			
		}		
		
		public function changeToPage(newPage:int):void {
			pageNum = newPage;	
		
			loadPage();
		}
		
		public function loadPage():void {
			pagedResults = pageResultsSetArray(originalResultSet);
			//controller.dataHasBeenLoaded(pagedResults);
		}

		public function pageResultsSetArray(pResultSet:Array):Array {
			var offset:int 		= (pageNum - 1) * CUSTOM_PAGE_SIZE;
			var lastRecord:int 	= (pageNum * CUSTOM_PAGE_SIZE) ;
			var pageResultSet:Array = new Array();
			
			for(var t:int = 0 ; t < pResultSet.length; t++) {
				if (t > lastRecord) break;
				
				if (t >= offset && t < lastRecord) {
					pageResultSet.push(pResultSet[t]);
				}
			}
			return pageResultSet;
		}
		
	}	
}
