package Models
{
	import Controllers.*;
	
	import Interfaces.iController;
	import Interfaces.iModel;
	
	import VOs.*;
	
	import flash.events.*;
	import flash.net.*;
	import flash.utils.getQualifiedClassName;
	
	import mx.rpc.events.ResultEvent;
	
	public class StatusIdeaViewModel extends BaseModel implements iModel
	{ 
 		public var mainObject:Object;
		[Bindable] public var pageNum:int = 1;
		[Bindable] public var totalRecords:int = 0;
		public const CUSTOM_PAGE_SIZE:int = 10;
		
		/* Constructor */
		public function StatusIdeaViewModel(myController:*) 
		{
			super(myController);
		}		
	}	
}
