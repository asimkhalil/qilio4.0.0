package Models
{
	import Controllers.*;
	
	import Interfaces.iController;
	import Interfaces.iModel;
	
	import VOs.*;
	
	import flash.events.*;
	import flash.net.*;
	import flash.utils.getQualifiedClassName;
	
	import mx.rpc.events.ResultEvent;
	
	public class FeedViewModel extends BaseModel implements iModel
	{ 
 		[Bindable] public var mainObject:UserFeedVO;
		 
		/* Constructor */
		public function FeedViewModel(myController:*) 
		{
			super(myController);
		}		
	}	
}
