	// ActionScript file
	   
	  [Bindable]  public var   SERVER_ADDRESS:String 		= "";
	  [Bindable]  public var   SERVER_LOGIN:String 			= "";

	 
	  
	  [Bindable]  public var  LINES_FEED_SESSION_KEY:String	= "";
 	  
	  public const PROCESS_ENTITY:String 					= "process_entity_xml.php";
	  public const GET_ENTITY:String 	  					= "get_entity_xml.php";
	  public const GET_SUMMARY:String 	  					= "get_summary_xml.php";

	  public const kNO:int									= 1;
	  public const kYES:int									= 0;

	  public const PAGE_SIZE:int							= 6;
		
		/* summary display options in mainView */
		[Bindable] public var videoSearchOptions:Array = 
			[
				{label:"Youtube", data:"0"},
				{label:"Metacafe", data:"1"}
			];
		
		/* summary display options in mainView */
		[Bindable] public var YesNoOptions:Array = 
		[
			{label:"yes", data:"0"},
			{label:"no", data:"1"}
		];

		[Bindable] public var languagesOptions:Array = 
		[
			{label:"English", data:"0"},
			{label:"Chinese", data:"1"},
			{label:"Spanish", data:"2"},
			{label:"Russian", data:"3"}
		];

		[Bindable] public var fontSizeArr:Array =
		  [
			{label:"6", data:"6"},
			{label:"8", data:"8"},
			{label:"10", data:"10"},
			{label:"12", data:"12"},
			{label:"14", data:"14"},
			{label:"16", data:"16"},
			{label:"18", data:"18"},
			{label:"20", data:"20"},
			{label:"24", data:"24"},
			{label:"28", data:"28"},
			{label:"32", data:"32"},
			{label:"36", data:"36"}
		
	];

		[Bindable] public var fontList:Array = 
		[
			{label:"Arial", data:"Arial"},
			{label:"Ostrich", data:"ostrich"},
			{label:"Oldstandard", data:"oldstandard"},
			{label:"Lobster", data:"lobster"},
			{label:"HennyPenny", data:"hennypenny"},
			{label:"DroidSerif", data:"droidserif"},
			{label:"ChunkFive", data:"chunkfive"},
			{label:"CaptureIt", data:"captureit"},
			{label:"BebasNeue", data:"bebasneue"},
			{label:"Barrio", data:"barrio"},
			{label:"VastShadow", data:"vastshadow"},
			{label:"QuickSand", data:"quicksand"},
			{label:"Pacifico", data:"pacifico"},
			{label:"Ubunto", data:"ubunto"},
			{label:"Astigmatic", data:"astigmatic"},
			{label:"Impact", data:"impact"}
		];

		[Bindable] public var contentTypeOptions:Array = 
		[
			{label:"Content", 	data:"0", state: "", desc:"some text and maybe a link"},
			{label:"Images", 	data:"1", state: "ImageState", desc:"Curate your image add text, links and background"},
			{label:"Video", 	data:"2", state: "VideoState", desc:"Add tittle image and description text to videos"}
		];

		[Bindable] public var searchContentSourceOptions:Array = 
		[
			{label:"Google News search", 	data:"0"},
			{label:"Google BlogSearch", 	data:"1"}
		];

		[Bindable] public var imageSearchSourcesOptions:Array = 
		[
			{label:"Pixabay", 	data:"pixabay.com"},
			{label:"MorgueFile", 	data:"morguefile.com"}
		];

 

		[Bindable] public var statusIdeasCategoryOptions:Array = 
		[
			{label:"Ask for likes / comments", 	data:"0"},
			{label:"Questions", 	data:"1"},
			{label:"Fill in the blank", 	data:"2"},
			{label:"Insert your brand", 	data:"3"},
			{label:"Fan love", 	data:"4"}
		];


		[Bindable] public var imageBackgroundsArray:Array = 
		[
			
			{label:"Light Blue", 	data:"bg-5.png"},
			{label:"Pink", 	data:"bg-6.png"},
			{label:"Blue", 	data:"bg-7.png"},
			{label:"Rose", 	data:"bg-1.jpg"},
			{label:"Water", 	data:"bg-2.jpg"},
			{label:"Crayons", 	data:"bg-3.jpg"},
			{label:"Forest", 	data:"bg-4.jpg"}
		];

		[Bindable] public var iconImagesArray:Array = 
		[
			{label:"Dumb", 	data:"icon-1.png"},
			{label:"All over", 	data:"icon-2.png"},
			{label:"Devil", 	data:"icon-3.png"},
			{label:"Shy", 	data:"icon-4.png"},
			{label:"Happy", 	data:"icon-5.png"},
			{label:"???", 	data:"icon-6.png"},
			{label:"Beat up", 	data:"icon-7.png"},
			{label:"Gone", 	data:"icon-8.png"},
			{label:"Crying", 	data:"icon-9.png"},
			{label:"Say what?", 	data:"icon-10.png"},
			{label:"Le Bored", 	data:"icon-11.png"}
		];

		[Bindable] public var emoticonImagesArray:Array = 
		[
			{label:"Smile", 		data:"smile.png", 		text:":)"},
			{label:"Frown", 		data:"frown.png",		text:":("},
			{label:"Gasp", 			data:"gasp.png",		text:":O"},
			{label:"Grin", 			data:"grin.png",		text:":D"},
			{label:"Tongue", 		data:"tongue.png",		text:":P"},
			{label:"Wink", 			data:"wink.png",		text:";)"},
			{label:"Curly Lips", 	data:"curlylips.png",	text:":3"},
			{label:"Kiss", 			data:"kiss.png",		text:":*"},
			{label:"Grumpy", 		data:"grumpy.png",		text:">:("},
			{label:"Glasses", 		data:"glasses.png",		text:"8)"},
			{label:"Sunglasses", 	data:"sunglasses.png",	text:"8|"},
			{label:"Upset", 		data:"upset.png",		text:">:O"},
			{label:"Confused", 		data:"confused.png",	text:"o.O"},
			{label:"Shark", 		data:"shark.png",		text:"(^^^)"},
			{label:"Pacman", 		data:"pacman.png",		text:":v"},
			{label:"Squint", 		data:"squint.png",		text:"-_-"},
			{label:"Angel", 		data:"angel.png",		text:"O:)"},
			{label:"Devil", 		data:"devil.png",		text:"3:)"},
			{label:"Unsure", 		data:"unsure.png",		text:":/"},
			{label:"Cry", 			data:"cry.png",			text:":'("},
			{label:"Chris Putnam", 	data:"putnam.png",		text:":putnam:"},
			{label:"Robot", 		data:"robot.png",		text:":|]"},
			{label:"Heart", 		data:"heart.png",		text:"<3"},
			{label:"Kiki", 			data:"kiki.png",		text:"^_^"},
			{label:"42", 			data:"42.png",			text:":42:"},
			{label:"Penguin", 		data:"penguin.png",		text:"<(“)"},
			{label:"Poop", 			data:"poop.png",		text:":poop:"},
			{label:"♦", 			data:"♦",		text:"♦"},
			{label:"♠", 			data:"♠",		text:"♠"},
			{label:"¶", 			data:"¶",		text:"¶"},
			{label:"Ø", 			data:"Ø",		text:"Ø"},
			{label:"Â", 			data:"Â",		text:"Â"},
			{label:"Ã", 			data:"Ã",		text:"Ã"},
			{label:"■", 			data:"■",		text:"■"},
			{label:"►", 			data:"►",		text:"►"},
			{label:"◄", 			data:"◄",		text:"◄"},
			{label:"▲", 			data:"▲",		text:"▲"},
			{label:"▼", 			data:"▼",		text:"▼"},
			{label:"™", 			data:"™",		text:"™"},
			{label:"©", 			data:"©",		text:"©"},
			{label:"®", 			data:"®",		text:"®"},
			{label:"°", 			data:"°",		text:"°"},
			{label:"±", 			data:"±",		text:"±"},
			{label:"¶", 			data:"¶",		text:"¶"},
			{label:"¾", 			data:"¾",		text:"¾"},
			{label:"×", 			data:"×",		text:"×"},
			{label:"¢", 			data:"¢",		text:"¢"},
			{label:"¡", 			data:"¡",		text:"¡"},
			{label:"¿", 			data:"¿",		text:"¿"},
			{label:"☻", 			data:"☻",		text:"☻"},
			{label:"☼", 			data:"☼",		text:"☼"},
			{label:"♀", 			data:"♀",		text:"♀"},
			{label:"♂", 			data:"♂",		text:"♂"},
			{label:"♪", 			data:"♪",		text:"♪"},
			{label:"♫", 			data:"♫",		text:"♫"},
			{label:"∑", 			data:"∑",		text:"∑"},
			{label:"√", 			data:"√",		text:"√"},
			{label:"∞", 			data:"∞",		text:"∞"},
			{label:"↑", 			data:"↑",		text:"↑"},
			{label:"↓", 			data:"↓",		text:"↓"},
			{label:"→", 			data:"→",		text:"→"},
			{label:"←", 			data:"←",		text:"←"},
			{label:"↕", 			data:"↕",		text:"↕"},
			{label:"↔", 			data:"↔",		text:"↔"}
		];

		[Bindable] public var scheduledPostStatusOptions:Array = 
		[
			{label:"pending", 	data:"0"},
			{label:"published", 	data:"1"}
		];

		[Bindable] public var scheduledPostTypeOptions:Array = 
		[
			{label:"content", 	data:"0"},
			{label:"image", 	data:"1"},
			{label:"video", 	data:"2"}
		];

		/******* Dummy DATA *****/ 
		[Bindable] public var statusIdeasDummyOptions:Array = 
		[
			{label:"What are your favorite poets", 	data:"0",category: "questions"},
			{label:"Favorite color?", 			data:"1",category: "questions"},
			{label:"What’s the awesomest invention that doesn’t exist yet?", 	data:"2", 	category: "questions"},
			{label:"Would you rather go a week without Internet or a week without running water?", 	data:"3", category: "questions"}
		];

		[Bindable] public var contestIdeasDummyOptions:Array = 
		[
			{header:"My xmas tree 1", 	text: "fvjl dfhbdfsñjhb dñkjb dñk", image: "Media/Administrator.png"},
			{header:"My xmas tree 2", 	text: "fvjl dfhbdfsñjhb dñkjb dñk", image: "Media/computer_48.png"},
			{header:"My xmas tree 3", 	text: "fvjl dfhbdfsñjhb dñkjb dñk", image: "Media/Control Panel.png"},
			{header:"My xmas tree 4", 	text: "fvjl dfhbdfsñjhb dñkjb dñk", image: "Media/danger-icon.gif"}
		];

		

		/***** ****/