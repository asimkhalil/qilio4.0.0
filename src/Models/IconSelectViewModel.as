package Models
{
	import Controllers.*;
	
	import Interfaces.iController;
	import Interfaces.iModel;
	
	import VOs.*;
	
	import flash.events.*;
	import flash.net.*;
	import flash.utils.getQualifiedClassName;
	
	import mx.rpc.events.ResultEvent;
	
	public class IconSelectViewModel extends BaseModel implements iModel
	{ 
 		[Bindable] public var mainObject:Object;
		 
		/* Constructor */
		public function IconSelectViewModel(myController:*) 
		{
			super(myController);
		}		
	}	
}
