package Models
{
	import Controllers.*;
	
	import Interfaces.iController;
	import Interfaces.iModel;
	
	import VOs.*;
	
	import flash.display.BitmapData;
	import flash.events.*;
	
	import mx.rpc.events.ResultEvent;
	
	public class CropImageViewModel extends BaseModel implements iModel
	{ 
 		[Bindable] public var mainObject:CropImageVO;
		 
		/* Constructor */
		public function CropImageViewModel(myController:*) 
		{
			super(myController);
		}		
	}	
}
