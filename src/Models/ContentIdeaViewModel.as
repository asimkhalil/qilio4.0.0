package Models
{
	import Components.GoogleSearchCenter;
	
	import Controllers.*;
	
	import Interfaces.iController;
	import Interfaces.iModel;
	
	import VOs.*;
	
	import flash.events.*;
	import flash.net.*;
	import flash.utils.getQualifiedClassName;
	
	import mx.rpc.events.ResultEvent;
	
	public class ContentIdeaViewModel extends BaseModel implements iModel
	{ 
		public var mainObject:Object;
		
		public const CUSTOM_PAGE_SIZE:int = 5;		// number of rows per page
		public const ELEMENTS_PER_ROW_SIZE:int = 1;		
		
		[Bindable] public var googleSearchCOM:GoogleSearchCenter;
		[Bindable] public var pageNum:int = 1;
		[Bindable] public var totalResults:int = 0;
		
		
		[Bindable] public var searchWords:String = "";
		[Bindable] public var searchSource:String = "";
		[Bindable] public var searchResults:Array;
		
		/* Constructor */
		public function ContentIdeaViewModel(myController:*) {
			super(myController);
			init();
		}		
		
		public function init():void {
			googleSearchCOM = new GoogleSearchCenter();
			googleSearchCOM.addEventListener("GOOGLE_SEARCH_DATA_READY", finishedSearch);
			googleSearchCOM.addEventListener("GOOGLE_SEARCH_DATA_FAILED", finishedSearchAndFailed);
			googleSearchCOM.addEventListener("GOOGLE_SEARCH_END_OF_RESULTS", finishedSearchResultSet);
			
		}		
		
		public function finishedSearchResultSet(evt:Event):void {
			controller.endOfResultSetReached();
			pageNum--;
		}
		
		public function finishedSearchAndFailed(evt:Event):void {
			controller.dataHasBeenLoaded(new Array());
		}
		
		public function performSearch(pSource:String, pSearchWords:String, isNewSearch:Boolean):void {
			searchWords  = pSearchWords != "" ? pSearchWords : searchWords;
			searchSource = pSource != "" ? pSource : searchSource;
		
			googleSearchCOM.pageNum = pageNum;
			googleSearchCOM.pageSize = CUSTOM_PAGE_SIZE;
			googleSearchCOM.elementsPerRow = ELEMENTS_PER_ROW_SIZE;	
			
			if (pSource == "Google News search") {
				trace("Searching news");
				googleSearchCOM.searchNews(pSearchWords, isNewSearch);
			}
			
			if (pSource == "Google BlogSearch") {
				googleSearchCOM.searchBlog(pSearchWords);
			}
		}
		
		public function resetSearch():void {
			pageNum = 1;
			totalResults = 0;
			dispatchEvent(new Event("REFRESH_PAGING"));
			searchResults = new Array()
		}
		
		public function changeToPage(newPage:int):void {
			pageNum = newPage;	
			performSearch(searchSource, searchWords, false);
		}
		
		public function finishedSearch(evt:Event):void {
			totalResults = googleSearchCOM.totalResults;
			searchResults = googleSearchCOM.pagedResults;
			//trace(searchResults);
			dispatchEvent(new Event("REFRESH_PAGING"));
			controller.dataHasBeenLoaded(new Array());
		}
	}	
}
