package Models
{
	import Components.GoogleSearchCenter;
	import Components.MetaCafeCenter;
	
	import Controllers.*;
	
	import Interfaces.iController;
	import Interfaces.iModel;
	
	import VOs.*;
	
	import flash.events.*;
	import flash.net.*;
	import flash.utils.getQualifiedClassName;
	
	import mx.rpc.events.ResultEvent;
	
	public class ImageFeedViewModel extends BaseModel implements iModel
	{ 
 		public const CUSTOM_PAGE_SIZE:int = 3;		// number of rows per page
		public const ELEMENTS_PER_ROW_SIZE:int = 2;		
		
		public var mainObject:Object;
		[Bindable] public var pageNum:int = 1;
		[Bindable] public var totalResults:int = 0;
		[Bindable] public var searchWords:String = "";
		[Bindable] public var searchSource:String = "";
		[Bindable] public var _searchResults:Array;
		[Bindable] public var googleSearchCenter:GoogleSearchCenter;
		[Bindable] public var metaCafeCenter:MetaCafeCenter;
		[Bindable] public var resultSetPaged:Array;
		[Bindable] public var formattedArray:Array;
		
		[Bindable]
		public function set searchResults(value:Array):void{
			_searchResults = value;
			
			if(_searchResults){
				BaseEventDispatcher.getInstance().dispatchEvent(new Event('ONRESULT'))
			}
		}
		
		public function get searchResults():Array {
			return _searchResults;
			
			
		}
		/* Constructor */
		public function ImageFeedViewModel(myController:*) {
			super(myController);
			init();
		}		
		
		public function init():void {
			googleSearchCenter = new GoogleSearchCenter();
			googleSearchCenter.addEventListener("GOOGLE_SEARCH_DATA_READY", finishedSearch);
			
			/*metaCafeCenter = new MetaCafeCenter();
			metaCafeCenter.addEventListener("METACAFE_SEARCH_DATA_READY", finishedSearch);*/
		}
		
		public function performSearch(isNewSearch:Boolean):void {
			//trace("perform search");
			/*searchSource= pSource != "" ? pSource : searchSource;*/
 			googleSearchCenter.pageNum = pageNum;
			googleSearchCenter.pageSize = CUSTOM_PAGE_SIZE;
			googleSearchCenter.elementsPerRow = ELEMENTS_PER_ROW_SIZE;	
			googleSearchCenter.loadImagesFeed("http://qilio.com/publicdomain.xml", isNewSearch);
		}
		
		public function resetSearch():void {
			pageNum = 1;
			totalResults = 0;
			dispatchEvent(new Event("REFRESH_PAGING"));
			searchResults = new Array();
			//searchResults = new XMLList("<root/>");
		}
		
		public function changeToPage(newPage:int):void {
			pageNum = newPage;	
			performSearch(false);
		}
				
		public function finishedSearch(evt:Event):void {
			
			/*if (searchSource == "Youtube") {*/
				totalResults = googleSearchCenter.totalResults / ELEMENTS_PER_ROW_SIZE;
				searchResults = googleSearchCenter.pagedResults;
				
				for(var i:int=0;i<searchResults.length;i++) {
					if(searchResults[i].length == 0) {
						//add dummy results 
						for(var j:int=0;j<ELEMENTS_PER_ROW_SIZE;j++) {
							var obj:Object = {imagePath:"",title:"",link:""};
							searchResults[i].push(obj);
						}
					}
				}
				
			/*} else {
				totalResults = metaCafeCenter.totalResults / ELEMENTS_PER_ROW_SIZE;
				searchResults = metaCafeCenter.pagedResults;
			}*/
				
			//formatArrayForDisplay();
			dispatchEvent(new Event("REFRESH_PAGING"));
			controller.dataHasBeenLoaded(new Array());			
		}
	}	
}
