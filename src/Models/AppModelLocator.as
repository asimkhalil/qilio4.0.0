package Models
{
	import Components.CustomTextControl;
	
	import flash.xml.XMLDocument;
	
	import mx.collections.ArrayCollection;

	public class AppModelLocator
	{
		[Bindable]
		public static var channelsCol:String;
		public static var currentlySelectedCustomTextName:String;
		
		[Bindable]
		public static var imageEffectsFilters:Array = [];
	}
}