package Models
{
	import Controllers.*;
	
	import Interfaces.iController;
	import Interfaces.iModel;
	
	import VOs.*;
	
	import flash.events.*;
	import flash.net.*;
	import flash.utils.getQualifiedClassName;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	public class KeywordMaintainanceViewModel extends BaseModel implements iModel
	{ 
		/*[Bindable]
 		public var keywordsDp:ArrayCollection = new ArrayCollection();*/
		public var mainObject:Object;
		[Bindable] public var pageNum:int = 1;
		[Bindable] public var totalRecords:int = 0;
		
		/* Constructor */
		public function KeywordMaintainanceViewModel(myController:*) {
			super(myController);
		}		
	}	
}
