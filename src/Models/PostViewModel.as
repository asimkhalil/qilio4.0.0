package Models
{
	import Controllers.*;
	
	import Interfaces.iController;
	import Interfaces.iModel;
	
	import VOs.*;
	
	import flash.events.*;
	import flash.net.*;
	import flash.utils.getQualifiedClassName;
	
	import mx.rpc.events.ResultEvent;
	
	public class PostViewModel extends BaseModel implements iModel
	{ 
 		public var mainObject:Object;
		
		/* Constructor */
		public function PostViewModel(myController:*) 
		{
			super(myController);
		}
	}	
}
