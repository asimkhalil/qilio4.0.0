package Models
{
	import Interfaces.iController;
	
	import VOs.*;
	
	import flash.events.*;
	import flash.net.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.getDefinitionByName;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.managers.CursorManager;
	import mx.messaging.messages.RemotingMessage;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	public class BaseModel{
		include "../Models/Constants.as";
		
		/* Singleton model reference */
		[Bindable] public var appStateVars:AppStateVars;
		[Bindable] protected var controller:*;
		
		/* ADOBE BUG FIX , if these bogus objects are not created then the object factory fails.*/
		public var facebookPageVO:FacebookPageVO;
		public var googleBlogResultVO:GoogleBlogResultVO;
		
		public var welcomeShown:Boolean = false;
		
		public function BaseModel(myController:*):void {
			controller = myController;
			appStateVars = AppStateVars.getInstance();
		}
		
		public function addToSearchStatement(statementArray:Array, searchField:String, searchValue:String):Array {
			//trace("addToSearchStatement ran, searchValue = " + searchValue + " and searchField = " + searchField);
			
			/* If any of the fields are empty, just return the array as it is */
			if (searchValue == "" || searchValue == null || searchField == "" || searchField == null) {
				//trace("addToSearchStatement invalid, cancelling");
				return statementArray;
			}
			
			if (statementArray == null) {
				statementArray = new Array();
				//trace("initialized array");
			}
			
			statementArray.push({field:searchField, value:searchValue});
			return statementArray;
		}
		
		
		public function loadListOfObjectsFromArray(objectList:Array, targetArray:String, objectClass:String, labelForComboBoxOption:String = "", dataForComboBoxOption:String = ""):void {
			objectClass = "VOs." + objectClass;
			
			var dynamicClass:Class = getDefinitionByName(objectClass) as Class;
			
			if ( !this[targetArray])
				this[targetArray] = new Array();
			
			for (var t:int = 0; t < objectList.length; t++) {
				
				var inst:Object = new dynamicClass();
				inst.loadDataFromResult(objectList[t]);
				
				if (labelForComboBoxOption != "") {
					inst["label"] = inst[labelForComboBoxOption];
					inst["data"]  = inst[dataForComboBoxOption];
				}
				
				this[targetArray].push(inst);
			}
		}
	}
}