package Models
{
	import Controllers.*;
	
	import Interfaces.iController;
	import Interfaces.iModel;
	
	import VOs.*;
	
	import flash.events.*;
	import flash.net.*;
	import flash.utils.getQualifiedClassName;
	
	import mx.rpc.events.ResultEvent;
	
	public class SettingsViewModel extends BaseModel implements iModel
	{ 
 		public var mainObject:Object;
		[Bindable] public var hasSomethingChanged:Boolean = false;
		/* Constructor */
		public function SettingsViewModel(myController:*) {
			super(myController);
		}		
	}	
}
