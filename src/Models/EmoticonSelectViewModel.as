package Models
{
	import Controllers.*;
	
	import Interfaces.iController;
	import Interfaces.iModel;
	
	import VOs.*;
	
	import flash.events.*;
	import flash.net.*;
	import flash.utils.getQualifiedClassName;
	
	import mx.rpc.events.ResultEvent;
	
	public class EmoticonSelectViewModel extends BaseModel implements iModel
	{ 
 		[Bindable] public var mainObject:Object;
		 
		/* Constructor */
		public function EmoticonSelectViewModel(myController:*) 
		{
			super(myController);
		}		
	}	
}
